#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

################################################################################
##                                                                            ##
##	Problem describing a the SOD problem with two fluids in a small domain.   ##
##                                                                            ##
################################################################################
# Preliminary imports
import numpy as np


# ==============================================================================
#                            Problem definition
# ==============================================================================
class ProblemData():
	def __init__(self):

		# ******************************************************************************
		#                          Meshing
		#*******************************************************************************
		self.ProblemID  = "Euler_SOD_2Fluid_SmallDomain"
		self.MeshName   = "SimpleRectangle"

		# ******************************************************************************
		#                          Equation
		#*******************************************************************************
		self.GoverningEquationsIndex = 1
		self.EOSEquationsIndex = [1, 1]

		# *******************************************************************************
		#                          Fluids properties
		#********************************************************************************
		# Select the indices speciying the fluids properties living on each subdomain
		self.FluidIndex = [0, 2]

		# The fluid at index 0 is the complementary to the specified ones.
		# Only need to precise the location of other fluids
		self.FluidInitialSubdomain = [(4,0,0,0.5,0.5,0.5,4,4)]

		# Index of the initial conditions to apply on each subdomain
		self.InitialConditions = [(5,), (4,)]

		# Row: BcTag, coklumn, index of the condition to apply depending on the fluid that comes next to it
		self.BoundaryConditions = np.array(\
								  [[(0,), (0,)],\
								   [(0,), (0,)],\
								   [(0,), (0,)],\
								   [(0,), (0,)]])




# ==============================================================================
#                            Problem Solution
# ==============================================================================
class ProblemSolution():
	"""
		Simple class containing the tools and exact solution associated to the problem "Euler_SOD_2Fluid_SmallDomain".

		.. note::

			- THIS SOLUTION HAS NOT BEEN IMPLEMENTED YET

	"""

	def __init(self):
		"""
			Args:
				None
		"""
		self.ProblemID = "Euler_SOD_2Fluid"
		self.MeshName  = "SimpleRectangle"

	#### Solution routine that will be called externally #####
	def Solution(self, xy, t):
		"""
		Function defining the exact solution associated to the problem "Euler_SOD_2Fluid_SmallDomain".

		Args:
			xy  (2D numpy array, float)   The points on which to evaluate the exact solution (NbPoints x 2)
			t   (float)                   The time point where to consider the solution
		"""

		# ------------------- Initialisation -----------------------------------
		raise(NotImplementedError("Error: The exact solution to this problem has not be defined yet or do not exist. Abortion."))
