#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

####################################################################################
##                                                                                ##
##	Problem describing a simple constant rotation of a bump in a given fluid.     ##
##                                                                                ##
####################################################################################
# Preliminary imports
import numpy as np



# ==============================================================================
#                            Problem definition
# ==============================================================================
class ProblemData():
	def __init__(self):

		# **********************************************************************
		#                          Meshing
		#***********************************************************************
		self.ProblemID = "Advection_Rotation_1Fluid"
		self.MeshName  = "SimpleSquare"

		# **********************************************************************
		#                          Equation
		#***********************************************************************
		self.GoverningEquationsIndex = 2
		self.EOSEquationsIndex = [1]

		# **********************************************************************
		#                          Fluids properties
		#***********************************************************************
		# Select the indices speciying the fluids properties living on each subdomain
		self.FluidIndex = [0]

		# The fluid at index 0 is the complementary to the specified ones.
		# Only need to precise the location of other fluids
		self.FluidInitialSubdomain = []

		# Index of the initial conditions to apply on each subdomain
		self.InitialConditions = [(2,)]

		# Row: BcTag, coklumn, index of the condition to apply depending on the fluid that comes next to it
		self.BoundaryConditions = np.array(\
								  [[(0,)],\
								   [(0,)],\
								   [(0,)],\
								   [(0,)]])



# ==============================================================================
#                            Problem Solution
# ==============================================================================
class ProblemSolution():
	"""
		Simple class containing the tools and exact solution associated to the problem "Advection_Rotation_1Fluid",
		whose method can be used in routine postprocessing convergence rate and comparison tools.

	"""

	def __init(self):
		"""
			Args:
				None
		"""
		self.ProblemID = "Advection_Rotation_1Fluid"
		self.MeshName  = "SimpleSquare"

	#### Solution routine that will be called externally #####
	def Solution(self, xy, t):
		"""
		Function defining the exact solution associated to the problem "Advection_Rotation_1Fluid".

		Args:
			xy  (2D numpy array, float)   The points on which to evaluate the exact solution (NbPoints x 2)
			t   (float)                   The time point where to consider the solution
		"""

		# ------------------- Initialisation -----------------------------------
		# Center of the bump
		xc = 1
		yc = 1

		# Center of the rotation
		xr = 0
		yr = 0

		# Recasting the points to rotate
		NbPoints = xy.shape[0]
		Points   = np.zeros((3, NbPoints))
		Points[2,:]   = 1
		Points[0:2,:] = xy.T

		# Defining the vector that will store the exact solution
		Init = np.zeros((1,NbPoints))

		# ---------------- Computing the exact solution --------------------------------------
		# Computing the rotation and translation matrices
		PP   = np.array([[1,0,0],[0,1,0],[0,0,1]])
		QP   = np.array([[1,0,-0], [0,1,-0], [0,0,1]])
		R    = np.array([[np.cos(2*np.pi*t),np.sin(2*np.pi*t), 0],[-np.sin(2*np.pi*t),np.cos(2*np.pi*t), 0], [0,0,1]])
		RR = np.dot(PP,np.dot(R,QP))

		# Evaluating the initial condition on the rotated points
		RotatedPoints = np.dot(RR, Points)
		r2  = (RotatedPoints[0,:]-xc)**2+(RotatedPoints[1,:]-yc)**2
		ind = np.where(r2<0.5-1e-2)[0]
		Init[0,ind] = 3*np.exp(-1/(1-r2[ind]))

		# ---------------- Sending back the solution -----------------------------------------
		# Filling the solution vectors
		self.Sol      = Init
		self.LSValues = np.ones((1,NbPoints))
