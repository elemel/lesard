#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

################################################################################
##                                                                            ##
##	Problem describing a Stationatry vortex problem with a one fluid.         ##
##                                                                            ##
##                                                                            ##
################################################################################
# Preliminary imports
import numpy as np


# ==============================================================================
#                            Problem definition
# ==============================================================================
class ProblemData():
	def __init__(self):

		# ******************************************************************************
		#                          Meshing
		#*******************************************************************************
		self.ProblemID = "Euler_StationaryVortex"
		self.MeshName  = "BigSimpleRectangle"

		# ******************************************************************************
		#                          Equation
		#*******************************************************************************
		self.GoverningEquationsIndex = 1
		self.EOSEquationsIndex = [1]

		# *******************************************************************************
		#                          Fluids properties
		#********************************************************************************
		# Select the indices speciying the fluids properties living on each subdomain
		self.FluidIndex = [3]

		# The fluid at index 0 is the complementary to the specified ones.
		# Only need to precise the location of other fluids
		self.FluidInitialSubdomain = []

		# Index of the initial conditions to apply on each subdomain
		self.InitialConditions = [(2,)]

		# Row: BcTag, coklumn, index of the condition to apply depending on the fluid that comes next to it
		self.BoundaryConditions = np.array(\
								  [[(0,)],\
								   [(0,)],\
								   [(0,)],\
								   [(0,)]])



# ==============================================================================
#                            Problem Solution
# ==============================================================================
class ProblemSolution():
	"""
		Simple class containing the tools and exact solution associated to the problem "Euler_StationaryVortex_1Fluid".

	"""

	def __init(self):
		"""
			Args:
				None
		"""
		self.ProblemID = "Euler_StationaryVortex"
		self.MeshName  = "BigSimpleRectangle"

	def Solution(self, xy, t):
		"""
		Function defining the exact solution associated to the problem "Euler_StationaryVortex_1Fluid".

		Args:
			xy  (2D numpy array, float)   The points on which to evaluate the exact solution (NbPoints x 2)
			t   (float)                   The time point where to consider the solution
		"""

		# ------------------- Initialisation -----------------------------------
		# Initialising the point-values
		Init = np.ones((4, len(xy)))

		# Retrieving the xy values of the points and the geometrical informations from the subdomain
		xc    = 0
		yc    = 0
		gamma = 1.5
		pinf  = 0

		# Defining the predefined constants (copied from THE fortran code)
		uinf = 0
		vinf = 0
		Tinf = 1
		beta = 5
		Ma   = 0.5*beta/np.pi

		# Defining the space-dependent quantities
		r2 = ((xy[:,0]-xc)**2+(xy[:,1]-yc)**2)
		co = np.exp(0.5*(1-r2))

		# Retrieving the quantities that will furnish a stationary vortex
		du = Ma*co*(-xy[:,1])
		dv = Ma*co*( xy[:,0])
		u  = uinf + du
		v  = vinf + dv

		# Linking them with the fluid's properties
		Dt  = -(gamma-1)*0.5*Ma*Ma*co*co/gamma
		rho = (Tinf + Dt)**(1/(gamma-1))

		# Defining the raw initilisation variable
		Init[0,:] = rho
		Init[1,:] = rho*u
		Init[2,:] = rho*v
		Init[3,:] = rho**gamma   # p

		# Getting the value of E through the EOS
		FluidSpecs = np.array([[pinf, gamma]])
		val = Init[3,:]/((FluidSpecs[:,1]-1)*Init[0,:])
		Init[3,:] = Init[0,:]*val + 0.5*Init[0,:]*(u**2+v**2)

		# -------------- Assigning the values to the solution ------------------
		self.Sol = Init
