#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

""" 
This file automatically extends the pythonpath to run the code properly, 
independently of the folder from which the code is ran.  
It is dependent from the file :code:`Pathes`. Therefore, the location of :code:`Pathes` has to 
be added to the current pythonpath in use when importing this file (:code:`IncludePathes`).

.. code::

    # Modify the line 
    sys.path.append(os.path.join(Dir, ".."))
    # to
    sys.path.append(os.path.join(Dir, "The relative location of Pathes.py"))

That way, the :code:`SettingPath` and :code:`ProblemPath` will be correctly set-up upon the user wishes,
taking care of your local configuration.
You can also edit the pathes to the different parts where the source code is located by editing the following lines
to your taste.

.. code::

    sys.path.append(os.path.join(Dir, "..", "LocalLib"))
    sys.path.append(os.path.join(Dir,"PostProcessing"))
    sys.path.append(os.path.join(Dir,"ProblemDefinition"))
    sys.path.append(os.path.join(Dir,"Meshing"))
    sys.path.append(os.path.join(Dir,"_Solver"))

"""

# --------------------------------------------------------------------------
# Includes that allows the functionnality of the code
# --------------------------------------------------------------------------
import sys, os

# Get the current file location (workaround for the file 
# to be able to be loaded from anywhere)
Dir = os.path.dirname(__file__)

# Import for the locally copied libraries parts
sys.path.append(os.path.join(Dir, "..", "LocalLib"))

# Pathes for the different parts of the code
sys.path.append(Dir)
sys.path.append(os.path.join(Dir,"PostProcessing"))
sys.path.append(os.path.join(Dir,"ProblemDefinition"))
sys.path.append(os.path.join(Dir,"Meshing"))
sys.path.append(os.path.join(Dir,"_Solver"))

# --------------------------------------------------------------------------
# Includes that allows to solve a user defined problem
# --------------------------------------------------------------------------
# Pathes for locating the user defined Path file at a higher level
sys.path.append(os.path.join(Dir, ".."))

# Load user defined pathes for the user defined problems in the environment
import Pathes
sys.path.append(os.path.abspath(Pathes.ProblemPath))
sys.path.append(os.path.abspath(Pathes.SettingPath))


