#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

"""
|
|

The file :code:`SourceCode/Mappers.py` maps the parameters that can be set by the user to the actual module or routine that implements the related problem or property.
Please then use this page as a reference when designing a new problem or setting file. Furthermore, when implementing a new possibility for the code, please copy the new module or portion of code in the corresponding folder and enrich the mappers below.

|

.. Note::

    The mappers for the problem dependent equations of state, boundary and
    initial conditions should be provided in the ProblemFile module in the initialisation of their
    respective classes (see the problem definition's documentation).

|
|

"""

# ==============================================================================
#                  Import the different modules
# ==============================================================================
# Target the local modules
import SourceCode.IncludePathes
import Pathes

# Import necessary python libraries
import LocalLib.ReversedFunctools as fcts

# Import the custom problems modules
import ProblemDefinition.GoverningEquations  as GE
import ProblemDefinition.FluidModels         as FM
import ProblemDefinition.BoundaryConditions  as BCs

# Import the custom schemes modules
import _Solver.TemporalSchemes    as TI
import _Solver.QuadratureRules    as QR
import _Solver.SpatialSchemes     as SC
import _Solver.SpatialModifiers   as SM

# Import the custom fluids module
import _Solver.FluidSelectors     as FS
import _Solver.Redistancers       as FR

# ==============================================================================
#                  Mappers for the problem's definitions
# ==============================================================================

"""-----------------------------------------------------------------------------
Mapper function for the governing equations
-----------------------------------------------------------------------------"""
def Governing_Equations(id):
    """
    .. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

    Mapper to the modules contained in the :code:`ProblemDefinition.GoverningEquations` library,
    defining the governing equations.

    .. rubric:: **Parameters**

    |bs| |bs| **id** *(integer)* -- the index corresponding to the investigated equation

        *Implemented equations given the id*

        1. | Euler equations
        2. | Linear advection (rotation)
        3. | Linear advection (translation)

    |

    Returns:
        GoverningEquations.Module (module): the module itself,
        containing the classes associated to the given equation
        (see the documentation of :code:`ProblemDefinition`)
    """

    # ------------------ Mapping through the index -----------------------------
    if   id==1: return(GE.EulerEquations)
    elif id==2:	return(GE.LinearAdvectionRotation)
    elif id==3:	return(GE.LinearAdvectionTranslation)

    else: raise ValueError("Error in the problem definition:\n\
                            Wrong index for the wished governing equation.\n\
                            Check the Mapper and ProblemDefinition files.\n\
                            Abortion.")


"""-----------------------------------------------------------------------------
Mapper function for the pre-registered fluids
-----------------------------------------------------------------------------"""
def Fluid_Properties(id, Model):
    """
    .. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

    Mapper that defines the considered fluid properties to input in the model required by the
    GoverningEquations, according to the fluids templates (classes) defined in
    :code:`ProblemDefinition.FluidModels` module.

    .. rubric:: **Parameters**

    |bs| |bs| **id** *(integer)* -- the index corresponding to the wished Fluid properties

    |bs| |bs| **Model** *(string)* -- name of the fluid's model that one wants to use


        *Implemented fluid types given the id*

        0. | Air with properties :math:`\gamma=1.5` and :math:`p_\infty=0` and :math:`R=0.287`,   with a blue representation color
        1. | R22 fluid with properties :math:`\gamma=1.249` and :math:`p_\infty=0` and :math:`R=0.091`, with a red representation color
        2. | Helium with properties :math:`\gamma=1.67` and :math:`p_\infty=0` and :math:`R=2.08`, with a green representation color

    |

    Returns:
        Prop (Model instance): Returns an instance of the fluid model with filled properties
    """

    # ------------------ Mapping through the index -----------------------------
    if   id==0:  Properties = ['b', 1.4, 0, 0.287]					# Air, ideal
    elif id==1:  Properties = ['r', 1.249, 0, 0.091]		        # R22, ideal
    elif id==2:  Properties = ['g', 1.67, 0, 2.08]					# Helium, ideal
    elif id==3:  Properties = ['b', 1.5, 0, 0.287]					# Air, ideal, gmm=1.5

    try:    Prop = (eval("FM."+ Model)(*Properties))
    except: raise ValueError("Error in the problem definition:\n\
                            Wrong index for the wished fluid properties.\n\
                            Check the Mapper and ProblemDefinition files.\n\
                            Abortion.")
    return(Prop)


# ==============================================================================
#                  Mappers for the solvers
# ==============================================================================

"""-----------------------------------------------------------------------------
Mapper function for the temporal schemes
-----------------------------------------------------------------------------"""
def Temporal_Scheme(id, params):
    """
    .. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

    Mapper that defines the temporal scheme to use, according to the predefined temporal schemes
    given in :code:`SourceCode._Solver.TemporalSchemes` library.

    .. rubric:: **Parameters**

    |bs| |bs| **id** *(integer)* -- the index corresponding to the wished Fluid properties

    |bs| |bs| **params** *(integers list)* -- list of the parameters to give to the time stepping functions as given by the user


        *Implemented temporal schemes given the id*

        1. | Deffered correction (DeC)

    |

    Returns:
        Func (function callback): Returns the iteration routine itself
    """

    # ------------------ Mapping through the index -----------------------------
    if   id==1:  return(fcts.partial(TI.DeC.DeC, *params))

    else: raise ValueError("Error in the problem definition:\n\
                            Wrong index for the wished temporal scheme.\n\
                            Check the Mapper and ProblemSettings files.\n\
                            Abortion.")


"""-----------------------------------------------------------------------------
Mapper function for the spatial schemes
-----------------------------------------------------------------------------"""
def Spatial_Scheme(id):
    """
    .. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

    Mapper that defines the temporal scheme to use, according to the predefined spatial schemes
    given in :code:`SourceCode._Solver.SpatialSchemes` library.

    .. rubric:: **Parameters**

    |bs| |bs| **id** *(integer)* -- the index corresponding to the wished Fluid properties

        *Implemented spatial schemes given the id*

        0. | Continuous Galerkin
        1. | Continuous Galerkin + Lax-Friedrichs flux
        2. | Continuous Galerkin on Primary variables

    |

    Returns:
        SpatialSchemes.Module (module): Returns the module associated to the wished spatial scheme
    """

    # ------------------ Mapping through the index -----------------------------
    if   id==0:  return(SC.CG)
    elif id==1:  return(SC.CG_LFX)
    elif id==2:  return(SC.CG_Primary)

    else: raise ValueError("Error in the problem definition:\n\
                            Wrong index for the wished spatial scheme.\n\
                            Check the Mapper and ProblemSettings files.\n\
                            Abortion.")


"""-----------------------------------------------------------------------------
Mapper function for the mollifiers
-----------------------------------------------------------------------------"""
#### Limiting routines ####
def Limiter(id):
    """ .. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

    Mapper that defines the limiting amendement to apply to the scheme, according to the given index.
    The user input for the admendements sequence is interpreted in the :code:`SourceCode._Solver.SpatialModifiers`
    library. All the limiters should have a filename begining by :code:`Limiter_`.

    .. rubric:: **Parameters**

    |bs| |bs| **id** *(integer)* -- the index corresponding to the wished amendement to the scheme

        *Implemented amendements given the id* and their type

        -  |bs| Limiters
            1. | Psi

    |

    Returns:
        SpatialModifiers.Module (module): Returns the module associated to the wished limiter
    """

    # ------------------ Mapping through the index -----------------------------
    if id==1:	return(SM.Limiter_Psi)

    else: raise ValueError("Error in the problem definition:\n\
                            Wrong index for the wished limiter.\n\
                            Check the Mapper and ProblemSettings files.\n\
                            Abortion.")

#### Filtering routines ####
def Filtering(id):
    """ .. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

    Mapper that defines the filtering amendement to apply to the scheme, according to the given index.
    The user input for the admendements sequence is interpreted in the :code:`SourceCode._Solver.SpatialModifiers`
    library. All the filters should have a filename begining by :code:`Filtering_`.

    .. rubric:: **Parameters**

    |bs| |bs| **id** *(integer)* -- the index corresponding to the wished amendement to the scheme

        *Implemented amendements given the id* and their type

       -  |bs| Filtering
            1. | Streamline (failing for systems)

    |

    Returns:
        SpatialModifiers.Module (module): Returns the module associated to the wished limiter
    """

    # ------------------ Mapping through the index -----------------------------
    if id==1:
        print("WARNING: You selected an amendement to the scheme that is known for having bugs and is still under debug.")
        return(SM.Filtering_Streamline)

    else: raise ValueError("Error in the problem definition:\n\
                            Wrong index for the wished limiter.\n\
                            Check the Mapper and ProblemSettings files.\n\
                            Abortion.")


"""-----------------------------------------------------------------------------
Mapper function for the fluid selectors
-----------------------------------------------------------------------------"""
def Fluid_Selector(id):
    """
    Mapper that defines the  the fluid selector to use, according to the ones defined in
    :code:`SourceCode._Solver.FluidSelectors` module, containing its type, evolution equation,
    fluid determination and possibly redistancing routines.

    .. rubric:: **Parameters**

    |bs| |bs| **id** *(integer)* -- the index corresponding to the wished Fluid selector

        *Implemented fluid's selectors*

        1. | NaiveLS_CG
        2. | NaiveLS_CG_LFX
        3. | NaiveLS_CG_LFX_Limited

    |

    Returns:
        FluidSelectors.Module (module): class containing all the fluid's selector tools
    """

    # ------------------ Mapping through the index -----------------------------
    if   id == 1: return(FS.NaiveLS_CG)
    elif id == 2: return(FS.NaiveLS_CG_LFX)
    elif id == 3: return(FS.NaiveLS_CG_LFX_Limited)

    else: raise ValueError("Error in the problem definition:\n\
                            Wrong index for the wished fluid selector.\n\
                            Check the Mapper and ProblemSettings files.\n\
                            Abortion.")


"""-----------------------------------------------------------------------------
Mapper function for the fluid selectors
-----------------------------------------------------------------------------"""
def Boundary_Conditions(id, *params):
    """
    Mapper that defines the  the fluid selector to use, according to the ones defined in
    :code:`SourceCode._Solver.FluidSelectors` module, containing its type, evolution equation,
    fluid determination and possibly redistancing routines.

    .. rubric:: **Parameters**

    |bs| - |bs| **id** *(integer)* -- the index corresponding to the wished Fluid selector
         - |bs| **id** *(params)*  -- (optional) the parameters to pass to the selected boundary function routine

        *Implemented boundary conditions*

        0. | Outflow
        1. | Inflow (see the inflow doc to know which inflow function and parameters are available)
        2. | Wall
        3. | Dirichlet

    |

    Returns:
        BoundaryConditionsRoutine (function callback): function callback applying the selected boundary condition.
    """

    # ------------------ Mapping through the index -----------------------------
    if   id == 0: return(BCs.Outflow.Outflow)
    elif id == 1: return(BCs.Inflow.Inflow(params).Inflow)
    elif id == 2: return(BCs.Wall.Wall)
    elif id == 3: return(fcts.partial(BCs.Dirichlet.Dirichlet, *params))

    else: raise ValueError("Error in the problem definition:\n\
                            Wrong index for the boundary conditions.\n\
                            Check the Mapper and ProblemSettings files.\n\
                            Abortion.")


"""---------------------------------------------------------------------------
Mapper function for the redistancing
------------------------------------------------------------------------------"""
def Redistancer(id, *params):
    """
    Mapping to the wished redistancing algorithm for the level set. Please refer to each module to
    discover the possible further parameters to select.

    .. rubric:: **Parameters**

    |bs| - |bs| **id** *(integer)* -- the index corresponding to the wished quadrature implementation

        *Implemented redistancing routines*

        1. | HopfLax with Gradient Descent as an optimisation solver and Weno-like gradient approximation (recomendedd)
        2. | HopfLax with Bregman split method as optimizer
        3. | HopfLax with Backstracking Bregman split as optimizer, using Weno-like gradient approximation
        4. | HopfLax with Classical Bregman split as optimizer and Backstracking Bregman split as sub-optimizer, using Weno-like gradient approximation

    |

    Returns:
        QuadratureRules.Module (module): instance of the clas furnishing the redistancing technique.
    """

    # ------------------ Mapping through the index -----------------------------
    if    id == 1: return(FR.HopfLax_Descent.Redistancing(*params))
    elif  id == 2: return(FR.HopfLax_Bregman.Redistancing(*params))
    elif  id == 3: return(FR.HopfLax_GlobalBregman.Redistancing(*params))
    elif  id == 4: return(FR.HopfLax_NestedBregman.Redistancing(*params))

    else: raise ValueError("Wrong index for the redistancing routine type.")


"""-------------------------------------------------------------------------------------
Mapper function for the quadratures modules (each possibly containing several types of)
----------------------------------------------------------------------------------------"""
def Quadrature_Rules(id):
    """
    Mapping to the wished quadrature rule module. Please refer to each module to
    discover the possible further parameters to select.

    .. rubric:: **Parameters**

    |bs| - |bs| **id** *(integer)* -- the index corresponding to the wished quadrature implementation

        *Implemented quadrature rules*

        1. | HandBased, hardly coded
        2. | Using the library of QuadPy, with further choices to select (see the module itself for more details)

    |

    Returns:
        QuadratureRules.Module (module): module containing the classes furnishing the inner and boundary quadratures.
    """

    # ------------------ Mapping through the index -----------------------------
    if    id == 1: return(QR.HandBased)
    elif  id == 2: return(QR.QuadPy)

    else: raise ValueError("Wrong index for the quadrature type.")
