#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

"""

|

Aim of the module
*****************

Module providing the class :code:`MeshHandling` gathering the tools to handle a mesh given in a custom MeshStructure format.
For having a direct access to the class, import the module by

.. code-block:: python

	from Meshing_PrimalMesh import *

|


Attributes description
**********************

"""

# ===============================================================================
# Import modules
# ===============================================================================
# Import geometric modules
from shapely.geometry import LinearRing as ShapelyLinearRing
from shapely.geometry import Polygon    as ShapelyPolygon
from shapely.geometry import Point      as ShapelyPoint
from matplotlib.colors import hsv_to_rgb

# Import python math modules
import matplotlib.pyplot as plt
import numpy as np

# Import python generic modules
from   itertools import chain
import sys, os, copy
import pickle
import re

# Import custom modules
from Meshing.MeshStructure import MeshStructure
import Pathes

# ===============================================================================
#                          Mesh handling
# ===============================================================================

#### Class funrnishing tools for handling a mesh #####
class MeshHandling():
	"""
		|

		.. rubric::	**Class purpose**

		Class Furnishing the basic tools to handle a mesh that is in a
		MeshStructure format. The initialisation of the class without any
		argument (allowing to use LoadMesh and ReadTxtMesh from optional arguments),
		with a mesh itself in a MeshStructure format or from 4 argument that point
		to the mesh file to load.

		|

		.. admonition::	**Creating a mesh instance**

			One can create a void instance, generating an instance with all the empty fields
			given in the MeshStructure class.

			.. code-block::

				Msh = MeshHandling()

			Once such a void instance has been creates, the only possible use of this class is to load
			a mesh to populate the empty fields.

			.. code-block::

				Msh.ReadTxtMesh(MeshName = "Name", MeshOrder = 3, MeshResolution = 10, MeshDofsType = "DG")
				Msh.LoadMesh   (MeshName = "Name", MeshOrder = 3, MeshResolution = 10, MeshDofsType = "DG")

			Rather than loading the mesh afterwards, one can load it upon the instance creation.
			When a single argument is given, it should be a MeshStructure type variable

			.. code-block::

				Msh = MeshHandling(mesh)
				# The mesh is automatically copied in the HandlingStructure.

			When four arguments are given, a corresponding mesh file should exist in the Pathes.MeshPath folder,
			either in a binary pyMsh or in raw text file Msh format.

			.. code-block::

				# The mesh contained in the file "MeshFolder/Name_o2_DG_res_10.pyMsh" will be loaded.
				# If it does not exist, it tries to load "MeshFolder/Name_o2_DG_res_10.msh" and creates the
				# corresponding binary file automatically.
				Name       = "Name"
				Order      = 2
				DofsTypes  = "DG"
				Resolution = 10
				Msh = MeshHandling(Name, Order, DofsTypes, Resolution)


		|

		.. note::
			Once a mesh is loaded, its properties (element's normals, width, etc) are automatically extracted

		|

		.. admonition:: Using the mesh within the structure

			Complete the mesh properties (cells volume, diameter, normals,
			automatically called when the instance is created upon argument(s))

			.. code-block::

				Msh.ExtractMeshProperties()

			Export the mesh in a Binary pickle format associated to the MeshStructure class
			(in general, the mesh properties are not exported, this routine is generally used after a fresh load
			of a text-file containing the raw mesh information to generale a binary one, before any property extraction)

			.. code-block::

				Msh.ExportBinaryMesh()

			Export the mesh in a custom .Msh text format

			.. code-block::

				Msh.ExportTextMesh()

			Export the mesh in a Gmsh-2.16 format (only for tringles up to order 5, quads up to order 2 and Lagrange elements):

			.. code-block::

				Msh.ExportGmshFormatMesh()

			Plot the mesh (save it in a png file, not showing it):

			.. code-block::

				Msh.PlotMesh()

			To plot the numbering of the local element (shows it, no save it):

			.. code-block::

				# Select the element you want to information for
				i = 50
				# Plot the numbering details in this element
				Msh.PlotMeshElementDetails(i)

			To extract the mesh itself in a variable without carrying the full :code:`MeshHandling` structure,
			consider only the field :code:`Mesh`:

			.. code-block::

				MeshItself = Msh.Mesh
				# Or directly
				MeshItself = MeshHandling(MeshName,  MeshOrder, MeshType, MeshResolution).Mesh

		|

		.. note::

			Each element of the mesh stored in a .pyMsh or .Msh format should be given by an ordered list of vertices,
			but the orientation is not impacting. However, if some elements are given counter-clockwise, the export to gmsh2-2
			will lead to unwished results, at least in the gmsh display.

		"""

	### Initialisation routine ####
	def __init__(self, *args):
		"""
		.. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

		|
		.. rubric::	**Initialisation**

		The initialisation loads the mesh within the structure and extracts its properties (normals, elements' width etc).
		Two configurations of arguments are allowed; either furnishing a mesh of a Mesh structure type or all the attributes
		allowing to load a mesh from an external mesh file.

		|

		.. rubric::	 **Parameters**

		|bs| |bs| **args** *(list)* -- Variable length list of arguments, their type corresponding to their number as follows.

			Option 1:
				- |bs| **args** *(MeshStructure)* -- A mesh of MeshStructure class

			Option 2:
				- |bs| **args** *(string)* -- A file name storing a mesh, either finishing in ".pyMsh" (binary) or ".Msh" (text file).

			Option 3:
				- |bs| **args[0]** *(string)*  -- Mesh name
				- |bs| **args[1]** *(integer)* -- Mesh order
				- |bs| **args[2]** *(string)*  -- types of degrees of freedom in the mshr fenics format
				- |bs| **args[3]** *(integer)* -- Mesh resolution in the mshr fenics format or equivalent integer represenation
		|
		|

		.. rubric:: **Methods**

		"""

		# ---------- Initialising the mesh structure --------------------
		self.Mesh = MeshStructure()

		# ----- Filling the mesh in the inner Handling structure --------
		# Loading a mesh from a given text file
		if len(args) == 1 and type(args[0])==str and ".Msh" in args[0]:
			self.ReadTxtMesh(FileName = args[0])
			self.ExtractMeshProperties()
			self.ExtractVariationalProperties()

		# Loading a mesh from a given binary file
		elif len(args) == 1 and type(args[0])==str and ".pyMsh" in args[0]:
			self.LoadMesh(FileName = args[0])
			self.ExtractMeshProperties()
			self.ExtractVariationalProperties()

		# Copying directly the mesh in the structure
		elif len(args) == 1:
			self.Mesh = args[0]
			self.ExtractMeshProperties()
			self.ExtractVariationalProperties()

		# If a file is to be loaded from its specificities, filling the above created elements
		elif len(args) == 4:
			self.Mesh.MeshName       = args[0]
			self.Mesh.MeshOrder      = args[1]
			self.Mesh.MeshDofsType   = args[2]
			self.Mesh.MeshResolution = args[3]

			# Trying to load the mesh from binary file
			try:
				self.LoadMesh()
				self.ExtractMeshProperties()
				self.ExtractVariationalProperties()

			# If no binary file, try to load it from text file and generate binary file
			except:
				try:
					self.ReadTxtMesh()
					self.ExportBinaryMesh()
					self.ExtractMeshProperties()
					self.ExtractVariationalProperties()

				# If neither files were found
				except:
					sys.tracebacklimit = None
					raise NameError("Neither txt or npy MeshFile found, or meshfile in a wrong format. Check your problem definition. Abortion.")

		# If a void structure is created
		elif len(args) == 0:
			print("WARNING: Initialising a void MeshStructure. Please load a mesh afterwards.")

		# In case of wrong number of input arguments in the class
		else:	raise NotImplementedError("Wrong number of arguments in the mesh generation routine. Check your problem definition. Abortion.")

	### Reads the Mesh as a numpy structure from filename ####
	def LoadMesh(self, **kwargs):
		"""Reads the Mesh as a numpy structure from filename. All the arguments are keyword
		arguments and are optional.
		If the keyword "FileName" is given, the mesh will be loaded from the given file.
		If another keyword is given, it will overrite the parameter already
		registered in the structure (during the init phase), convenient for reloading a new mesh
		when changing only one parameter.

		Args:
			"FileName"  (string): the mesh file to load the mesh from in a text format
			"MeshName"  (string): the name of the mesh to be loaded
			"MeshOrder" (integer): the order of the mesh to be loaded
			"MeshResolution" (integer):  the resolution of the mesh to be loaded
			"MeshDofsType" (string): the type of degrees of freedom in a fenics nomenclature

		Returns:
			None: Populates or overwrites the fields of the instance.
		"""

		# ----------- Registering the target mesh options ----------------------
		# Saving the preregistered values and setting LoadFromFile to false
		FileName = ""
		Name     = self.Mesh.MeshName
		Order    = self.Mesh.MeshOrder
		Type     = self.Mesh.MeshDofsType
		Resolution   = self.Mesh.MeshResolution
		LoadFromFile = False;

		# Overriting depending on the optional items desired by the user
		for key, value in kwargs.items():
			if key == "FileName":       LoadFromFile = True; FileName = value
			if key == "MeshName":       Name  = value
			if key == "MeshOrder":      Order = value
			if key == "MeshResolution": Resolution = value
			if key == "MeshDofsType":   Type = value

		# ---------- Reloading a new mesh --------------------------------------
		# Creating the file name
		if not LoadFromFile: FileName = os.path.join(Pathes.MeshPath, Name + "_o{0:1d}_{1!s}_res_{2:02d}.pyMsh".format(Order, Type, Resolution))

		# Loading the mesh from a binary format
		Mesh = MeshStructure()
		Mesh = np.load(FileName, allow_pickle=True)
		self.Mesh = Mesh

	### Reads the Mesh as a textfile from filename ####
	def ReadTxtMesh(self, **kwargs):
		"""Reads the Mesh as a textfile from filename. All the arguments are keyword
		arguments and are optional. If a keyword is given, it will overrite the parameter already
		registered in the structure (during the init phase), convenient for reloading a new mesh when changing only one parameter

		Args:
			"MeshName"  (string): the name of the mesh to be loaded
			"MeshOrder" (integer): the order of the mesh to be loaded
			"MeshResolution" (integer):  the resolution of the mesh to be loaded
			"MeshDofsType" (string): the type of degrees of freedom in a fenics nomenclature

		Returns:
			None: Populates or overwrites the fields of the instance.
		"""

		# ----------- Registering the target mesh options ----------------------
		# Saving the preregistered values and setting LoadFromFile to false
		FileName = ""
		Name     = self.Mesh.MeshName
		Order    = self.Mesh.MeshOrder
		Type     = self.Mesh.MeshDofsType
		Resolution   = self.Mesh.MeshResolution
		LoadFromFile = False;

		# Overriting depending on the optional items desired by the user
		for key, value in kwargs.items():
			if key == "FileName":       LoadFromFile = True; FileName = value
			if key == "MeshName":       Name  = value
			if key == "MeshOrder":      Order = value
			if key == "MeshResolution": Resolution = value
			if key == "MeshDofsType":   Type = value

		# ---------- Reloading a new mesh --------------------------------------
		# Creating the file name and opening the mesh file
		if not LoadFromFile: FileName = os.path.join(Pathes.MeshPath, Name + "_o{0:1d}_{1!s}_res_{2:02d}.pyMsh".format(Order, Type, Resolution))
		MeshFile = open(FileName, "r")

		# Loading the full file content in a single list of strings
		MeshContent = list(map(str.strip, MeshFile.readlines()))

		# ---------- Reading and splitting the mesh-properties line-wise -------
		# Initialise the mesh structure
		Mesh = MeshStructure()

		# Mesh name
		Mesh.MeshName =  Name

		# Mesh element size parameters (the hmax will be overwriten when exporting the mesh properties)
		specs = list(filter(None, MeshContent[2].split("\t")))
		Mesh.MeshResolution, Mesh.hmax = int(specs[0]), float(specs[1])

		# Getting the global properties of the mesh
		Dofs = list(filter(None,MeshContent[3].split("\t")))
		Mesh.MeshOrder, Mesh.MeshDofsType = int(Dofs[0]), Dofs[1]
		Mesh.MeshType     = list(map(int, filter(None,MeshContent[4].split(" "))))
		Mesh.Convexity    = int(MeshContent[5])
		Mesh.BdConformity = int(MeshContent[6])

		# Getting the number of the various quantities
		CurrLine = 7
		Mesh.NbMeshPoints       = int(MeshContent[CurrLine + 2])
		Mesh.NbInnerElements    = int(MeshContent[CurrLine + 3])
		Mesh.NbBoundaryElements = int(MeshContent[CurrLine + 4])
		Mesh.NbDofs             = int(MeshContent[CurrLine + 5])
		Mesh.NbBoundaryDofs     = int(MeshContent[CurrLine + 6])

		# Extracting the coordinates of the vertices
		CurrLine = CurrLine + 9
		NextLine = CurrLine + Mesh.NbMeshPoints
		Mesh.CartesianPoints      = np.zeros((Mesh.NbMeshPoints, 2))
		Mesh.CartesianPoints[:,:] = [list(map(float, a.split(" "))) for a in MeshContent[CurrLine:NextLine]]

		# Extracting the coordinates of the dofs
		CurrLine = CurrLine + 2
		NextLine = CurrLine + Mesh.NbDofs
		Mesh.DofsXY      = np.zeros((Mesh.NbDofs, 2))
		Mesh.DofsXY[:,:] = [list(map(float, a.split(" "))) for a in MeshContent[CurrLine:NextLine]]

		# Retrieving the 2D elements and their neighbors
		CurrLine = NextLine + 2
		NextLine = CurrLine + Mesh.NbInnerElements
		buff = [list(map(int, filter(None,re.split(' |\\|',a)))) for a in MeshContent[CurrLine:NextLine]]
		Mesh.InnerElements = [a[1:a[0]+1] for a in buff]
		Mesh.Neighborhoods = [a[a[0]+1:2*a[0]+1] for a in buff]

		# Retrieving the domain's boundary elements (1D) and their neigbors
		CurrLine = NextLine + 2
		NextLine = CurrLine + Mesh.NbBoundaryElements
		buff = [list( map(int, filter(None,re.split(' |\\|',a)))) for a in MeshContent[CurrLine:NextLine]]
		Mesh.BoundaryElements    = [a[0:2] for a in buff]
		Mesh.BoundaryElementsTag = [a[2] for a in buff]

		# Retrieving the vertices lying on the domain's boundary
		CurrLine = NextLine + 2
		NextLine = CurrLine + Mesh.NbBoundaryElements
		buff = [list( map(int, filter(None,re.split(' |\\|',a)))) for a in MeshContent[CurrLine:NextLine]]
		Mesh.BoundaryVertices    = [a[0] for a in buff]
		Mesh.BoundaryVerticesTag = [a[1] for a in buff]
		Mesh.BoundaryVerticesMap = dict(zip(Mesh.BoundaryVertices, Mesh.BoundaryVerticesTag))

		# Retrieving the Dofs of each 2D element
		CurrLine = NextLine + 2
		NextLine = CurrLine + Mesh.NbInnerElements
		buff = [list(filter(None,re.split('\\|',a))) for a in MeshContent[CurrLine:NextLine]]
		Mesh.ElementsInnerDofs        = [list(map(int, filter(None,re.split(' ',a[1])))) for a in buff]
		Mesh.ElementsBoundaryWiseDofs = [[list(map(int, filter(None, re.split(' ', b)))) for b in a[2:]] for a in buff ]
		Mesh.ElementsBoundaryDofs = [[]]*Mesh.NbInnerElements
		for (i,dofs) in enumerate(Mesh.ElementsBoundaryWiseDofs):
			Flattened = np.array([v for a in dofs for v in a])
			_, Idx    = np.unique(Flattened, return_index = True)
			Mesh.ElementsBoundaryDofs[i] = Flattened[np.sort(Idx)].tolist()

		# Retrieiving the Dofs lying on the boundary elements
		CurrLine = NextLine + 2
		NextLine = CurrLine + Mesh.NbBoundaryElements
		buff = [list( map(int, filter(None,re.split(' ',a)))) for a in MeshContent[CurrLine:NextLine]]
		Mesh.BoundaryElementsDofs = [a for a in buff]

		# Retrieve a direct access to the dofs, element-wise
		Mesh.ElementsDofs = [[]]*Mesh.NbInnerElements
		for i in range(Mesh.NbInnerElements): Mesh.ElementsDofs[i] = Mesh.ElementsBoundaryDofs[i] + Mesh.ElementsInnerDofs[i]

		# Retrieving the global access to the boundary Dofs, not binded by elements
		CurrLine = NextLine + 2
		NextLine = CurrLine + Mesh.NbBoundaryDofs
		buff = [list( map(int, filter(None,re.split(' |\\|',a)))) for a in MeshContent[CurrLine:NextLine]]
		Mesh.BoundaryDofs    = [a[0] for a in buff]
		Mesh.BoundaryDofsTag = [a[1] for a in buff]
		Mesh.BoundaryDofsMap = dict(zip(Mesh.BoundaryDofs, Mesh.BoundaryDofsTag))

		# Saving it in the internal Structure
		self.Mesh = Mesh

	### Extracting the basic properties of each inner element ####
	def ExtractMeshProperties(self):
		"""
		.. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

		Extracting the basic properties of each inner element

		.. warning:: The boundaryElements list should not be empty

		Args:
			None: A freshly loaded mesh in the mesh structure

		.. rubric:: **Returns**

		|bs| |bs| *None* -- Edits the mesh which is already given within the structure,
		filling the fields

			**InnerElementsVolume**  -- List of the element's volumes

			**InnerElementsDiameter**  -- List of the element's volumes

			**ElementsBoundaryWiseNormals** -- Nested list of the element's normal to faces

			**ElementsDofs** -- List of the degrees of freedom of each element (numbering different from their vertices, even in P1/B1)
		"""

		# ------------- Initilising the four new fields -----------------------
		self.Mesh.InnerElementsVolume   = np.zeros(self.Mesh.NbInnerElements)
		self.Mesh.InnerElementsDiameter = np.zeros(self.Mesh.NbInnerElements)
		self.Mesh.ElementsBoundaryWiseNormals = [[]]*self.Mesh.NbInnerElements
		self.Mesh.ElementsBoundaryWiseWidth   = [[]]*self.Mesh.NbInnerElements

		# ------------- Computing the mesh properties element-wise -------------
		# Spanning the inner elements to get their volume and their diameter
		for (i, ele) in enumerate(self.Mesh.InnerElements):
			# Get the coordinates of the vertices
			xy = self.Mesh.CartesianPoints[ele,:]

			# Assigning the flip parameter to compute the outer normal regardless the element's orientation
			ori = 1
			if not ShapelyLinearRing([tuple(a) for a in xy]).is_ccw: ori = -1

			# Computing the area of the element either by the shoelace formula or the shapely tools
			#area = 0.5*np.abs(np.dot(xy[:-1,0], xy[1:,1]) - np.dot(xy[:-1,1], xy[1:,0]) + xy[-1,0]*xy[0,1] - xy[-1,1]*xy[0,0])
			#self.Mesh.InnerElementsVolume[i] = area
			Pp = ShapelyPolygon([tuple(a) for a in xy]).area
			self.Mesh.InnerElementsVolume[i] = Pp

			# Computing the maximum spacing between two vertices of the element
			diam = 0
			for j in range(len(xy[:,0])):
				for k in range(0,j):
					diam = max(diam, np.linalg.norm(xy[j,:]-xy[k,:],2))
			self.Mesh.InnerElementsDiameter[i] = diam

			# Computing the outward normal of the element
			XY = np.append(xy,[xy[0,:]],axis=0)
			normals = np.zeros((len(xy[:,0]),2))
			widths  = np.zeros((len(xy[:,0]),1))
			for j in range(len(xy)):
				normals[j,:] = (ori*(-XY[j+1,:]+XY[j,:])[::-1])*np.array([-1,1])
				widths[j]    =  np.linalg.norm(-XY[j+1,:]+XY[j,:],2)
			self.Mesh.ElementsBoundaryWiseNormals[i] = normals
			self.Mesh.ElementsBoundaryWiseWidth[i]   = widths

		# --------- Computing the boundary rings and sorting them to find the outer and inners boundary rings -------------
		# This does not support completely disconnected domains, and is not optimized

		# --- Creating the linear rings
		# Gathering all the boundary elements (and take care of python closures)
		BdElements = copy.deepcopy(self.Mesh.BoundaryElements)

		# Check if we can actually perform the extraction of the boundary rings or not
		if not BdElements:	raise(ValueError("No boundary element registered. Cannot find the outer boundary ring. Abortion."))

		# Initialising the local rings list and buffers
		LinearRingsList   = []
		CollectedVertices = []
		RingVertices      = []

		# Initialise the first ring by the first vertex of the boundary element
		CurrentElement = BdElements[0]
		CurrentVertex  = CurrentElement[0]
		BdElements.remove(BdElements[0])

		# Span the boundary elements while all the boundary points are not collected until all the
		# rings are formed. WARNING: if the boundary elements are not forming close lines then it will crash
		j=0
		while j<self.Mesh.NbBoundaryElements:		# Just for safety, not practically doing anything
			j=j+1

			# If the construction of the current ring is now closed and there is no more
			# non-assigned boundary elements, then perform the end operations
			if not BdElements and \
				(  [RingVertices[0], CurrentVertex] in self.Mesh.BoundaryElements \
				or [CurrentVertex, RingVertices[0]] in self.Mesh.BoundaryElements):
				# Add the current ring to the boundary ring list
				LinearRingsList   = LinearRingsList   + [RingVertices]
				# Enrich the list of already collected vertices
				CollectedVertices = CollectedVertices + RingVertices
				break

			# Check if the BdElement is valid, and if so continuing the ring construction
			if np.shape(np.array(BdElements).T)[0] == 2:
				# Get the next boundary element connected to this one
				Idx, Idy = np.where(np.array(BdElements) == CurrentVertex)

				# There should be only one or none (end of the loop) index
				if len(Idx)>1:
					raise(ValueError("Boundary vertex shared by more than two boundary elements. Abortion"))

				# Case where the ring is to be continued
				elif len(Idx)==1:
					# Register the connected vertex to the current ring
					RingVertices = RingVertices + [CurrentVertex]

					# Locate the Cursor to the next element (the connected vertex: complement to Idx)
					# And flipping the current element to follow the spanning order
					CurrentElement = BdElements[Idx[0]]
					CurrentVertex  = CurrentElement[1-Idy[0]]

					# Remove the element that is currently spanned from the BdElements
					BdElements.remove(CurrentElement)

				# If none index, check that indeed, we are closing the loop
				elif len(Idx)==0:
					# If we can indeed close the ring (the last vert would form an element with the first registered vertex)
					if (   [RingVertices[0], CurrentVertex] in self.Mesh.BoundaryElements \
						or [CurrentVertex, RingVertices[0]] in self.Mesh.BoundaryElements):

						# Close the ring and register it as a boundary
						LinearRingsList   = LinearRingsList   + [RingVertices]
						# Enrich the list of already collected vertices
						CollectedVertices = CollectedVertices + RingVertices

						# Jump to the next arbitrary beginning of the ring
						CurrentElement = BdElements[0]
						CurrentVertex = CurrentElement[0]

						# Jump to another ring
						RingVertices = []

						# Remove the element that is currently spanned from the BdElements
						BdElements.remove(CurrentElement)

					else:
						raise(ValueError("Error in closing the boundary ring. Abortion."))
			else:
				raise(ValueError("Encountered an invalid boundaryElement in the boundary ring creation. Abortion."))

		# --- Determining whose linear ring is the outerboundary (nested domains are not supported)
		# Construct the polygons list out of the rings
		BoundaryPolygonList =  [self.Mesh.CartesianPoints[Ring, :].tolist() for Ring in LinearRingsList]

		# Sorting the polygons by their areas: by assumptions, the polygon defining the bigger aera
		# will define the outer boundary. It is assumed that the smaller ones form holes (not checked)
		PolygonAreas = [ShapelyPolygon([tuple(Point) for Point in Poly]).area for Poly in BoundaryPolygonList]
		Index        = np.argsort(PolygonAreas)[::-1]

		# Sorting the list of domains in the order [external, biggest internal hole, ..., smallest internal hole]
		self.Mesh.Domain = np.array(BoundaryPolygonList, dtype=object)[Index].tolist()

	### Extracting the variational properties of the mesh ####
	def ExtractVariationalProperties(self):
		""".. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

		Extracting the variational properties of the mesh.

		.. warning:: The boundaryElements list should not be empty

		Args:
			None: A freshly loaded mesh in the mesh structure

		.. rubric:: **Returns**

		|bs| |bs| *None* -- Edits the mesh which is already given within the structure by filling the fields

			**Dof2Vertex** --|bs|		 correspondance table from global Dof numbering to the corresponding Vertex global index

			**Vertex2Dofs** --  correspondance table from global Vertex numbering to the corresponding
			Dofs global indices (in case of DG, mapping to several Dofs)

			**TabulatedVertex** -- Gives locally the correspondance table from Vertex to Dof

			**VertexLocation** -- Give locally the position of the vertices in the list of Dofs

			**TabulatedDofs** -- Gives locally the correspondance table from Dof to Vertex

			**Dof2VertexLoc** -- Same as Dof2Vertex, but locally to an element

			**Vertex2DofLoc** -- Same as Vertex2Dofs, but locally to an element
		"""

		# --------------- Initialisations --------------------------------------------
		# Shortcutting the cartesian coordinates of the physical vertices and the Dofs
		DofsXY = self.Mesh.DofsXY[:,:]
		VertXY = self.Mesh.CartesianPoints[:,:]

		# Creating the new fields
		self.Mesh.Vertex2Dofs = [[]]*self.Mesh.NbMeshPoints
		self.Mesh.Dofs2Vertex = [[]]*self.Mesh.NbDofs

		self.Mesh.TabulatedVertex = [[]]*self.Mesh.NbInnerElements
		self.Mesh.VertexLocation  = [[]]*self.Mesh.NbInnerElements
		self.Mesh.TabulatedDofs   = [[]]*self.Mesh.NbInnerElements
		self.Mesh.Dof2VertexLoc   = [[]]*self.Mesh.NbInnerElements
		self.Mesh.Vertex2DofLoc   = [[]]*self.Mesh.NbInnerElements

		# --------------- Computations of the global mapping -------------------------
		# Computing the global mapping between the Dofs index and the vertex ones.
		# NOTE: The two mappings have been separated as it is possible to have high order schemes
		# where internal Dofs are not associated to vertices and vertices not corresponding to Dofs

		# --- Mapping the dof to the vertex
		for i in range(self.Mesh.NbDofs):
			# Retrieve the correspondance map w.r.t. the cartesian xy location
			Coin = np.all(np.isclose(DofsXY[i,:], VertXY), axis=1)
			vertex = np.where(Coin)[0]

			# Check if only one vertex got selected
			if   len(vertex) == 1: self.Mesh.Dofs2Vertex[i] = vertex[0]
			elif len(vertex) == 0: self.Mesh.Dofs2Vertex[i] = None
			else: raise ValueError("""Mesh error: a same Dof has been assigned to several vertices.\n
					                            Invalid Mesh. Abortion.""")

		# --- Mapping the vertex to dofs
		for i in range(self.Mesh.NbMeshPoints):
			# Retrieve the correspondance map w.r.t. the cartesian xy location
			Coin = np.all(np.isclose(VertXY[i,:], DofsXY), axis=1)
			dof = np.where(Coin)[0]

			# In case of discontinuous variational setting, it is possible to have several Dofs
			# at one vertex. The above check is thus not necessary and we register the output  as a list.
			self.Mesh.Vertex2Dofs[i] = np.array(dof)

		# --------------- Computations of the local mapping ------------------------------------
		# For each element, furnish the local mapping between dofs and vertex
		# and furnish the vertices in the order associated to the dof ordering (in view of computing
		# the barycentric coordinates from them)
		for e in range(self.Mesh.NbInnerElements):
			# Create the local correspondance table from the previous exported global one
			self.Mesh.Dof2VertexLoc[e] = np.array([self.Mesh.Dofs2Vertex[p] for p in self.Mesh.ElementsDofs[e]])
			self.Mesh.Vertex2DofLoc[e] = np.array([self.Mesh.Vertex2Dofs[p] for p in self.Mesh.InnerElements[e]])

			# Select the location of the vertices depending on the type of the element
			if self.Mesh.MeshDofsType in ['DG', 'CG'] and self.Mesh.MeshOrder>0:
				self.Mesh.VertexLocation[e] = np.array([i*(self.Mesh.MeshOrder) for i in range(len(self.Mesh.InnerElements[e]))])
			else:
				self.Mesh.VertexLocation[e] = np.array([0, 1, 2])

			# (Crucial for CG/DG) Tabulate the Dofs from their local index to make the order of the Dofs correspond to the order
			# of the physical vertices
			self.Mesh.TabulatedDofs[e]   = np.array(list([np.where(self.Mesh.Dof2VertexLoc[e][self.Mesh.VertexLocation[e]] == p)[0][0]\
				                                          for p in self.Mesh.InnerElements[e]]))
			self.Mesh.TabulatedVertex[e] = np.argsort(self.Mesh.TabulatedDofs[e])

	#### Export the obtained the best mesh file ####
	def ExportTextMesh(self):
		"""Export the obtained mesh file

		Args:
			None: The mesh structure stored within the instance of mesh

		Returns:
			None: Exports the mesh files into the same folder as the run code + mesh prefix given in :code:`Pathes.MeshPath`.
			The variable self.Meshes is exported in a human readable version in :code:`ExportFile+'.Msh'`.
		"""

		# Creating the file name
		TextFileName   = os.path.join(Pathes.MeshPath, self.Mesh.MeshName + "_o{0:1d}_{1!s}_res_{2:02d}.Msh".format(self.Mesh.MeshOrder, self.Mesh.MeshDofsType, self.Mesh.MeshResolution))

		# Exporting the mesh infos
		ExportContent  = "# Mesh Type\n"
		ExportContent += self.Mesh.MeshName+"\n"
		ExportContent += "{0:d}\t{1:8.6f}\n".format(self.Mesh.MeshResolution, self.Mesh.hmax)
		ExportContent += "{0:d}\t{1:s}\n".format(self.Mesh.MeshOrder, self.Mesh.MeshDofsType)
		ExportContent += ("{:d}\n"*len(self.Mesh.MeshType)).format(*self.Mesh.MeshType)
		ExportContent += "{0:d}\n{1:d}\n\n".format(self.Mesh.Convexity, self.Mesh.BdConformity)

		ExportContent += "# Numbers of points and elements\n"
		ExportContent += "{0:d}\n{1:d}\n{2:d}\n".format(self.Mesh.NbMeshPoints, self.Mesh.NbInnerElements, self.Mesh.NbBoundaryElements)
		ExportContent += "{0:d}\n{1:d}\n\n".format(self.Mesh.NbDofs, self.Mesh.NbBoundaryDofs)

		ExportContent += "# Physical Cartesian Points\n"
		ExportContent += "\n".join(["{0:16.15f} {1:16.15f}".format(*self.Mesh.CartesianPoints[j,:]) for j in range(self.Mesh.NbMeshPoints)])+"\n\n"

		ExportContent += "# Dofs cartesian Points\n"
		ExportContent += "\n".join(["{0:16.15f} {1:16.15f}".format(*self.Mesh.DofsXY[j,:]) for j in range(self.Mesh.NbDofs)])+"\n\n"

		ExportContent += "# Inner elements types, vertices and Neighborhoods\n"
		ExportContent += "\n".join([ \
						 "{0:d} | ".format(len(self.Mesh.InnerElements[j]))+\
						 ("{:d} "*len(self.Mesh.InnerElements[j]) + "| "+ "{:d} "*len(self.Mesh.Neighborhoods[j]))\
						 .format(*self.Mesh.InnerElements[j], *self.Mesh.Neighborhoods[j])\
						 for j in range(self.Mesh.NbInnerElements)])

		ExportContent += "\n\n# Boundary elements and tag\n"
		ExportContent += "\n".join(["{0:d} {1:d} {2:d}".format(*self.Mesh.BoundaryElements[j], self.Mesh.BoundaryElementsTag[j]) for j in range(self.Mesh.NbBoundaryElements)])+"\n\n"

		ExportContent += "# Boundary vertices index and tag\n"
		ExportContent += "\n".join(["{0:d} {1:d}".format(self.Mesh.BoundaryVertices[j], self.Mesh.BoundaryVerticesTag[j]) for j in range(self.Mesh.NbBoundaryElements)])+"\n\n"

		ExportContent += "# Inner Elements' inner Dofs, Inner elements' boundary Dofs\n"
		ExportContent += "\n".join([ \
						 "{0:d} | ".format(len(self.Mesh.InnerElements[j])) +\
						 ("{:d} "*len(self.Mesh.ElementsInnerDofs[j])+"| " + "| ".join(''.join("{:d} "*len(self.Mesh.ElementsBoundaryWiseDofs[j][i])) for i in range(len(self.Mesh.ElementsBoundaryWiseDofs[j])))) \
						 .format(*self.Mesh.ElementsInnerDofs[j], *[k for i in self.Mesh.ElementsBoundaryWiseDofs[j] for k in i])\
						 for j in range(self.Mesh.NbInnerElements)])

		ExportContent += "\n\n# Boundary elements Dofs\n"
		ExportContent += "\n".join([("{:d} "*len(self.Mesh.BoundaryElementsDofs[j])).format(*self.Mesh.BoundaryElementsDofs[j]) for j in range(self.Mesh.NbBoundaryElements)])+"\n"

		ExportContent += "\n# Boundary Dofs and tags\n"
		ExportContent += "\n".join(["{0:d} {1:d}".format(self.Mesh.BoundaryDofs[j], self.Mesh.BoundaryDofsTag[j]) for j in range(self.Mesh.NbBoundaryDofs)])+"\n"

		Export = open(TextFileName, 'w')
		Export.write(ExportContent)
		Export.close()

	#### Export the obtained the best mesh file ####
	def ExportBinaryMesh(self):
		"""
		Export the obtained the best mesh file

		Args:
			None: The mesh structure stored within the instance of mesh

		Returns:
			None: Exports the mesh files into the same folder as the run code + mesh prefix given in :code:`Pathes.MeshPath`.
			The variable self.Meshes is exported in a numpy binary :code:`.pyMsh` file.
		"""

		BinaryFileName = os.path.join(Pathes.MeshPath, self.Mesh.MeshName + "_o{0:1d}_{1!s}_res_{2:02d}.pyMsh".format(self.Mesh.MeshOrder, self.Mesh.MeshDofsType, self.Mesh.MeshResolution))
		pickle.dump(self.Mesh, open(BinaryFileName, "wb"))

	#### Export the mesh in a Gmsh2-2 format ####
	def ExportGmshFormatMesh(self):
		"""Export the mesh in a Gmsh2-2 format.
		Due to Gmsh2-2 format, the export is only possible for quadrangles up to order 2
		and triangles up to order 5. Only accepts Lagrange elements so far.

		Args:
			None: The mesh structure stored within the instance of mesh

		Returns:
			None: Exports the mesh files into the same folder as the run code + mesh prefix given in :code:`Pathes.MeshPath`.
			The variable self.Meshes is exported in a gmsh-2.2 version in :code:`ExportFile+'.msh'`.
		"""

		# Creating the file name
		TextFileName   = os.path.join(Pathes.MeshPath, self.Mesh.MeshName + "_o{0:1d}_{1!s}_res_{2:02d}_gmsh.msh".format(self.Mesh.MeshOrder, self.Mesh.MeshDofsType, self.Mesh.MeshResolution))

		# Exporting the mesh infos
		ExportContent  = "$MeshFormat\n"
		ExportContent += "2.2 0 8\n"
		ExportContent += "$EndMeshFormat\n"

		# Exporting the boundary labels and related tags
		RelevantTags   = set(self.Mesh.BoundaryElementsTag)
		RelevantTags.discard(0) 							# Removing the non-tagged elements from the physical names to export
		ExportContent += "$PhysicalNames\n"
		ExportContent += "{0:d}\n".format(len(RelevantTags))
		ExportContent += "\n".join(["1 {0:d} \"{1!s}\"".format(i, i) for i in RelevantTags])
		ExportContent += "\n$EndPhysicalNames\n"

		# Exporting the number of nodes (Dofs only) and their cartesian coordinates
		ExportContent += "$Nodes\n"
		ExportContent += "{0:d}\n".format(self.Mesh.NbDofs)
		ExportContent += "\n".join(["{0:d} {1:16.15f} {2:16.15f} 0".format(i+1, *self.Mesh.DofsXY[i,:]) for i in range(self.Mesh.NbDofs)])
		ExportContent += "\n$EndNodes\n"

		# Shifting variable for the Gmsh numberring of the Dofs as here the physical vertices are disregarded
		Sht = -1 # To catch up the shift with the indexing starting at 0 in python

		# Exporting the total number elements
		ExportContent += "$Elements\n"
		ExportContent += "{0:d}\n".format(self.Mesh.NbBoundaryElements+self.Mesh.NbInnerElements) #

		# Exporting the boundary elements
		MappingGmshEdgeType = {1:1, 2:8, 3:26, 4:27, 5:28}
		ExportContent += "\n".join(["{0:d} {1:d} {2:d} {3:d} {4:d}".format(i+1, MappingGmshEdgeType[self.Mesh.MeshOrder], 2, 1, self.Mesh.BoundaryElementsTag[i])+ \
		                 (" {:d}"*len(self.Mesh.BoundaryElementsDofs[i])).format(np.array(self.Mesh.BoundaryElementsDofs[i][0])-Sht, np.array(self.Mesh.BoundaryElementsDofs[i][-1])-Sht, *(np.array(self.Mesh.BoundaryElementsDofs[i][1:-1])-Sht)) for i in range(self.Mesh.NbBoundaryElements)])

		# Getting the Gmsh type of the inner elements (Order, NbEdge), only maps to Lagrange elements so far
		MappingGmshEdgeType = {(1,3):2, (2,3):9, (3,3):21, (4,3):23, (5,3):25,\
		                       (1,4):3, (2,4):10}

		# Converting to Gmsh format
		Sht = -1

		for i in range(self.Mesh.NbInnerElements):
			DofVertexBuff  = []
			DofBdInnerBuff = []
			ExportContent += "\n{0:d} {1:d} {2:d} {3:d} {4:d}".format(i+1, MappingGmshEdgeType[(self.Mesh.MeshOrder, len(self.Mesh.InnerElements[i]))], 2, 0, 6)
			for ele in self.Mesh.ElementsBoundaryWiseDofs[i]:
				DofVertexBuff  += [ele[0]]
				DofBdInnerBuff += ele[1:-1]
			ExportContent += (" {:d}"*(len(DofVertexBuff)+len(DofBdInnerBuff)+len(self.Mesh.ElementsInnerDofs[i]))).format(*(np.array(DofVertexBuff)-Sht), *(np.array(DofBdInnerBuff)-Sht), *(np.array(self.Mesh.ElementsInnerDofs[i])-Sht))
		ExportContent += "\n$EndElements\n"

		# Export the content in a text file
		Export = open(TextFileName, 'w')
		Export.write(ExportContent)
		Export.close()

	#### Plots the mesh given here ####
	def PlotMesh(self):
		"""
		Plots the mesh given in the structure

		Args:
			None: The (filled) mesh loaded and whose properties have been computed within the structure

		Returns:
			None: Plots the meshes and saves it as png files in :code:`Pathes.MeshPath`
		"""

		# Construct the name of the export file in the same format as the .msh itself
		PlotFileName   = os.path.join(Pathes.MeshPath, self.Mesh.MeshName + "_o{0:1d}_{1!s}_res_{2:02d}.png".format(self.Mesh.MeshOrder, self.Mesh.MeshDofsType, self.Mesh.MeshResolution))

		# Generate the colors (swaped for easing the disctinction) to tag the boundaries
		plt.figure(1)
		Hues = np.linspace(0.2, 0.8, max(self.Mesh.BoundaryElementsTag)+1).tolist()
		HuesTmp = list(chain(*[[Hues[i+2], Hues[i], Hues[i+1]] for i in range(0,(len(Hues)//3)*3,3)]))
		Hues = HuesTmp + Hues[len(HuesTmp):][::-1]

		# Plot the elements
		for ele in self.Mesh.InnerElements:
			elem = self.Mesh.CartesianPoints[ele,:]
			plt.plot(np.append(elem[:,0],elem[0,0]), np.append(elem[:,1],elem[0,1]), "-b")
			plt.fill(np.append(elem[:,0],elem[0,0]), np.append(elem[:,1],elem[0,1]), "b", alpha=0.1)

		"""# Plot the element's normals (for check purposes, do not use with fine meshes)
		for ele in range(self.Mesh.NbInnerElements):
			EdgeNormals = self.Mesh.ElementsBoundaryWiseNormals[ele]
			Verts       = self.Mesh.InnerElements[ele] + [self.Mesh.InnerElements[ele][0]]
			for edge in range(len(EdgeNormals)):
				verts  = 0.5*np.sum(self.Mesh.CartesianPoints[Verts[edge:edge+2],:], axis=0)
				normal = EdgeNormals[edge]
				plt.quiver(verts[0], verts[1], normal[0], normal[1], scale=4/self.Mesh.InnerElementsDiameter[ele])"""

		# Plot the boundary elements, with a color per boundary tag
		for (i, ele) in enumerate(self.Mesh.BoundaryElements):
			ele = self.Mesh.CartesianPoints[ele,:]
			BdTag = self.Mesh.BoundaryElementsTag[i]
			plt.plot(ele[:,0],ele[:,1], "-",color = hsv_to_rgb((Hues[BdTag], 1,1)),linewidth=2)
		plt.axis([min(self.Mesh.CartesianPoints[:,0]-0.2),max(self.Mesh.CartesianPoints[:,0]+0.2),min(self.Mesh.CartesianPoints[:,1]-0.2),max(self.Mesh.CartesianPoints[:,1]+0.2)])

		# Plot the Elements' inner Dofs
		for j in range(self.Mesh.NbInnerElements):
			for (i, ele) in enumerate(self.Mesh.ElementsInnerDofs[j]):
				ele = self.Mesh.DofsXY[ele,:]
				plt.plot(ele[0],ele[1], "cp")

		# Plot the Elements' boundary Dofs
		for j in range(self.Mesh.NbInnerElements):
			for (i, ele) in enumerate(self.Mesh.ElementsBoundaryDofs[j]):
				ele = self.Mesh.DofsXY[ele,:]
				plt.plot(ele[0],ele[1], "gp")

		# Plot the Boundary Dofs w.r.t. their Tags
		for (i, ele) in enumerate(self.Mesh.BoundaryDofs):
			ele = self.Mesh.DofsXY[ele,:]
			BdTag = self.Mesh.BoundaryDofsTag[i]
			plt.plot(ele[0],ele[1], "p", color = hsv_to_rgb((Hues[BdTag], 1,1)))


		# Plot the boundary vertices wrt to the boundary tag they are given
		for (i, ele) in enumerate(self.Mesh.BoundaryVertices):
			ele = self.Mesh.CartesianPoints[ele,:]
			BdTag = self.Mesh.BoundaryVerticesTag[i]
			plt.plot(ele[0],ele[1],"o",color = hsv_to_rgb((Hues[BdTag], 1,1)),linewidth=2)

		# Save the figure
		plt.savefig(PlotFileName)
		plt.clf()
		plt.close()

	#### Plots the mesh element details and numbering ####
	def PlotMeshElementDetails(self, i):
		# Input:  The mesh structure stored within the instance of mesh
		#          i: the index of the element to plot the information for
		# Output: None, displays the mesh element information
		# -------------------------------------------------------------------------------------------

		# -------------- Creating the figure for the numbering --------------------------------------
		# Creating the figure
		ax = plt.subplot(1,1,1)

		# Selecting the element to plot and the correponding spatial information
		Element  = self.Mesh.InnerElements[i]
		Eleverts = self.Mesh.CartesianPoints[Element,:]
		EleDofs  = self.Mesh.ElementsDofs[i]
		DofsXY   = self.Mesh.DofsXY[EleDofs,:]

		# ------------- Plot the desired quantities --------------------------------------------------
		# Plot the element itself
		plt.plot([*Eleverts[:,0], Eleverts[0,0]], [*Eleverts[:,1], Eleverts[0,1]], "-r*")
		# Plot the degrees of freedom
		plt.plot(DofsXY[:,0], DofsXY[:,1], "ob")
		# Plot the numbering of the Dofs
		for k in range(len(EleDofs)): ax.annotate("v"+str(k), (DofsXY[k,0], DofsXY[k,1]), color='blue')
		# Plot the numbering of the vertices
		for k in range(len(Element)): ax.annotate("d"+str(k), (Eleverts[k,0], Eleverts[k,1]), color='red')

		# -------------- Show the figure and element properties ---------------------------
		# Customisation
		plt.xlabel("x")
		plt.ylabel("y")
		plt.title("Numbering associated to a representative element of the mesh")
		ax.set_aspect('equal', 'box')

		# Showing off
		plt.show()
