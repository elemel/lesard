#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

"""
|

Aim of the module
*****************

Module providing the class :code:`DualMesh` extending :code:`MeshHandling`.
Given a mesh in a MeshStructure format, it computes the properties of the dual mesh
(based on the geometry, not the variational space).

For having a direct access to the class, import the module by

.. code-block:: python

    from Meshing_DualMesh import *

|

Attributes description
**********************

"""

# ==============================================================================
# Import modules
# ==============================================================================
# Import geometric modules
from shapely.geometry import Polygon    as ShapelyPolygon
from shapely.geometry import LinearRing as ShapelyLinearRing

# Import mathy modules
import numpy as np

# Import custom modules
from  Meshing.Meshing_PrimalMesh  import  MeshHandling
from  Meshing.MeshStructure       import  MeshStructure

# Import local path information
import Pathes


# ===============================================================================
#                         Dual mesh handling
# ===============================================================================

#### Class funrnishing tools for handling a mesh with its dual properties #####
class DualMesh(MeshHandling):
    """
        |

        .. rubric::	**Class purpose**

        Class that inherits from MeshHandling and that completes the MeshStructure class
        by the Dual mesh informations. The generation of the properties is automatic on
        the instance creation and enriches the MeshStructure class by the following fields

            **DualEdgesWidths** *(float numpy array)* -- the length of the segments connecting the edges mid-points with the barycenter, for each edge

            **DualNormals**     *(2D float numpy array)* --  the normal of the segments connecting the edges mid-points with the barycenter, for each edge, pointing from the upflow vertex to downflow vertex according to the shape's orientation

            **DualCenters**      *(2D float numpy array)* -- the coordinates of each edge's center, followed by the ones of the center of mass

            **DualAreas**        *(2D float numpy array)* -- the areas corresponding to each subelement attached to each vertex

            **DualAreasTot**     *(float numpy array)* -- the areas corresponding to the control cell center attached to each vertex

        |

        .. note:: Watch out not to save the DualMesh instance as a pyMsh binary file. Indeed, the pickle
                  is planned for the MeshStructure class, not its extension.

        |

        .. admonition::	**Creating a mesh instance**

            The class is inherited from :code:`MeshHandling` and does not take any further argument when initialising the
            instance. Please therefore chek the instance creation of :code:`MeshHandling` for complete details.
            However, a simple instance creation can be done by

            .. code-block::

                Msh = DualMesh(MeshName,  MeshOrder, MeshType, MeshResolution)

            To retrieve the mesh itself without carrying the full :code:`DualMesh` structure consider only the field :code:`Mesh`:

            .. code-block::

                MeshItself = DualMesh(MeshName,  MeshOrder, MeshType, MeshResolution).Mesh

        |

        .. note::
            Once a mesh is loaded, its properties (element's normals, width, etc) and dual properties (element's dual areas,
            element's barycenter and edges mid points, etc) are both automatically extracted.

        |


        .. admonition:: Using the mesh within the structure

            The loaded mesh sees its dual information extracted upon initialisation.
            If nevertheless the properties are to be extracted once more, consider the
            following command.

            .. code-block::

                Msh.ComputeDualProperties()

            All the methods from the PrimalMesh are available, however they do not consider the
            DualMesh extra fields. By example, the :code:`PlotMesh` method will not show the dual
            informations.

            .. code-block::

                Msh.PlotMesh()

            .. warning ::

                Pay attention not to use the method :code:`ExportBinaryMesh` as loading a binary exported :code:`DualMesh` as a primal mesh later may fail.
                If you created a :code:`DualMesh` instance from a mesh stored in text file, a corresponding binary file will be created
                when loading first the primal mesh solely.
        """

    #### Initialisation routine ####
    def __init__(self, *params):
        """.. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

        |

        .. rubric:: **Initialisation**

        The initialisation is identical to the one of the class :code:`MeshHandling`.
        It loads the mesh within the structure and extracts its primal and dual properties.
        Two configurations of arguments are allowed; either furnishing a mesh of a Mesh structure type or all the attributes
        allowing to load a mesh from an external mesh file.

        |

        .. rubric:: **Parameters**

        |bs| |bs| **args** *(list)* -- Variable length list of arguments, their type corresponding to their number as follows.

            Option 1:
                - |bs| **args** *(MeshStructure)* -- A mesh of MeshStructure class

            Option2:
                - |bs| **args[0]** *(string)*  -- Mesh name
                - |bs| **args[1]** *(integer)* -- Mesh order
                - |bs| **args[2]** *(string)*  -- types of degrees of freedom in the mshr fenics format
                - |bs| **args[3]** *(integer)* -- Mesh resolution in the mshr fenics format or equivalent integer represenation

        |
        |

        .. rubric:: **Methods**
        """

        # Initialising the primal mesh in a MeshStructure format
        super().__init__(*params)

        # Computing the dual mesh, enriching the MeshStructure format by the Dual mesh information
        self.ComputeDualProperties()

    #### Computing the essential properties of the associated dual mesh ####
    def ComputeDualProperties(self):
        """
        Computing the essential properties of the associated dual mesh.
        .. warning: Not valid for non-convex elements so far

        Args:
            None: A freshly loaded mesh in the mesh structure

        .. rubric:: **Returns**

        |bs| |bs| *None* -- Edits the mesh which is already given within the structure, adding the fields

                **DualEdgesWidths** *(float numpy array)* -- the length of the segments connecting the edges mid-points with the barycenter, for each edge

                **DualNormals**     *(2D float numpy array)* --  the normal of the segments connecting the edges mid-points with the barycenter, for each edge, pointing from the upflow vertex to downflow vertex according to the shape's orientation

                **DualCenters**      *(2D float numpy array)* -- the coordinates of each edge's center, followed by the ones of the center of mass

                **DualAreas**        *(2D float numpy array)* -- the areas corresponding to each subelement attached to each vertex

                **DualAreasTot**     *(float numpy array)* -- the areas corresponding to the control cell center attached to each vertex
        """

        # ----------- Initialising the new mesh's attributes -------------------
        # Initialising the new quantitites
        self.Mesh.DualEdgesWidths = [[]]*self.Mesh.NbInnerElements
        self.Mesh.DualNormals     = [[]]*self.Mesh.NbInnerElements
        self.Mesh.DualAreas       = [[]]*self.Mesh.NbInnerElements
        self.Mesh.DualCenters     = [[]]*self.Mesh.NbInnerElements
        self.Mesh.DualAreasTot    = np.zeros((self.Mesh.NbDofs,1))

        for i in range(self.Mesh.NbInnerElements):
            # ---- Element-wise initialisations
            # Getting the known properties of the current element
            NbFaces = len(self.Mesh.InnerElements[i])

            # Initialising the quantities of interest
            self.Mesh.DualEdgesWidths[i] = np.zeros((NbFaces,1))
            self.Mesh.DualNormals[i]     = np.zeros((NbFaces,2))
            self.Mesh.DualAreas[i]       = np.zeros((NbFaces,1))

            # ---- Extracting the element's properties
            # Create the circular coordinates
            XY = self.Mesh.CartesianPoints[[self.Mesh.InnerElements[i][-1], *self.Mesh.InnerElements[i],\
                                            self.Mesh.InnerElements[i][0]], :]

            # Create the corresponding shapely polygon and getting its orientation
            Poly = ShapelyPolygon([tuple(a) for a in XY[1:-1,:]])
            ori = -1
            if not ShapelyLinearRing([tuple(a) for a in XY[1:-1,:]]).is_ccw: ori = 1

            # ---- DualCenter computation
            # Precomputation of the edges's center and of the center of mass of the element's shape # WARNING: only for convex elements so far
            MidPoints = np.array([0.5*np.sum(XY[j+1:j+3], axis=0) for j in range(NbFaces)])
            self.Mesh.DualCenters[i] = np.append(MidPoints, [(Poly.centroid.coords[0])], axis = 0)

            # Computation of the face-dependent quantities
            for f in range(NbFaces):
                # Computing the normal's to the internal subsegment
                self.Mesh.DualNormals[i][f,:] = ((ori*(self.Mesh.DualCenters[i][-1,:]-self.Mesh.DualCenters[i][f,:])[::-1])*np.array([-1,1]))/(np.linalg.norm(self.Mesh.DualCenters[i][-1,:]-self.Mesh.DualCenters[i][f,:],2))

                # Computing the length of each internal subsegment
                self.Mesh.DualEdgesWidths[i][f] = np.linalg.norm(self.Mesh.DualCenters[i][-1,:]-self.Mesh.DualCenters[i][f,:], 2)

                # Computing the area of each subsection of element either by the shoelace or the shapely tool
                xy = np.array([self.Mesh.DualCenters[i][-1,:], MidPoints[f-1,:], XY[f+1,:], MidPoints[f,:]])
                #self.Mesh.DualAreas[i][f] = 0.5*np.abs(np.dot(xy[:-1,0], xy[1:,1]) - np.dot(xy[:-1,1], xy[1:,0]) + xy[-1,0]*xy[0,1] - xy[-1,1]*xy[0,0])
                Pp = ShapelyPolygon([tuple(a) for a in xy])
                self.Mesh.DualAreas[i][f] = Pp.area

                # Filling the area of the dual control cell
                if self.Mesh.ElementsBoundaryDofs[i]:
                    self.Mesh.DualAreasTot[self.Mesh.ElementsBoundaryDofs[i][f]] += self.Mesh.DualAreas[i][f]
