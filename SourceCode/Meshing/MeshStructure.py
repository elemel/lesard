#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

"""
|

Aim of the module
*****************

Module providing the class :code:`MeshStructure` furnishing a type defining a mesh compatible with the custom
pyMsh or Msh formats. A typical import to have a direct access to the class reads

.. code-block:: python

    from  MeshStructure import  MeshStructure

|

Attributes description
**********************

"""


# *******************************************************************************
#                     Primal Mesh structure
#********************************************************************************
class MeshStructure():
	"""
	|

	.. admonition::	**Class purpose**

		Class that provides a condensed information about a mesh and details all the information that
		can be retrieved from a :code:`.pyMsh` or :code:`.Msh` format.

	|

	.. note::

		The structure does not contain any method, it is here only for defining a product-type
		handler for a mesh and providing a class allowing pickles of binary meshes.

	.. admonition::	**Contained fields**

		Mesh definition, filled upon mesh load

			**MeshResolution** *(integer)* -- Fenics-like resolution index

			**MeshName** *(string)*        -- Mesh name, corresponding to the geometry

			**MeshOrder** *(integer)*      -- Order of the variational discretisation space

			**MeshType** *(integer list)*  -- List of all the number of edges that the elements contained in the mesh may have

			**MeshDofsType** *(string)*    -- Mshr codename of the variational space set up on the mesh

			**Convexity** *(integer)*      -- Indicates if the elements should be convex, not convex, or can be both of them

			**BdConformity** *(integer)*   -- Indicates if the mesh should be boundary conformal, up to adding a thin layer of triangles

		Mesh information, filled upon mesh load

			**NbMeshPoints** *(integer)*   --  Number of mesh vertices of the pysical mesh

			**NbInnerElements** *(integer)* -- Number of 2D elements

			**NbBoundaryElements** *(integer)* --  Number of 1D boundary element, either on the domain's outer boundary of on the domain's boundary forming holes

			**NbDofs** *(integer)*  --  Total number of Dofs

			**NbBoundaryDofs** *(integer)* --  Number of Dofs on the domain's boundary

		Mesh spatial information, filled upon mesh load

			**CartesianPoints**  *(2D numpy array)*  -- Nx2 (x,y) coordinates of the physical vertices

			**DofsXY**           *(2D numpy array)*  -- Nx2 (x,y) coordinates of the Dofs

			**InnerElements**    *(2-level nested integer list)*  -- List of all the physical vertices forming the inner elements (InnerElements[element][vertex local index] -> vertex global index)

		Mesh neighboring information, filled upon mesh load

			**Neighborhoods**       *(2-level nested integer list)*  -- List of the adjascent element given an element and an edge index [element][edge]-> neighboring element index

			**BoundaryElements**    *(2-level nested integer list)*  -- List of the 1D boundary elements of the domain [element] -> [list of vertices forming this boundary element]

			**BoundaryVertices**    *(integer list)*                 -- List of all physical vertices that lie on the domain's boundary (warning: the list index has no meaning here)

			**BoundaryElementsTag** *(integer list)* 			     -- Tag of the boundary elements (=numbering/labelling of boundary segments)

			**BoundaryVerticesTag** *(integer list)* 				 -- Tag of the boundary vertices  (labelling of boundary vertices, warning: the list index should be mapped to the index of self.BoundaryVertices, it does not correspond to the vertex)

			**BoundaryVerticesMap** *(dict)*						 -- Dictionary that maps directly the vertices number to the corresponding tag

		Variational information, filled upon mesh load

			**ElementsBoundaryWiseDofs** = []	# 3-level nested list giving the Dofs that are lying on each boundary of each inner element: [element][boundary] -> [dof list]

			**ElementsBoundaryDofs**     *(2-level nested integer list)* -- Dofs lying on the boundary of an element [element] -> [dof list]

			**ElementsInnerDofs**        *(2-level nested integer list)* -- Dofs living strictly within the element [element] -> [dof list]

			**ElementsDofs**             *(2-level nested integer list)* -- Dofs living within the element [element] -> [dof list]

			**BoundaryDofs**    *(integer list)* -- List of all the domain's boundary Dofs (warning: the list index is meaningless here)

			**BoundaryDofsTag** *(integer list)* -- Corresponding labelling of the boundary (warning: the list index has to be mapped with the index of self.BoundaryDofs, it does not relate to the Dof number directly)

			**BoundaryDofsMap** *(dict)* -- Dictionary creatng a direct access to the tag given a boundary Dof index


		Mesh properties, usually blank at mesh load/export and filled later on upon properties extraction

			**hmax**                       *(float)*      -- Max element diameter

			**InnerElementsVolume**        *(float list)* -- List containing the volume of the elements [element]-> diameter

			**InnerElementsDiameter**      *(float list)* -- List containing the max spacing beetween two phisical vertices of an element [element]->Diameter

			**ElementsBoundaryWiseNormals**  *(3-level nested float list)*  -- List containing the boundary normals of an element [element][boundary]->[nx, ny]

			**Domain**                      *(2-level nested integer list)* -- List that contains the ordered vertices of the [outer boundary, inner boundary1 (if any), inner boundary2 (if any), .....]

		Variational properties filled upon extraction of variational properties

			**Dofs2Vertex**       *(integer list)*               -- Correspondance table from global Dof numbering to the corresponding Vertex global index [dof]-> vertexId or None

			**Vertex2Dofs**      *(2-level nested integer list)* -- Correspondance table from global Vertex numbering to the corresponding Dofs global indices (in case of DG, mapping to several Dofs) [vertex]->[dofs located at thid vertex]

			**TabulatedVertex**  *(integer list)*                -- Gives locally the correspondance table from Vertex to Dof

			**VertexLocation**   *(2-level nested integer list)* -- Give locally the position of the vertices in the list of Dofs [element]->[indices where the dofs are actually located at vertices location]

			**TabulatedDofs**    *(integer list)*                -- Gives locally the correspondance table from Dof to Vertex

			**Dof2VertexLoc**    *(3-level nested integer list)* -- Same as Dof2Vertex, but locally to an element

			**Vertex2DofLoc**    *(3-level nested integer list)* -- Same as Vertex2Dofs, but locally to an element

	|

	.. note::

		Further fields can be added later to a :code:`MeshStructure` class's instance, but a such modified
		instance should not be saved through pickles with the wish of being later reloaded as such.

	|

	"""

	def __init__(self):
		# Attributes that are generated at the build/load of the mesh itlself
		self.MeshResolution = 0				# Fenics-like resolution index
		self.MeshName       = ""			# Mesh name, corresponding to the geometry
		self.MeshOrder      = -1			# Order of the discretisation space
		self.MeshType       = []			# List of all the number of edges that the elements contained in the mesh can have
		self.MeshDofsType   = ""			# Type of variational space that dictates the location of the Dofs
		self.Convexity      = -1			# Indicates if the elements should be convex, not convex, or can be both of them
		self.BdConformity   = -1			# Indicates if the mesh should be boundary conformal, up to adding a thin layer of triangles

		self.NbMeshPoints        = 0		# Number of mesh vertices of the pysical mesh
		self.NbInnerElements     = 0		# Number of 2D elements
		self.NbBoundaryElements  = 0		# Number of 1D boundary element, either on the domain's outer boundary of on the domain's boundary forming holes
		self.NbDofs              = 0		# Total number of Dofs
		self.NbBoundaryDofs      = 0		# Number of Dofs on the domain's boundary

		self.CartesianPoints  = []			# Nx2 (x,y) coordinates of the physical vertices
		self.DofsXY           = []			# Nx2 (x,y) coordinates of the Dofs
		self.InnerElements    = []			# List of all the physical vertices forming the inner elements
		self.Neighborhoods    = []			# 3-level nested list giving the adjascent element given an element and an edge index [element][edge]-> neighboring element index

		self.BoundaryElements     = []		# List of the 1D boundary elements of the domain
		self.BoundaryVertices     = []		# List of all physical vertices that lie on the domain's boundary (warning: the list index has no meaning here)
		self.BoundaryElementsTag  = []		# Tag of the boundary elements (=numbering/labelling of boundary segments)
		self.BoundaryVerticesTag  = []		# Tag of the boundary vertices  (labelling of boundary vertices, warning: the list index should be mapped to the index of self.BoundaryVertices, it does not correspond to the vertex)
		self.BoundaryVerticesMap  = []		# Dictionary that maps directly the vertices number to the corresponding tag

		self.ElementsBoundaryWiseDofs = []	# 3-level nested list giving the Dofs that are lying on each boundary of each inner element: [element][boundary] -> [dof list]
		self.ElementsBoundaryDofs     = []	# 1-level list giving the Dofs lying on the boundary of an element [element] -> [dof list]
		self.ElementsInnerDofs        = []	# 1-level list giving the Dofs living strictly within the element [element] -> [dof list]
		self.ElementsDofs             = []  # 1-level list giving the Dofs living within the element [element] -> [dof list]

		self.BoundaryDofs    = []			# List of all the domain's boundary Dofs (warning: the list index is meaningless here)
		self.BoundaryDofsTag = []			# Corresponding labelling of the boundary (warning: the list index has to be mapped with the index of self.BoundaryDofs, it does not relate to the Dof number directly)
		self.BoundaryDofsMap = []			# Dictionary creatng a direct access to the tag given a boundary Dof index

		# Attributes that are generated only after export properties, not built/load the mesh itlself
		self.hmax                        = 0.0 	# Max element diameter
		self.InnerElementsVolume         = []	# List containing the volume of the elements [element]-> diameter
		self.InnerElementsDiameter       = []	# List containing the max spacing beetween two phisical vertices of an element [element]->Diameter
		self.ElementsBoundaryWiseNormals = []	# 3-level nested list containing the boundary normals of an element [element][boundary]->[nx, ny]
		self.Domain                      = []	# List that contains the ordered vertices of the [outer boundary, inner boundary1 (if any), inner boundary2 (if any), .....]
