#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

"""
The module :code:`Errors` located in the :code:`SourceCode/Postprocessing` furnishes the tools to compute and analyse the errors made
during a solution's computation, provided the studied problem has an exact solution.

|
|

.. note::

     - | The dependency on the module :code:`PredefinedProblems` is there only to select and know the expression of the exact solution, when defined is defined along with the problem definition in the python problemfile.
     - | The errors are computed with the hand based quadrature that is exact up to order five

"""

# ==============================================================================
#	Preliminary imports
# ==============================================================================
# System and classical tools
import os, sys, glob
import importlib.util
import re

# Math and custom import
import numpy              as np
import matplotlib.pyplot  as plt

# Custom module import
import Pathes
import LocalLib.BarycentricTools             as BT
import LocalLib.BarycentricBasisFunctions    as BF
import PostProcessing.Exports                as EX
import _Solver.QuadratureRules.Quadratures   as QR

# Import the library where the problem is stored
import PredefinedTests.PredefinedProblems    as PB



# ==============================================================================
#	Technical routines
# ==============================================================================

#### Computing the error of a given solution associated to a given problem #####
def ComputeErrors(Problem, Mesh, Solution, ProblemName, TestCase, UsedParameters):
    """
    Computing the error of a given solution associated to a given problem. The computed norms are
    L2, L1, LInf and are retrieved through the quadrature scheme the solution has been computed through.
    If the scheme with which the solution has been computed did not need any quadrature rule, the hand-based
    quadrature will be used by default.

    Args:
        Problem         (Problem):         The considered problem
        Mesh            (MeshStructure):   The considered mesh
        Solution        (Solution):        A previously computed solution stored in a Solution structure
        ProblemName     (string):          The problem name, matching the file defining it in PredefinedProblems
        TestCase        (string):          The testcase, defining the name of the export folder
        UsedParameters  (string list):     The set of paramters used to compute the solution (used to reconstruct/interpolate the solution at quadrature points when integrating the error.)

    .. note::

        - If the arguments in UsedParamters do not allow to infer from which interpolation the solution should be recosntructed at quadrature points, a linear interpolation from the value at vertices will be performed.
        - The accuracy of the error computation themselves is up to the interpolation error in the reconstruction of the solution value at the quadrature points.
    """

    # ---------------- Initialising  -------------------------------------------
    # Defining the instance where the exact solution routine is located
    ExactSol = eval("PB."+ProblemName+".ProblemSolution()")

    # Initialising the error dictionnary that will contain the wished errors for all the variables
    Errors = dict()
    Errors["hmax"] = Mesh.hmax

    # Initialising the errors buffers that will get filled as the element get spanned
    L1err = np.zeros((Problem.GoverningEquations.NbVariables,1))
    L2err = np.zeros((Problem.GoverningEquations.NbVariables,1))
    LInferr = -1000*np.ones((1,Problem.GoverningEquations.NbVariables))

    # Getting the scheme parameters with wich the solution has been computed (to know how to reconstruct the solution)
    SchemeParams     = list(filter(None,re.split(",|\t| ", re.findall(".+?(?=#)", UsedParameters[2])[0])))
    SchemeQuadParams = list(filter(None,re.split("\\|", UsedParameters[8])))

    # Retrieving the quadrature with which the solution has been computed to get the
    # errors from the same quadrature points. If no data, simply take the hand-based one
    if len(SchemeQuadParams)==2: QuadratureRules = QR.Quadratures(Mesh, SchemeQuadParams[0], SchemeQuadParams[1])
    else:                        QuadratureRules = QR.Quadratures(Mesh, "1", "1")

    # Retrieving the reconstruction process of the solution, assuming that if a basis function based scheme has been used,
    # the two first paramters are the order and the type of basis function, respectively
    if len(SchemeParams)>1 and (SchemeParams[0].isdigit()) and SchemeParams[1].isalpha():
        InterpBFOrder = int(SchemeParams[0])
        InterpBFType  = SchemeParams[1]
        ToInterp      = 0
    else:
        InterpBFOrder = 1
        InterpBFType  = "B"
        ToInterp      = 1

    # ----------------- Computing the errors -----------------------------------
    for i in range(Mesh.NbInnerElements):

        # Getting the element's properties
        ele = Mesh.InnerElements[i]
        vol = Mesh.InnerElementsVolume[i]
        n   = Mesh.ElementsBoundaryWiseNormals[i]

        # Get the coordinates of the vertices in the physical order and the associated solution state
        elevert  = Mesh.CartesianPoints[ele,:]
        dele     = np.array(Mesh.ElementsDofs[i])

        # Extracting or reconstructing the solution at the points used as reference to interpolate the solution at quadrature points
        if not ToInterp:
            UU = Solution.Sol[:, dele]
        else:
            # Retrieve the dofs located at the vertices to extract its value.
            index = self.Mesh.VertexLocation[i]
            UU    = Solution.Sol[:,dele[index]]

        # Retrieving the quadrature points as barycentric coordinates and convert them to cartesian
        # in the Dof's vertex ordering referential
        bqp, weights = QuadratureRules.InnerQuadrature(len(ele))
        qp = BT.GetCartesianCoordinates(bqp, elevert)

        # Evaluate the basis function in the order of the Dofs
        Base  = BF.BarycentricBasis(len(ele), InterpBFType, InterpBFOrder, bqp)
        Uloc  = np.dot(UU,  Base.T)

        # Evaluate the solution at the quadrature point and reconstruct the approximated solution there
        ExactSol.Solution(qp, Solution.t)
        ExactU  =  ExactSol.Sol

        # Computing the error local to the element and storing it to the global integration value
        L1err   += np.sum(weights[:]*(np.abs(Uloc-ExactU)),    axis=1, keepdims=True)*vol
        L2err   += np.sum(weights[:]*(np.abs(Uloc-ExactU)**2), axis=1, keepdims=True)*vol
        LInferr  = np.array([np.max(np.append(LInferr.T, np.abs(Uloc-ExactU),axis=1), axis=1)])

    # Post treatment for the L2 norm
    L2err = np.sqrt(L2err)

    # Storing the error in each variable in the disctionnary
    for i in range(Problem.GoverningEquations.NbVariables):
        Errors[i, "L2norm"]   = L2err[i,0]
        Errors[i, "L1norm"]   = L1err[i,0]
        Errors[i, "LInfnorm"] = LInferr[0,i]

    # ---------------------------- Errors export --------------------------------------------------------------------
    # Selecting the files to save to
    Folder       = os.path.join(Pathes.SolutionPath, TestCase, "Res"+str(Mesh.MeshResolution).replace(".","_"), "Errors")
    FileName     = os.path.join(Folder, ("Errors_t{0:8.6f}".format(Solution.t)).replace(".","_"))
    TextFileName = os.path.join(Folder, ("Errors_t{0:8.6f}.txt".format(Solution.t)).replace(".","_"))

    # Create the folder if not already there
    try:    os.makedirs(Folder)
    except: pass

    # Saving the errors in a binary format
    np.save(FileName, Errors, allow_pickle=True)

    # Creating a human readable content
    ExportContent  = "MeshHmax: {0:8.7f}\t\t Time \t\t {1:8.7f}\n".format(Mesh.hmax, Solution.t)
    ExportContent += "Variable\t\t L2err \t\t L1err \t\t LInferr\n"
    for i in range(Problem.GoverningEquations.NbVariables):
        ExportContent += "{0!s}\t\t {1:16.15f} \t {2:16.15f} \t {3:16.15f} \n".format(Problem.GoverningEquations.VariablesNames[i], L2err[i,0], L1err[i,0], LInferr[0,i])

    # Saving the errors in a human readable format for an external access
    Export = open(TextFileName, 'w')
    Export.write(ExportContent)
    Export.close()

    # Returning the errors for external use
    return(Errors)

#### Computing the convergence rate and plots of a given solution associated to a given problem #####
def PlotConvergenceRate(ProblemName, TestCase, Times, Resolutions, WishedNorms):
    """
    Computing the convergence plots for one test case, on several time points at a fixed time t.

    Args:
        ProblemName (string):                The problem name, matching the file defining it in PredefinedProblems
        TestCase    (string):                The testcase, defining the name of the export folder
        Times       (string or float list):  The timestamps to export the error for. Write "all" if every timestamp has to be considered, give the list of times otherwise.
        Resolutions (integer list):          The resolutions to consider to export the errors for and generate the error plots
        WishedNorms (string list):           The errors types to compute and the relative plots to export, should be among ["L2norm", "LInfnorm", "L1norm"]

    .. note::

        In any case, the three norms "L2norm", "LInfnorm", "L1norm" will be computed and exported individually in the Resolution-wise result folders.
        The choice in WishedNorms is only for exporting the plot.
	"""
    Resolutions=[20,80]
    # ---------------- Initialising -----------------------------------------------------
    # Initialising the storage of the errors
    Errors = dict()

    # ---------- Generated the errors for all the wished times stamps for all considered resolutions ----------------
    # User information
    print("Computing the error plot based on the exported resolutions", *Resolutions)
    
    # Span all the resolutions and compute the erros for the given time stamps
    for Res in Resolutions:

        # --- Initialise and target the right solution file
        #Check the type of solution export
        Settings    = open(os.path.join(Pathes.SolutionPath, TestCase, "Res"+str(Res).replace(".","_"), "RawResults","UsedSettings.txt"), "r")
        SettingsSet = Settings.readlines()
        LightExport = int(re.split(" |\t", SettingsSet[23])[0])
        Settings.close()

        # Get the time stamps for which to generate the solutions' snapshots
        if Times=="all":
            lst   = glob.glob(os.path.join(Pathes.SolutionPath, TestCase, "Res"+str(Res).replace(".","_"), "RawResults","*.sol"))
            times = np.sort([float(FileName[-12:-4].replace("_",".")) for FileName in lst])
            if times.size==0: print("Error: No previous results exported. Pass.")
        else:
            times = Times
            lst   = np.array([len(glob.glob(os.path.join(Pathes.SolutionPath, TestCase, "Res"+str(Res).replace(".","_"), "RawResults","Solution_*t{0:8.6f}".format(tt).replace(".","_")+".sol"))) for tt in times])
            if np.any(lst==0): raise ValueError("Error: at least one of the wished time is not exported for the resolution"+str(Res)+". Pass.")

        # Load the mesh once for all
        if len(times)>0: Mesh, ProblemData, Problem, Solution = EX.LoadSolution(LightExport, TestCase, Res, times[0], 1)

        # ---- Span all the timestamps and fill the dictionnary with the computed errors
        for t in times:
            # Load the solution and compute the errors
            ProblemData, Problem, Solution = EX.LoadSolution(LightExport, TestCase, Res, t, 0)
            err = ComputeErrors(Problem, Mesh, Solution, ProblemName, TestCase, SettingsSet)

            # Looping on the variables (as slicing are not available for dicts)
            for i in range(Problem.GoverningEquations.NbVariables):
                # Initialise a new sub-dictionnary if the time stamp were not already there before
                if not t in Errors.keys(): Errors[Solution.t] = dict()
                # Build the dictionnary containing all the timestamps and all errors for the mesh resoluti
                for typp in WishedNorms: Errors[Solution.t][i, Mesh.hmax, typp] = err[i, typp]

    # ---------------- Storage consideration ------------------------------------------
    # Selecting the file to save to
    Folder   = os.path.join(Pathes.SolutionPath, TestCase, "_ConvergenceResults")
    FileName = os.path.join(Folder, ("Errors"))

    # Create the folder if not already there
    try:    os.makedirs(Folder)
    except: pass

    # Saving the full errors in a binary format
    np.save(FileName, Errors, allow_pickle=True)

    # ---------------- Generate the error plots ---------------------------------------
    # Loop over the time-variables points
    for t in list(Errors.keys()):
        # Retrieve all the available error for this time point
        err  = Errors[t]
        keys = list(err.keys())

        # Loop over the error types to fursnish a plot per type
        for typp in WishedNorms:
            # ---- Build the plot attributes
            # Extract the errors of this type among the preselected ones at the considerd time-point
            ind = np.where([typp==key[2] for key in keys])[0]
            # Extract the available resolutions to plot the errors against
            AvailHmax = list(np.sort(list(set([keys[index][1] for index in ind]))))
            # Extract the variables to consider
            AvailVariables = set([keys[index][0] for index in ind])

            # ---- Build the plot
            for i in AvailVariables:
                # Extract all the resolutions associated to the max diameters
                errvals = (np.array([err[i, h, typp] for h in AvailHmax]))
                # Span all the resolutions
                plt.loglog(AvailHmax, errvals, "--o")
                # Printout the order
                print("The convergence rate for the variable", i, "is", np.polyfit(np.log(AvailHmax), np.log(errvals),1)[0])

            # ---- Build the plot attributes and storage location
            # Set the plot attributes
            plt.legend(Problem.GoverningEquations.VariablesNames)
            plt.xlabel("h")
            plt.ylabel("Convergence rate in" + typp)

            # Select the storage location
            FileName = os.path.join(Folder, ("ErrorPlot_{0!s}_t{1:8.7f}_Variable_{2:d}.png".format(typp,t,i)))
            # Write the plot out
            plt.savefig(FileName)
            plt.close()
