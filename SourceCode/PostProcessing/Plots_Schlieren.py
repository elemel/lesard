#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

"""

The module :code:`Plots_Schileren` located in the :code:`SourceCode/Postprocessing` file  proposes visualisation routines for generating the Schlieren images of a given field from a solution of a multi phase problem which is stored in a Solution structure, using matplotlib.

|
.. note::

    - The module is made to be used on-line, that is either while computing the solution (except videos) or after the solution has been exported
    - The plots will render properly only on decently fine meshes

|
|

"""


# ==============================================================================
#	Preliminary imports
# ==============================================================================

# 2D plots requirements
from matplotlib.collections import PatchCollection
from matplotlib.patches     import PathPatch
from matplotlib.colors      import ListedColormap
from matplotlib.path        import Path             as mpltPath
import matplotlib.pyplot                            as plt

# Import local directories
import Pathes

# Math packages
import scipy.interpolate
import scipy.integrate
import numpy  as np

# Pythonic packages
import itertools
import os, sys
import copy

# ==============================================================================
#  Tools
# ==============================================================================

#### Converts three vectors to a surface ####
def GetMeshgridFom3VectorsMaskX(Mesh, X, Y, Z):
    """ Interpolation routine in order to plot.

    .. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

    Args:
        Mesh       (MeshStructure):        the mesh in use
        X          (float array like):        the x coordinates of the considered points
        Y          (float array like):        the y coordinates of the considered points
        Z          (float array like):        the data's (scalar) value at the considered points

    .. rubric:: Returns

    - |bs| **xgrid** *(float 2D numpy array)*   --    x meshgrid of the points where the solution Z has been interpolated
    - |bs| **ygrid** *(float 2D numpy array)*   --    y meshgrid of the points where the solution Z has been interpolated
    - |bs| **zgrid** *(float 2D numpy array)*   --    values of the interpolated Z at the generated grid points

    |

    .. note::

        - When holes in the domains, the xy values are masked with None in both corresponding meshgrids
        - The x resolution of the interpolation grid is set to 5*(xmax-xmin)/np.min(Mesh.InnerElementsDiameter), and respectively for the y axis
        - The interpolation method is set to "linear", with a fill value of "-1e-15" in order to fit the 0 crossing at a physical boundary in case of interpolating a level set.
    """

    # Generating points in a square range of the scatter vector
    xgrid = Mesh.CartesianPoints[:,0]
    ygrid = Mesh.CartesianPoints[:,1]
    gxmin, gxmax = np.min(xgrid), np.max(xgrid)
    gymin, gymax = np.min(ygrid), np.max(ygrid)
    gridx = np.linspace(gxmin, gxmax, int(5*(gxmax-gxmin)/np.min(Mesh.InnerElementsDiameter)))
    gridy = np.linspace(gymin, gymax, int(5*(gymax-gymin)/np.min(Mesh.InnerElementsDiameter)))

    # Generating the corresponding grid points
    xgrid, ygrid = np.meshgrid(gridx, gridy)
    #xgrid, ygrid = np.meshgrid(np.sort(xgrid), np.sort(ygrid))

    # Interpolating with a fill-value, which is not impacting as the concerned points will be left out in the patch construction
    zgrid = scipy.interpolate.griddata((X,Y),Z, (xgrid, ygrid),method='linear',fill_value="-1e-15")

    # -------- Selecting points of the previous rectangle that actually belongs to the polygon --------
    # Converting the grid to scatter points
    pointsX=np.array(np.ndarray.ravel(xgrid))
    pointsY=np.array(np.ndarray.ravel(ygrid))

    # Converting the rectangle points in a suitable format
    points2=np.column_stack((pointsX,pointsY))
    # Retrieving the shape of the polygon
    polygons = [np.array(Dom) for Dom in Mesh.Domain]
    # Building the path corresponding to the polygon
    pathes = [mpltPath(polygon) for polygon in polygons]
    # Extract the points that are inside or on the path contour
    inside2 = np.prod(np.array([pathes[0].contains_points(points2, radius=1e-15)]+\
                               [    ~path.contains_points(points2, radius=1e-15)\
                                for path in pathes[1:]]), axis=0)

    ind  = np.where(inside2==False)
    ind2 = np.unravel_index(ind, np.shape(xgrid))

    # --------- Interpolating the scattered data on the plotting points ----------
    # Masking the points that fall out
    xgrid[ind2] = None
    ygrid[ind2] = None

    # Giving back the interpolated values
    return(xgrid,ygrid,zgrid)

#### Converts three vectors to a surface ####
def GetMeshgridFom3VectorsMaskZ(Mesh,X,Y,Z):
    """ Interpolation routine in order to plot.

    Args:
        Mesh       (MeshStructure):        the mesh in use
        X          (float array like):        the x coordinates of the considered points
        Y          (float array like):        the y coordinates of the considered points
        Z          (float array like):        the data's (scalar) value at the considered points

    .. rubric:: Returns

    - |bs| **xgrid** *(float 2D numpy array)*   --    x meshgrid of the points where the solution Z has been interpolated
    - |bs| **ygrid** *(float 2D numpy array)*   --    y meshgrid of the points where the solution Z has been interpolated
    - |bs| **zgrid** *(float 2D numpy array)*   --    values of the interpolated Z at the generated grid points

    |

    .. note::

        - When holes in the domains, the z values are masked with None in both corresponding meshgrids
        - The x resolution of the interpolation grid is set to 5*(xmax-xmin)/np.min(Mesh.InnerElementsDiameter), and respectively for the y axis
        - The interpolation method is set to "linear", with a fill value of "-1e-15" in order to fit the 0 crossing at a physical boundary in case of interpolating a level set.
    """

    # ----------------------- Initialisation of the grid ---------------------------------------
    # Generating points in a square range of the scatter vector
    xgrid = Mesh.CartesianPoints[:,0]
    ygrid = Mesh.CartesianPoints[:,1]
    gxmin, gxmax = np.min(xgrid), np.max(xgrid)
    gymin, gymax = np.min(ygrid), np.max(ygrid)
    gridx = np.linspace(gxmin, gxmax, int(5*(gxmax-gxmin)/np.min(Mesh.InnerElementsDiameter)))
    gridy = np.linspace(gymin, gymax, int(5*(gymax-gymin)/np.min(Mesh.InnerElementsDiameter)))

    # Generating the corresponding grid points
    xgrid, ygrid = np.meshgrid(gridx, gridy)
    #xgrid, ygrid = np.meshgrid(np.sort(xgrid), np.sort(ygrid))

    # Interpolating with a fill-value, which is not impacting as the concerned points will be left out in the patch construction
    zgrid = scipy.interpolate.griddata((X,Y),Z, (xgrid, ygrid),method='linear',fill_value="-1e-15")

    # -------- Selecting points of the previous rectangle that actually belongs to the polygon --------
    # Converting the grid to scatter points
    pointsX=np.array(np.ndarray.ravel(xgrid))
    pointsY=np.array(np.ndarray.ravel(ygrid))

    # Converting the rectangle points in a suitable format
    points2=np.column_stack((pointsX,pointsY))
    # Retrieving the shape of the polygon
    polygons = [np.array(Dom) for Dom in Mesh.Domain]
    # Building the path corresponding to the polygon
    pathes = [mpltPath(polygon) for polygon in polygons]
    # Extract the points that are inside or on the path contour
    inside2 = np.prod(np.array([pathes[0].contains_points(points2, radius=1e-15)]+\
                               [    ~path.contains_points(points2, radius=1e-15)\
                                for path in pathes[1:]]), axis=0)

    ind  = np.where(inside2==False)
    ind2 = np.unravel_index(ind, np.shape(xgrid))

    # --------- Interpolating the scattered data on the plotting points ----------
    # Masking the points that fall out
    zgrid[ind2] = None

    # Giving back the interpolated values
    return(xgrid,ygrid,zgrid)


# ==============================================================================
#  Functions
# ==============================================================================

#### Computing Schlieren images from the conservative variables ####
def Compute2DSchlierenImages(Problem, Mesh, Solution, Field, ParametersId):
	""" Plots the numerical Schlieren images from the density field

	Args:
		Problem        (Problem):             the considered problem and its properties
		Mesh           (MeshStructure):       the considered mesh
		Solution       (Solution):            the solution of the considered problem
		Field          (string):              the name of the variable to derive the image for (as registered in Problem)
		ParametersId   (string):              name of the export folder (TestCase_TestSettings)

	Returns:
		None: Only exporting the plot

	|

	.. note::

		So far only available up to three fluids because of the color selection

	.. warning::

		On some variables for which the gradient's magnitude is null, it will fail
	"""

	# ----------------------- Defining the static color plotting vectors -------------------------------------------

	# Defining the plotting representation quantities for several fluids
	K     = [600, 120, 400]				# Smoothing factor in the shading function
	start = [0, 230/255, 100/255]		# Value indexing of the plotting (in the B scale)

	# Creating the new colormaps for all fluids
	cmap = []
	for i in range(3):
		vals = np.ones((256, 4))
		vals[:, 0] = np.linspace(0, 200/255, 256)
		vals[:, 1] = np.linspace(0, 230/255, 256)
		vals[:, 2] = np.linspace(start[i], 1, 256)
		cmap += [ListedColormap(vals)]

	# ----------------------- Computing the Schlieren numerical images -----------------------------------------------
	# Getting the desired values and there spatial location
	X = Mesh.CartesianPoints[:,0]
	Y = Mesh.CartesianPoints[:,1]
	Z = Solution.RSol[Field,:]
	xx,yy,zz = GetMeshgridFom3VectorsMaskZ(Mesh, X, Y, Z)

	# Get the fluid's spotter markers on the interpolation nodes
	lll = np.zeros(zz.shape+(Solution.RLSValues.shape[0],))
	for i in range(Solution.RLSValues.shape[0]):
		Z = Solution.RLSValues[i,:]
		xx,yy,lll[:,:,i] = GetMeshgridFom3VectorsMaskZ(Mesh, X, Y, Z)

	# Using a cheap reconstructor for now at the plot points on the grid
	#ToFlag = np.reshape(lll, (np.size(xx), Solution.RLSValues.shape[0]))
	Fluid = np.argmax(lll,axis=2)

	# Computing the gradient field
	Gradients = np.gradient(zz, yy[:,0], xx[0,:])

	# Computing the gradient's magnitude
	MDField   = np.sqrt(Gradients[0]**2+Gradients[1]**2)
	MmMDField = np.nanmax(MDField)

	# Computing the shading functions for each fluid
	plt.figure()
	for i in range(Solution.RLSValues.shape[0]):
		# Spotting the fluid's location
		ind  = np.where(Fluid==i)
		ind2 = np.where(Fluid!=i)

		# Determining the shaded function
		val = np.exp(-(K[i]*(Fluid[ind[0], ind[1]]+1)*MDField[ind[0], ind[1]])/MmMDField)

		# Defining the plotting quantity
		Phi = np.zeros(MDField.shape)
		Phi[ind[0], ind[1]]   = val
		Phi[ind2[0], ind2[1]] = None

		# Plot the quantity
		plt.pcolor(xx,yy,Phi, cmap=cmap[i])


	# --------------- Customize the figure --------------------------------------------
	variableName      = Problem.GoverningEquations.PrimaryVariablesNames[Field]
	variableLatexName = Problem.GoverningEquations.PrimaryVariablesLatexNames[Field]
	plt.title("Computational Schlieren image for the variable {0!s} at time {1:8.6f}".format(variableLatexName, Solution.t))

	# --------------- Export the figure at the right location -------------------------
	# Creating the export environment
	FolderName = os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(Mesh.MeshResolution).replace(".","_"), "2DPlots")
	try:    os.makedirs(FolderName)
	except: pass

	# Set the figure properties and shows it
	FileName = "Mplt_Schlieren_{1!s}_t{0:5.3f}".format(Solution.t, variableName).replace(".","_")
	plt.savefig(os.path.join(FolderName, FileName)+".png", transparent=True, dpi=300)
	plt.close()

#### Computing Schlieren images from the EOS linked pressure variable ####
def Compute2DSchlierenImagesPrimary(Problem, Mesh, Solution, Field, ParametersId):
	""" Plots the numerical Schlieren images from the value given out of the EoS (usually pressure)

	Args:
		Problem        (Problem):             the considered problem and its properties
		Mesh           (MeshStructure):       the considered mesh
		Solution       (Solution):            the solution of the considered problem
		Field          (integer):             the index of the primary variable to derive the schlieren graph for
		ParametersId   (string):              name of the export folder (TestCase_TestSettings)

	Returns:
		None: Only exporting the plot

	|

	.. warning::

		On some variables for which the gradient's magnitude is null, it will fail
	"""

	# ----------------------- Defining the static color plotting vectors -------------------------------------------
	# Defining the plotting representation quantities for several fluids
	K     = [60, 12, 40]				# Smoothing factor in the shading function
	start = [0, 230/255, 100/255]		# Value indexing of the plotting (in the B scale)

	# Creating the new colormaps for all fluids
	cmap = []
	for i in range(3):
		vals = np.ones((256, 4))
		vals[:, 0] = np.linspace(0, 200/255, 256)
		vals[:, 1] = np.linspace(0, 230/255, 256)
		vals[:, 2] = np.linspace(start[i], 1, 256)
		cmap += [ListedColormap(vals)]

	# ----------------------- Computing the Schlieren numerical images -----------------------------------------------
	# Getting the desired values and there spatial location
	X = Mesh.CartesianPoints[:,0]
	Y = Mesh.CartesianPoints[:,1]
	Z = Solution.RSol[:,:]

	# Compute the primary variables
	Zbuf = Problem.GoverningEquations.ConservativeToPrimary(Z, Solution.RFluidFlag)[Field,:]

	# Mapping them to a cartesian grid
	xx,yy,zz = GetMeshgridFom3VectorsMaskZ(Mesh, X, Y, Zbuf)

	# Get the fluid's spotter markers on the interpolation nodes
	lll = np.zeros(zz.shape+(Solution.RLSValues.shape[0],))
	for i in range(Solution.RLSValues.shape[0]):
		Z = Solution.RLSValues[i,:]
		xx,yy,lll[:,:,i] = GetMeshgridFom3VectorsMaskZ(Mesh, X, Y, Z)
	Fluid = np.argmax(lll,axis=2)

	# Computing the shading functions for each fluid
	plt.figure()
	for i in range(Solution.RLSValues.shape[0]):
		# Spotting the fluid's location
		ind  = np.where(Fluid==i)
		ind2 = np.where(Fluid!=i)

		# Computing the gradient field
		Gradients = np.gradient(zz, yy[:,0], xx[0,:])

		# Computing the gradient's magnitude
		MDField   = np.sqrt(Gradients[0]**2+Gradients[1]**2)
		MmMDField = np.nanmax(MDField)

		# Determining the shaded function
		val = np.exp(-(K[i]*(Fluid[ind[0], ind[1]]+1)*MDField[ind[0], ind[1]])/MmMDField)

		# Defining the plotting quantity
		Phi = np.zeros(MDField.shape)
		Phi[ind[0], ind[1]]  = val
		Phi[ind2[0], ind2[1]] = None

		# Plot the quantity
		plt.pcolor(xx,yy,Phi, cmap=cmap[i])

	# --------------- Customize the figure --------------------------------------------
	variableName      = Problem.GoverningEquations.PrimaryVariablesNames[Field]
	variableLatexName = Problem.GoverningEquations.PrimaryVariablesLatexNames[Field]
	plt.title("Computational Schlieren image for the variable {0!s} at time {1:8.6f}".format(variableLatexName, Solution.t))


	# --------------- Export the figure at the right location -------------------------
	# Creating the export environment
	FolderName = os.path.join(Pathes.SolutionPath, ParametersId, "Res"+str(Mesh.MeshResolution).replace(".","_"), "2DPlots")
	try:    os.makedirs(FolderName)
	except: pass

	# Set the figure properties and shows it
	FileName = "Mplt_Schlieren_{1!s}_t{0:5.3f}".format(Solution.t, variableName).replace(".","_")
	plt.savefig(os.path.join(FolderName, FileName)+".png", transparent=True, dpi=300)
	plt.close()
