#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

# ==============================================================================
#	Preliminary imports
# ==============================================================================
# import python modules
import numpy as np
import copy
import re

# import custom modules
import Mappers

# ==============================================================================
#	Class interfacing the quadrature schemes
# ==============================================================================
class Quadratures():
	"""Input wrapper for the quadrature scheme within and on the boundary of the considered element.

	.. note::

		This interfacing is useful only to allow a type (Quadpy, Handbased) quadrature different for inner and boundary quadratures,
		otherwise a simple mapper class with specific initialisation parameters would have been enough.
	"""

	def __init__(self, Mesh, *Params):
		"""Args:
			Mesh   (MeshStructure):  the mesh considered for solving the problem
			Params (string list):    the parameters that selects the type of quadrature to consider according to the mapper class, and their parametersself.

		The Params should be of the type [InnerQuadratureParams, BoundaryQuadratureParams] where both InnerQuadratureParams and BoundaryQuadratureParams
		are strings in the format "int1,param1,param2,..." containing first the index of the wished quadrature type (see :code:`Mappers`) followed by the
		parameters required by the selected quadrature type (see each quadrature class to know their respective parameter formats).
		"""

		# ------------------- Parsing and interfacing ------------------------------------------------------
		# In the case where a quadrature scheme is given
		if len(Params)>1:
			# Parse each parameter string into parameter lists
			InnerQuadParams = list(filter(None,re.split(",|\t", re.sub("\D", ",", Params[0]))))
			BdQuadParams    = list(filter(None,re.split(",|\t", re.sub("\D", ",", Params[1]))))

			# Convert the first character to retrieve which quadrature class to map to
			InnerQuadType    = int(InnerQuadParams[0])
			BoundaryQuadType = int(BdQuadParams[0])

			# Map and initialise the quadrature instance upon the given parameters
			self.InnerQuadrature    = Mappers.Quadrature_Rules(InnerQuadType).InnerQuadrature(Mesh, *InnerQuadParams[1:]).PointsWeightsData
			self.BoundaryQuadrature = Mappers.Quadrature_Rules(BoundaryQuadType).BoundaryQuadrature(Mesh, *BdQuadParams[1:]).PointsWeightsData

		# In the case where no quadrature scheme is given (only to be used with scheme not requiring quadrature rules)
		else:
			self.InnerQuadrature    = None
			self.BoundaryQuadrature = None
