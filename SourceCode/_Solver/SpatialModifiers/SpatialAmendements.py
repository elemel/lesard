#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

"""


The module  :code:`SpatialAmendements` located in the file :code:`_Solver/SpatialModifiers/SpatialAmendements.py`furnishes a single class
:code:`Amendements` that interfaces the user instructions for limiting and amendments of the schemes to the actuals amemdement routine,
respecting the sequence and order of mollifications.

Upon instance creation, it converts the string-user input defining the sequence of modifications to a unique equivalent modification
routine that is to apply on any freshly computed residual.
To do so, it converts the string to a sequence of unitary application process and combines them according
to the given syntax. The method :code:`Mollify` is therefore the generic interface to apply any sequence of mollifyer to a spatial scheme.

|
|

"""

# ==============================================================================
#	Preliminary imports
# ==============================================================================
import numpy as np
import copy
import re

import Mappers
import _Solver.QuadratureRules.Quadratures as QR

# ==============================================================================
#	Class furnishing all the limiter tools
# ==============================================================================
class Amendements():
	"""
	Class furnishing all the schemes tools and registers the scheme's wished
	parameters. The main iteration is accessible with the method "Mollify".

	.. rubric:: Fields

	Upon instance creation, the given parameters are repeated within the structure
	with identical names (see below). It also has for further fields:

	- |bs| **MolliProcess** *(list of callbacks)*  --  the list of amendements to apply to the designated sequential intermediate residuals

	|
	"""

	#### Automatic initialisation routine ####
	def __init__(self, AmendementsSequence, Problem, Mesh):
		"""
		Args:
			AmendementsSequence (string):        the user-understandable list of amendements to apply to the designated sequential intermediate residuals
			Problem             (Problem):       the considered problem
			Mesh                (MeshStructure): the considered mesh instance

		|

		.. rubric:: Methods
		"""

		# ------------------ Initialisations -----------------------------------
		# Repeating the initialisation variable within the structure
		self.AmendementsSequence = AmendementsSequence
		self.Problem = Problem
		self.Mesh    = Mesh

		# Extracting the modifications sequence as tabular of unitary modifications
		# (and mapping to the wished modifiers according to the mapper class)
		# if any modifier is wished
		if "0" != self.AmendementsSequence:
			# Filling self.MolliProcess
			self.StringToResu(self.ExtractSequence())
		else:
			self.MolliProcess = 0

	#### Converting the string user input into a sequential modification tabular ####
	def ExtractSequence(self):
		""" Converting the string user input into a tabular giving the sequence
		    of modification to apply

		    Args:
				None:        self.AmendementsSequence of the structure should be filled

		    Returns:
				Tab    (string list): the unitary modification list as strings, each in the format ModifyierType_Index(ApplicationTarget)
		"""

		# ------------------- Initialisations ---------------------------------------
		string = "("+self.AmendementsSequence+")"
		curr, Compact = [""], []
		level = -1

		# ------------------ Extracting the depth of each action --------------------
		# (designed for e.g. a limiter applied on the top of a filter etc)

		# Spanning each character of the string to find the max depth
		for c in string:
			# Detecting a new level
			if c == "(":
				if curr != "":
					# Creating a new level in the level-list
					if len(Compact)<=level:
						Compact += [[]]
						curr    += [""]

				# Initialising the current level
				level = level+1
				curr[level] = ""

			# Detecting an end of level
			elif c==")":
				# Safe expansion case if we went down the levl
				if len(Compact)<=level:
					Compact += [[]]
					curr    += [""]
				Compact[level] += [curr[level]]

				# Expanding the upper levels by the one furnished there
				for i in range(level-1, level):
					curr[i] += "("+curr[level]+")"

				# Reinitialising the level
				curr[level] = ""
				level -=1

			# Otherwise, simply increment the considered string extraction
			else: curr[level] += c

		# ---- Converting the global modification as a sequence of unitary ones -------
		# Initialise all the unitary modifiers
		levels = len(Compact)

		# Prepare the practical sequential tabular and the expanded human readable ones
		Tab, TabTot = [],[]

		# Span each level starting by the most nested one (to apply directly to the original residuals)
		for lvl in range(levels)[::-1]:

			# Get the iterative construction tabular for that level
			for term in Compact[lvl]:
				# Replace the terms that would already have been computed in the string litterals
				for (i,te) in enumerate(TabTot[::-1]):
					if te in term:
						# Spanning all occurences of the string and replacing the already computed
						# terms with its index while leaving the non-already computed terms with
						# similar syntax: e.g. if only Limiter_1 and Limiter_4 on the initial residual
						# were already computed, in Limiter_4(Limiter_1)+Limiter_4, only the last term
						# and Limiter_1 will be replaced by the corresponding intermediate residual indices.
						search = te.replace("(","\(").replace(")","\)").replace("+","\+").replace("-","\-")
						occurences = [tm.start() for tm in re.finditer(search, term)]
						k=0
						for j in occurences:
							idx = j-k*(len(te)-1)
							if (idx+len(te)>= len(term)) or not (term[idx+len(te)]=="(" or term[idx+len(te)]=="<"):
								k=k+1
								term = term[:idx]+"{0:d}".format(len(TabTot)-i-1)+term[idx+len(te):]

				# Separate the different terms that are on the same level
				uu = list(filter(None, re.split("[+\-*/]", term)))

				# Recontruct the same-level terms in a human readable string
				term2 = copy.deepcopy(term)
				for i in range(len(Tab))[::-1]:
					if "({0:d})".format(i) in term2\
						or "+{0:d}".format(i) in term2 or "{0:d}+".format(i) in term2\
						or "-{0:d}".format(i) in term2 or "{0:d}-".format(i) in term2\
						or "/{0:d}".format(i) in term2 or "{0:d}/".format(i) in term2\
						or "*{0:d}".format(i) in term2 or "{0:d}*".format(i) in term2:
						term2 = term2.replace("({0:d})".format(i), "({0!s})".format(TabTot[i]))
						term2 = term2.replace("+{0:d}".format(i),  "+{0!s}".format(TabTot[i])).replace("{0:d}+".format(i),  "{0!s}+".format(TabTot[i]))
						term2 = term2.replace("-{0:d}".format(i),  "-{0!s}".format(TabTot[i])).replace("{0:d}-".format(i),  "{0!s}-".format(TabTot[i]))
						term2 = term2.replace("/{0:d}".format(i),  "/{0!s}".format(TabTot[i])).replace("{0:d}/".format(i),  "{0!s}/".format(TabTot[i]))
						term2 = term2.replace("*{0:d}".format(i),  "*{0!s}".format(TabTot[i])).replace("{0:d}*".format(i),  "{0!s}*".format(TabTot[i]))

				# Register each member of the same level as terms that have to be computed independently
				if len(uu)>1:
					for u in uu:
						if not (u.isdigit() or u in Tab):
							# Register the practical table
							Tab.append(u)
							# Enrich the human-readable table in a similar way
							littu=copy.deepcopy(u)
							for i in range(len(Tab))[::-1]:
								if "({0:d})".format(i) in littu:  littu = littu.replace("({0:d})".format(i), "({0!s})".format(TabTot[i]))
							TabTot.append(littu)

				# Register in the practical table the aggregation term defining a same level
				for (i,tt) in enumerate(Tab[::-1]):
					if tt in term:
						search = tt.replace("(","\(").replace(")","\)").replace("+","\+").replace("-","\-")
						occurences = [tm.start() for tm in re.finditer(search, term)]
						k=0
						for j in occurences:
							idx = j-k*(len(tt)-1)
							if (idx+len(tt)>= len(term)) or not term[idx+len(tt)]=="(":
								k=k+1
								term = term[:idx]+"{0:d}".format(len(TabTot)-i-1)+term[idx+len(tt):]

				# If not already computed, register the new element
				if not (term.isdigit() or term in Tab):
					TabTot.append(term2)
					Tab.append(term)

		# Returning the unitary modifications sequence
		return(Tab)

	####  Interfacing the user wishes with the actual routines #####
	def StringToResu(self, stringList):
		""" Interfacing the user wishes with the actual routines, according to the unitary modification sequence list.

		Args:
			StringList  (list of strings):  the unitary modification list, each in the format ModifyierType_Index(ApplicationTarget)

		Returns:
			None: fills directly the self.MolliProcess of the structure, a tabular containing each step of modification [modifier, ApplicationTarget]
		"""

		# ------------------------- Initialisations -----------------------------------------
		self.MolliProcess = [[]]*len(stringList)

		# ------------- Converting the strings to callback depending on their type ----------
		# Spanning each instruction line contained in the list
		for (i, string) in enumerate(stringList):

			# Modifiers of the type Modifyer_Index(Target)
			if "(" in string and ")" in string:
				# Extract the parameters if any
				if re.findall("\<(.*?)\>", string):
					# Retrieve the parameters as strings
					Properties = list(filter(None, re.split(",| ", list(filter(None, re.findall("\<(.*?)\>", string)))[0])))

					# In case of a quadrature parameter, retrieve the quadrature rules through the mappers to furnish it as a ready-to use argument
					for prop in range(len(Properties)):
						quadrule = re.findall("\#(.*?)\#", Properties[prop])
						if   len(quadrule)==2: Properties[prop] = QR.Quadratures(self.Mesh, *[qr.replace("_", ",") for qr in quadrule])
						elif len(quadrule)==1: raise(ValueError("One input of the mollifiers has not been understood. Either the inner of boundary quadrature type has not been mentionned. Abortion."))
						elif len(quadrule)>2:  raise(ValueError("One input of the mollifiers has not been understood. Check the syntax of a arguments defining quadratures. Abortion."))

					# Removing the retrieved parameters to the string to parse
					string = re.sub('\<(.*?)\>', '', string)
				else: Properties = []

				# Map to the right method
				Type    = (string.split("(")[0]).split("_")[0]
				Index   = (string.split("(")[0]).split("_")[1]
				ResiInd = int(list(filter(None, re.findall("\((.*?)\)", string)))[0])
				Modif   = eval("Mappers."+Type+"("+Index+")."+Type+"(self.Problem, self.Mesh, *Properties)")
				self.MolliProcess[i] = [copy.deepcopy(Modif.Mollify), ResiInd+1]

			# Modifiers of the type Target1 operator Target2 operator Target3
			elif "+" in string or "-" in string or "*" in string or "/" in string:
				pieces   = list(filter(None, re.split("[\*+\-/]", string)))
				operator = list(filter(None, re.findall(r'[\+\-*/]*', string)))
				for p in range(len(pieces)):
					if pieces[p].isdigit(): pieces[p] = "resu[int({0:d}),:,:]".format(p)
				string = "".join([piec for piec2 in [[pieces[k], operator[k]] for k in range(len(operator))] for piec in piec2]+[pieces[-1]])
				self.MolliProcess[i] = [copy.deepcopy(lambda dumb1, dumb2, resu, *argv, string=string: eval(string)), np.arange(len(pieces))]

			# Case of untouched original residual (used for simple additions of e.g. filters to the original residual)
			elif "Original" in string:
				# Map to the identity method
				Modif  = lambda dumb1, dumb2, resu, *argv: resu[:,:]
				self.MolliProcess[i] = [copy.deepcopy(Modif), 0]

			# Modifiers of the type Modifyer_Index, acting on the original residual
			else:
				# Extract the given parameters if any
				if re.findall("\<(.*?)\>", string):
					# Retrieve the parameters as strings
					Properties = list(filter(None, re.split(",| ", list(filter(None, re.findall("\<(.*?)\>", string)))[0])))

					# In case of a quadrature parameter, retrieve the quadrature rules through the mappers to furnish it as a ready-to use argument
					for prop in range(len(Properties)):
						quadrule = re.findall("\#(.*?)\#", Properties[prop])
						if   len(quadrule)==2: Properties[prop] = QR.Quadratures(self.Mesh, *[qr.replace("_", ",") for qr in quadrule])
						elif len(quadrule)==1: raise(ValueError("One input of the mollifiers has not been understood. Either the inner of boundary quadrature type has not been mentionned. Abortion."))
						elif len(quadrule)>2:  raise(ValueError("One input of the mollifiers has not been understood. Check the syntax of a arguments defining quadratures. Abortion."))

					# Removing the retrieved parameters to the string to parse
					string = re.sub('\<(.*?)\>', '', string)

				else: Properties = []

				# Map to the right method
				Type    = (string.split("(")[0]).split("_")[0]
				Index   = (string.split("(")[0]).split("_")[1]
				ResiInd = 0
				Modif   = eval("Mappers."+Type+"("+Index+")."+Type+"(self.Problem, self.Mesh, *Properties)")
				self.MolliProcess[i] = [copy.deepcopy(Modif.Mollify), ResiInd]

	#### Main routine, definining the limiting mollifier ####
	def Mollify(self, FlagPoints, SolBuff, resu, fluxes, i, du=0, dt=1):
		""" Limting routine according to the given sequence stored in the structure

		Args:
			FlagPoints  (function callback):    the handle of a function flagging the points to the relevant fluid
			Solbuff     (solution structure):   structure containing the current solution's values to iterate
			resu       (float numpy array):    previoulsy computed residuals that have to be modified (NbVars x NbElementDofs)
			fluxes     (numpy (multiD)array):  pre-computed fluxes at the points of interest. For this scheme, access with fluxes[element, face, coordinate(fx or fy), variable, pointindex]
			i          (integer):              the index of the considered element within which the partial residuals will be computed
			du         (float numpy array):    (optional) when using DeC, the difference in the time iteration
			dt         (float):                (optional) when using DeC, the full time step

		Returns:
			Lresu      (float numpy array):    the modified residuals (NbVars x NbElementDofs)

		.. warning::
			the fluxes are not changing through the intermediary steps
		"""

		# --------------- Initialisation ------------------------------------------
		# Checking that there is indeed the wish of a mollifier
		if self.MolliProcess == 0: return(resu)

		# Create a structure gathering all the intermediary steps' residuals
		NbSteps = len(self.MolliProcess)+1
		ModRes  = np.zeros((NbSteps, np.shape(resu)[0], np.shape(resu)[1]))
		ModRes[0,:,:] = copy.deepcopy(resu)

		# --------------- Applying the sequencee of modifications -----------------
		for (j,mol) in enumerate(self.MolliProcess):
			ModRes[j+1,:,:] = mol[0](FlagPoints, SolBuff, ModRes[mol[1],:,:], fluxes, i, du, dt)

		# Return the last (and final) modified residual
		return(ModRes[-1,:,:])
