#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

"""
|

The structures and classes that allow a shortcut of all the routines
used upon the user's choices (governing equations, spatial schemes, etc)
are defined in the module :code:`_Solver.Solver_Initialisations`.
After shortuting the routine accordingly to the values registered in the file
:code:`Mappers.py`, it automatically defines the spatial subdomains,
initialises the solution and the fluid's selector.
The fields and methods of the three main instances representing the Problem, Solver and Solution
are given below.

"""

# ==============================================================================
#	Preliminary imports
# ==============================================================================
# Load the solver and problem's initialisation modules
import _Solver.SpatialModifiers.SpatialAmendements  as SA
import _Solver.QuadratureRules.Quadratures          as QR
import _Solver.Redistancers.WrapRedistancer         as FR
import ProblemDefinition.FluidSubdomains            as SD
import Mappers
import Pathes

# Import python and math libraries
import os, sys, re
import numpy as np


# ==============================================================================
#	Parameter (and reader) class of scheme properties and runtime informations
# ==============================================================================
class Parameters():
	"""
	.. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

	Parameter (and reader) class of scheme properties and runtime informations.
	It furnishes all the information given from the user through the
	setting file given in argument when running the script in a structure
	containing the following fields:

	|

	.. rubric:: Parameters chosen by the user

	The text content is exported into a ParamsReader structure variable containing:

		- |bs| **ExportInterval**     *(float)*   --      time interval after which the computed results will be exported
		- |bs| **Tmax**               *(float)*   --      the (physical) end-time of the computations
		- |bs| **CFL**                *(float)*   --      the CFl number
		- |bs| **TimerParams**        *(strings list)* -- parameters of the time schemes
		- |bs| **TimeSchemeId**       *(integer)*      -- index of the time scheme  (see the Mapper file)
		- |bs| **SchemeParams**       *(string list)*  -- parameters of the spatial scheme
		- |bs| **SchemeQuadParams**   *(string list)*  -- quadrature parameters of the spatial scheme
		- |bs| **SchemeId**           *(integer)*      -- index of the spatial scheme  (see the Mapper file)
		- |bs| **ScModifiers**        *(string)*       -- modifier sequence for spatial scheme (limiters etc (see the Mapper file))
		- |bs| **FluidSpotterId**     *(integer)*      -- index of the fluid selector (see the Mapper file)
		- |bs| **FluidSpotterParams** *(string list)*  -- parameters of the fluid selector (see the Mapper file)
		- |bs| **RedistancerParams**  *(string list)*  -- redistancing scheme and properties (see the Mapper file and the settings parser of the redistancing library)
		- |bs| **FSQuadParams**       *(string list)*  -- quadrature parameters of the fluid selector
		- |bs| **QuadratureType**     *(integer)*      -- index of the wished quadrature (see the Mapper file)
		- |bs| **QuadratureOrder**    *(integer)*      -- order of the wished quadrature
		- |bs| **Verbose**            *(string)*       -- verbose mode: , "d": debug, "m": minimal, "n": none
		- |bs| **LightExport**        *(integer)*      -- specifies the export mode: 0 for an export containing the mesh, 1 for a mesh free export

	|

	.. rubric::  Automatically generated attributes:

	*Retrieved directly at instance creation*

		- |bs| **MeshType**  *(string)*  -- type of the mesh associated (CG, DG, RT, BDM, ...)
		- |bs| **MeshOrder** *(integer)* -- order of the mesh

	|

	.. note::

		This is only a product-type class: no method is available

	|

	"""

	#### Initialisation (reader) routine #####
	def __init__(self, SolverSpecs):
		"""
		Args:
			SolverSpecs (string):  the name of the setting file, either the raw name of the settings stored in the PredefinedSettings folder (or equivalent if changed by the user), or the full path to "Used settings in case of a restart."

		"""

		# Mapping to the file where the SolverData are stored and get all of its content (UsedSettings is used in case of a restart only, 
		# and the path should be completely given, with the extension)
		if     "UsedSettings" in SolverSpecs:   ParamFileName = SolverSpecs
		else:  ParamFileName = os.path.join(Pathes.SettingPath, SolverSpecs) + ".txt"
        
        # Exploring its content
		ParamFile     = open(ParamFileName, "r")
		Params        = ParamFile.readlines()
		ParamFile.close()

		# Get the runtime information
		self.CFL  = float(re.split(" |\t", Params[17])[0])
		self.Tmax = float(re.split(" |\t", Params[18])[0])

		# Retrieve the time scheme and its parameters
		self.TimeSchemeId = int(re.split("\t| ", Params[15])[0])
		self.TimerParams  = list(map(int, filter(None,re.split(",|\t| ", re.findall(".+?(?=#)", Params[16])[0]))))

		# Retrieve the spatial scheme and limiter to use
		self.SchemeParams  = list(filter(None,re.split(",|\t| ", re.findall(".+?(?=#)", Params[2])[0])))
		self.SchemeId      = int(re.split(" |\t", Params[1])[0])
		self.ScModifiers   = re.split(" |\t", Params[3])[0]

		# Retrieve the FluidSelector type
		self.FluidSpotterId     = int(re.split(" |\t", Params[6])[0])
		self.FluidSpotterParams = list(filter(None,re.split(",|\t| ", re.findall(".+?(?=#)", Params[7])[0])))

		# Retrieve the Redistancing type and properties (if any given)
		self.RedistancerParams = re.split(" |\t", Params[8])[0]

		# Retrieve the quadrature type for the spatial scheme and the fluid selector
		self.SchemeQuadParams = list(filter(None,re.split("\\|", Params[11])))
		self.FSQuadParams     = list(filter(None,re.split("\\|", Params[12])))

		# Retrieve the runtime options
		self.ExportInterval = float(re.split(" |\t", Params[22])[0])
		self.Verbose        = re.split(" |\t", Params[21])[0]
		self.LightExport    = int(re.split(" |\t", Params[23])[0])

		# Retrieve the type of required Mesh
		MeshProp = Mappers.Spatial_Scheme(self.SchemeId).AssociatedMeshType(self.SchemeParams)
		self.MeshType  = MeshProp.MeshType
		self.MeshOrder = MeshProp.MeshOrder

# ==============================================================================
#	Solution's structure class
# ==============================================================================
class Solution():
	"""
	.. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

	Class used as an product type object containing all the informations on
	the computed solution, containing the following attributes:

		- |bs| **Subdomains**  *(shapely multipolygons list)*  --  List of shapely multipolygons that define each fluids countour (NbFluids)
		- |bs| **FluidFlag**   *(integer list)*                --   For each mesh Dof, the Flag associated to the fluid that is present at this point (NbDofs)
		- |bs| **RFluidFlag**  *(integer list)*                --    For each mesh vertex, the Flag associated to the fluid that is present at this point (NbMeshPoints), reconstructed from the value at Dofs
		- |bs| **LSValues**    *(float list)*                  --    Level set values at each Dof (NbFluids x (NbDofs))
		- |bs| **RLSValues**   *(float list)*                  --    Level set values at each vertex, reconstructed from the Dofs (NbFluids x (NbMeshPoints))
		- |bs| **Mesh**        *(MeshStructure)*               --    A recall of the used mesh
		- |bs| **maxh**        *(float)*                       --    A shortcut of the mesh max element's size
		- |bs| **minh**        *(float)*                       --     A shortcut of the mesh min element's size (for plotting purposes)
		- |bs| **Sol**         *(float numpy array)*           --    The actual solution (overwritten each time step, NbVar x (NbDofs))
		- |bs| **RSol**        *(float numpy array)*           --    The reconstructed solution at the Mesh's physical vertices (overwritten each time step, NbVar x (NbMeshPoints))
		- |bs| **RXY**         *(float numpy array)*           --    A shortcut of the cartesian coordinates of the MeshVertices ((NbMeshPoints) x 2)
		- |bs| **t**           *(float)*                       --    The time corresponding to the solution

	|

	Upon instance creation, itnitialises the solution structure upon the problem and the mesh (except LSValues).

	|
	.. note:

		The class is not inherited from the mesh/problem in order to be able to keep the
	    solution as a subclass to be transported easily

	|

	"""

	#### Actual initialisation routine upon the problem's parameters ####
	def __init__(self, Problem, Mesh):
		"""


		Args:
			Problem (Problem):          structure containing all the problem info and shortcuted routines
			Mesh    (MeshStructure):    the considered mesh

		|

		.. rubric:: Methods

		"""

		# Initialisation of the CFL related fix properties
		self.t    = 0
		self.maxh = np.max(Mesh.InnerElementsDiameter)
		self.minh = np.min(Mesh.InnerElementsDiameter)

		# Initialisation of the Solution's fixed properties
		self.Sol  = np.zeros((Problem.GoverningEquations.NbVariables, Mesh.NbDofs))
		self.FluidFlag = np.zeros(Mesh.NbDofs, dtype=int)
		self.LSValues  = []			# Initialized to void.
									# Will be initialized when the LS itlself will be set up.

		# Initialisation of the Reconstructed solution's fixed properties
		self.RSol = np.zeros((Problem.GoverningEquations.NbVariables, Mesh.NbMeshPoints))
		self.RXY  = Mesh.CartesianPoints[:,:]
		self.RFluidFlag = np.zeros(Mesh.NbMeshPoints, dtype=int)
		self.RLSValues  = []		# Initialized to void as for LSValues.

		# Initialisation of the fluid-dependent parameters
		self.Initialise_Flags(Problem, Mesh)
		self.Initialise_SolutionValues(Problem, Mesh)

	#### Initialising the fluid flags on the Dofs and MeshVertices ####
	def Initialise_Flags(self, Problem, Mesh):
		"""
		Initialise the solution structure upon the problem and the mesh.

		Args:
			Problem (Problem):        structure containing all the problem info and shortcuted routines
			Mesh    (MeshStructure):  the considered mesh

		Returns:
			None: filled FluidFlags vector within solution structure.
		"""

		# Exports the Flags
		self.Subdomains = Problem.InitialSubdomains
		self.FluidFlag  = SD.ExportFluidFlags(Problem.InitialSubdomains, Mesh)

	#### Initilising the actual values of the solutions ####
	def Initialise_SolutionValues(self, Problem, Mesh):
		"""
		Initialise the solution structure upon the problem and the mesh.

		Args:
			Problem (Problem):        structure containing all the problem info and shortcuted routines
			Mesh    (MeshStructure):  the considered mesh

		Returns:
			None: filled intial solution values within solution structure.
		"""

		# Initialise the solution by extracting the asked initial condition for each fluid aera
		for i in range(Problem.NbFluids):
			# Getting the index of the considered points
			Index  = np.where(self.FluidFlag[:]==i)[0]

			# Getting the fluid's properties
			EOS = Problem.FluidEOS[i]
			FP  = Problem.FluidProp[i]
			IS  = Problem.InitialSubdomains[i]

			# Getting the initialisation values
			Values = Problem.InitialConditions[i](Index, Mesh, EOS, FP, IS)

			# Saving them at the right location
			self.Sol[:,Index] = Values

# ==============================================================================
#	Functions Mapping/Shortcutting classes for problem and solver routines
# ==============================================================================
#### Defining the problem itself and the shortut to the relevant functions #####
class Problem():
	"""
	.. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

	Shortcutting class gathering all the methods and functions that the user targets
	to solve the problem. It sets the problem up upon the solver parameters and the mesh,
	and furnishes the following fields:

		- |bs| **NbFluids**            *(integer)*                   --  number of fluids present initially in the problem's domain
		- |bs| **FluidIndex**          *(integer list)*              --  the ordered list of fluid types corresponding to the fluids present initally in the problem's domain
		- |bs| **InitialSubdomains**   *(shapely multipolygon list)* --  list of the initial fluids subdomains, in the given order of the fluid lists
		- |bs| **StudyDomain**         *(shapely multipolygon)*      --  the considered computational domain
		- |bs| **GoverningEquations**  *(Equations class)*           --  structure containing the methods of flux, jacobian, spectral radius, and a possible Roe matrix
		- |bs| **InitialConditions**   *(function callback list)*    --  list of all the function callbacks defining the initial conditions for each fluid
		- |bs| **FluidProp**           *(Fluids structure list)*     --  list of all the fluids properties correspoding to the list of fluids themselves
		- |bs| **FluidEOS**            *(function callback list)*    --  list of all the equations of state in use for each fluid
		- |bs| **EOSType**             *(string)*                    --  the name of the EOS in use

	|

	"""

	#### Automatic initialisation routine ####
	def __init__(self, ProblemData, Mesh):
		""" Automatic initialisation routine.

		Args:
			ProblemData (Parameters):  the structure containing all the problem data
			Mesh        (MeshStructure):        the mesh in use

		Returns:
			None:       fills direclty the Problem structure
		"""

		# Getting the fluids information from the ProblemData
		self.FluidIndex = ProblemData.FluidIndex
		self.NbFluids   = len(ProblemData.FluidIndex)

		# Getting the considered equation class and the fluid's model associated to the equation
		Setup      = Mappers.Governing_Equations(ProblemData.GoverningEquationsIndex)
		FluidModel = Setup.FluidModel

		# Setting the fluids properties and their equations of state
		self.FluidProp = [Mappers.Fluid_Properties(i, FluidModel) for i in ProblemData.FluidIndex]
		self.FluidEOS  = [Setup.EOS(i) for i in ProblemData.EOSEquationsIndex]

		# Getting the GoverningEquations themselves and retrieving the associated initial conditions and boundary conditions
		self.GoverningEquations = Setup.Equations(self.FluidProp, self.FluidEOS)
		self.InitialConditions  = [Setup.InitialConditions(*id).IC for id in ProblemData.InitialConditions]

		# Registering the Boundary conditions routine per boundary tag and per fluid
		self.BoundaryConditions = np.zeros(ProblemData.BoundaryConditions.shape).tolist()
		for i in range(ProblemData.BoundaryConditions.shape[0]):
			for j in range(ProblemData.BoundaryConditions.shape[1]):
				self.BoundaryConditions[i][j] = Mappers.Boundary_Conditions(*ProblemData.BoundaryConditions[i][j])

		# Setting the initial subdomains where each of the initial fluid lie
		self.StudyDomain, self.InitialSubdomains = SD.GetSubdomains(ProblemData.FluidInitialSubdomain, Mesh)

#### Defining the solver class shortcuting all the routines that should be used #####
class Solver():
	"""
	.. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

	Shortcutting class gathering all the methods and functions that the solver
	requires, upon the user desires and the inherent solver's properties,
	furnishing the following fields:

		- |bs| **FluidSelector**  *(class instance)*           --  FluidSelector instance containing all the required Fluid selector routines
		- |bs| **SpatialScheme**  *(class instance)*           --  Scheme instance, containing the mail "Iteration" method that corresponds to the spatial solver
		- |bs| **SPAdmendements** *(class instance)*           --  Amendements instance, containing all the required routines to limit the scheme (if wished by the user)
		- |bs| **Redistancer**    *(class instance)*           --  Redistancing instance, containing all the required routines to redistance the level set (if wished by the user)
		- |bs| **ControlVolumes** *(string)*                   --  The type of control volume required by the scheme: either "primal" or "dual"
		- |bs| **Reconstructor**  *(function callback list)*   --  Functions that reconstructs the solution and Fluid selector values at physical vertices from the degrees of freedom
		- |bs| **TimeStep**       *(function callback)*        --  Function implementing one time step iteration
		- |bs| **CFL**            *(float)*                    --  A recall of the CFL number
		- |bs| **Tmax**           *(float)*                    --  A recall of the final time

	|
	"""

	""" Sets the solver up, with variables and shortcuts to functions, upon the solver parameters """
	def __init__(self, Problem, Mesh, SolverParams):
		"""
		Args:
			SolverParams (Parameters):  the structure containing all the problem data

		"""

		# Map the quadrature rules to be used within the selected schemes
		SpatialSchemeQuadratures = QR.Quadratures(Mesh, *SolverParams.SchemeQuadParams)
		FSQuadratures            = QR.Quadratures(Mesh, *SolverParams.FSQuadParams)

		# Map the schemes functions to the structure
		Scheme = Mappers.Spatial_Scheme(SolverParams.SchemeId)
		self.ControlVolumes = Scheme.AssociatedMeshType().ControlVolumes
		self.SpatialScheme  = Scheme.Scheme(Problem, Mesh, SpatialSchemeQuadratures, *SolverParams.SchemeParams)
		self.FluidSelector  = Mappers.Fluid_Selector(SolverParams.FluidSpotterId).FS(Problem, Mesh, FSQuadratures, *SolverParams.FluidSpotterParams)
		self.SPAdmendements = SA.Amendements (SolverParams.ScModifiers, Problem, Mesh)
		self.Redistancer    = FR.Redistancing(SolverParams.RedistancerParams, Problem, Mesh)

		# Map the solution's reconstructor to the structure
		self.Reconstructor = [self.SpatialScheme.ReconstructSolutionAtVertices, self.FluidSelector.ReconstructFSAtVertices]

		# Set up the time scheme and relocate the time parameters variables
		self.TimeStep = Mappers.Temporal_Scheme(SolverParams.TimeSchemeId, SolverParams.TimerParams)
		self.CFL      = SolverParams.CFL
		self.Tmax     = SolverParams.Tmax
