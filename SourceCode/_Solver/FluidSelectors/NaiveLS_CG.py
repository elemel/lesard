#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

# ==============================================================================
# Importing python libraries
# ==============================================================================
from shapely.geometry import MultiLineString as ShapelyMultiLineString
from shapely.geometry import MultiPolygon    as ShapelyMultiPolygon
from shapely.geometry import Polygon         as ShapelyPolygon
from shapely.geometry import Point           as ShapelyPoint
from shapely.ops      import unary_union
import matplotlib.pyplot   as plt
import numpy               as np

# Import custom modules
import BarycentricBasisFunctions as BF
import BarycentricTools          as BT

# ==============================================================================
#    Defining the actuall class gathering the externally accessible functions
# ==============================================================================
class FS():
	""".. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

	Classical level set approach, initialised according to a signed distance and iterated
	with a continuous galerkin - lfx scheme, without redistancing.

	.. rubric:: Fields

	All the fields given as arguments at the instance's creation are repeated within
	the structure with the same names (see the parameters below). It also contains the
	further field

	- |bs| **FluidSelectorName**  *(string)* -- the name of the level set
	- |bs| **Order**              *(integer)*           -- The order of the used basis functions
	- |bs| **InnerQWeigths**      *(float numpy array)* -- Weights of the quadrature points of each element (NbInnerElements x NbQuadraturePoints)
	- |bs| **InnerQGFValues**     *(float numpy array)* -- Value of the gradient of the basis function at the quadrature points of each element (NbInnerElements x NbQuadraturePoints x NbBasisFunc x dim)
	- |bs| **InnerQPoints**       *(float numpy array)* -- Quadrature points of each element (NbInnerElements x NbQuadraturePoints)
	- |bs| **FaceWiseQWeigths**   *(float numpy array)* -- Weights of the quadrature points of each element's face (NbInnerElements x NbFace x NbQuadraturePoints)
	- |bs| **FaceWiseQPoints**    *(float numpy array)* -- Quadrature points of each element's face (NbInnerElements x NbFace x NbQuadraturePoints)
	- |bs| **FaceWiseQBFValues**  *(float numpy array)* -- Value of the basis function at the quadrature points of each element's face (NbInnerElements x NbFace x NbQuadraturePoints x NbBasisFunc)

	|
	"""

	#### Automatic initialisation routine ####
	def __init__(self, Problem, Mesh, QuadratureRules, *Params):
		"""
		Args:
			Problem          (Problem):               the considered problem
			Mesh             (MeshStructure):         the considered mesh instance
			QuadratureRules  (Qudratures):            instance of quadrature class to be used when evaluating integral within the scheme
			Params           (optional, strings):     the level set's parameters as wished by the user, the first element being the order (integer)

		|

		.. rubric:: Methods
		"""

		# Setting the name of the class to be accessible from outside
		self.FluidSelectorName = "Naive Level Set -- Continuous Galerkin"

		# Setting the mesh and parameters for an internal access
		self.Problem = Problem
		self.Mesh    = Mesh
		self.QuadratureRules = QuadratureRules

		# Extracting the parameters from the user input list
		if len(Params)==2:
			self.Order           = int(Params[0])
			self.VariationalType = Params[1]
		else: raise ValueError("Wrong number of user input argument when definining the fluid selector. Check you problem definition.")

		# Precomputing the basis function values at quadrature points for fastening the run time
		self.PreEvaluateQuadratureQuantities()


	# **********************************************************
	# *  Definition of the Fluid spotter's type and dynamic    *
	# **********************************************************

	#### External initialisation routine ####
	def Initialiser(self, Solution):
		""" External initialisation routine.

		Args:
			Solution  (Solution structure): the solution instance the scheme is working on

		Returns:
			LSValues  (float numpy array):   the level set values at the MeshVertex and Dofs, to be copied to the Solution structure (NbSubdomains x (NbMeshPoints+NbDofs))
		"""

		# --------------- Shortcutting the required values ----------------------------------------------
		# Getting the subdomains list
		Subdomains = Solution.Subdomains

		# Recompute the number of dofs to drop the dependency on the mesh
		NbFluids = len(Subdomains)									# Valid because straight after initialisation
		NbDofs   = len(Solution.FluidFlag)							# Only dofs are there

		# Initialising the Level set values
		LSValues = np.zeros((len(Subdomains), len(Solution.FluidFlag)))

		# ------------- Get the distance from the interfaces --------------------------------------------
		# Evaluate the distance from Dofs to each boundary, and mark them by +-
		# depending on their location with respect to the fluid's boundary
		# Return the LS value for each fluid and for each Dof # may lead to non-precise determination
		# in case of multiple junctions (side effect of the classicall method.)

		# Extract the boundary of the domain
		Borders = []
		for sub in self.Problem.StudyDomain:
			Borders += [tuple(tuple(a) for a in np.array(sub.exterior.coords.xy).T)]\
					  +[tuple(tuple(a) for a in np.array(interior.coords.xy).T) for interior in sub.interiors]
		DomainBorder = ShapelyMultiLineString(Borders)

		# If we have only one fluid, set the LSValues everywhere equal to 1 for compatibility, otherwise fill the LSValues structure
		if  NbFluids==1: LSValues[:,:] = 1.
		else:
			# Looping on the intial number of fluids present in the computational domain
			for i in range(NbFluids):

				# Get the subdomain as a shapely multipolygon
				FluidArea = Subdomains[i]

				# Extract the boundary of the fluid
				Borders = []
				for sub in FluidArea:
					Borders +=  [tuple(tuple(a) for a in np.array(sub.exterior.coords.xy).T)]\
							   +[tuple(tuple(a) for a in np.array(interior.coords.xy).T) for interior in sub.interiors]
				FluidBorder = ShapelyMultiLineString(Borders)

				# Mask the parts that coincide with the domain's boundary itself
				FluidBorder = FluidBorder.difference(DomainBorder)

				# Computing the distance and location w.r.t. the subdomain through the shapely information
				Value = np.array([[ShapelyPoint(tuple(pt)).distance(FluidBorder),\
									int(2*FluidArea.intersects(ShapelyPoint(tuple(pt)).buffer(1e-14))-1)] \
									for pt in self.Mesh.DofsXY]).T

				# Computing the signed distance
				LSValues[i,:] = np.prod(Value, axis=0)

		# Returning the fresh LS values
		return(LSValues)

	#### Gives the dynamic of the Fluid's spotter through its flux ####
	def Flux(self, Var, Motion, *args):
		""" Flux function of the (conservative) EulerEquations

		Args:
			Var        (float numpy array):   the value of the Level Set values (NbFluids x NbGivenPoints)
			Motion     (float numpy array):   the velocity array (2xNbPoints)

		Returns:
			Flux       (3D numpy array):      the obtained flux (spatialdim x NbVars x NbPoints)

		.. note::

			*args is there for compatibility reason when e.g. a location xy is given when called
		"""

		# Determining the advection point-wise
		Flux = np.array([Motion[0,:]*Var, Motion[1,:]*Var])

		# Returning the values
		return(Flux)

	#### Jacobian of the equations set ####
	def Jacobian(self, Var, Motion, *args):
		""" .. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

		Computes the Jacobian of the flux for the advection of the LevelSet

		Args:
			Var         (float numpy array):    the value of the Level Set values (NbFluids x NbGivenPoints)
			Motion      (float numpy array):    the velocity array (2xNbPoints)

		Returns:
			J    (3D numpy array):   J[:,:,i] gives the jacobian of the flux taking care of the dynamic of the ith spatial coordinate.

		|

		.. note::

			- |bs| For each flux fi = (fi1,..., fin), the returned Jacobian reads:

			  | |bs| J[:,:] = [dfi1/dx1, ....., dfi1/dxn
			  | |bs| |bs| |bs|	|bs| |bs| |bs|	 ....
			  | |bs| |bs| |bs|	|bs| |bs| |bs|	df in/dx1, ....., dfin/dxn]

			- |bs| *args is there for compatibility reason when e.g. a location xy is given when called the x-y locations at which the variables are given  (NbGivenPoints x 2)
		"""

		# ------ Initialising the Jacobian structure ---------------------------
		# Getting the number of furnished points
		NbPoints = np.shape(Motion)[1]
		J = np.zeros((self.Problem.NbFluids, self.Problem.NbFluids, 2, NbPoints))

		for nf in range(self.Problem.NbFluids):
			J[nf, nf, 0, :] = np.array(Motion[0,:])
			J[nf, nf, 1, :] = np.array(Motion[1,:])

		# Returning the Jacobian
		return(J)

	#### Spectral radius of the equations set ####
	def SpectralRadius(self, Var, Motion, n, *args):
		""" Computes the spectral radius associated to the motion flux.

		Args:
			Var         (2D numpy array):         the variables of the problem (NbVars x NbGivenPoints)
			Motion      (float numpy array):      the velocity array (2xNbPoints)
			n           (2D numpy array):         the x-y values of the normal at each given point  (NbGivenPoints x 2)

		Returns:
			Lmb         (numpy array):            the array containing the spectral radius at each given point

		.. note::

			*args is there for compatibility reason when e.g. a the xy locations at which the variables are given  (NbGivenPoints x 2)
		"""

		# Retrieving the Jacobian
		J = self.Jacobian(Var, Motion)

		# Computing the spectral radius at each given point, shortcuted since here we are scalar
		Lmb = np.max(np.abs([np.sum(J[i,i,:,:]*n.T, axis = 0) for i in range(self.Problem.NbFluids)]), axis=0)

		# Returning the obtained values
		return(Lmb)


	# ************************************************************************
	# *  Definition of the Fluid spotter's spatial impact and rectification  *
	# ************************************************************************

	#### Determining the fluid flags depending on the various values of the level set #####
	def FlagDofs(self, Solution):
		""" Flags the degrees of freedom according to the different values of the
			associated level set.

			Args:
				Solution  (Solution structure):  the solution instance the scheme is working on

			Returns:
				None: updating directly the value of FluidFlag in the solution instance
		"""

		# ---------------- Updating the flags at Dofs ------------------------------
		Solution.FluidFlag = np.argmax(Solution.LSValues, axis=0)

	#### Determining the fluid flags at a specific points locations given a cell (primal) and the considered barycentric coordinates #####
	def FlagPoints(self, LSValues, CellId=-1, x=[], FaceId=-1):
		""" Flags the degrees of freedom according to the different values of the
			associated level set.

			Args:
				LSValues  (float numpy multidarray):  the vector within which the values of the level set at dofs are stored (full vector in the same format as stored in Solution.LSValues)
				CellId    (integer):                  the id of the primal cell the points under consideration lie on (important esp. for points on the cell's boundary)
				x         (2D numpy array):           (optional) barycentric coordinates of the point under consideration, ordered as the physical vertices of the element (NbPoints x NbBarycoor)
				                                      If not given the FlagDofs will be simply computed at the Dofs of the CellId.
				FaceId    (integer):                  When CellId and x are given, the face where the given point lie (important for code speedup only)

			Returns:
				Flags (integer list):  If CellId and x are given, the list of indices corresponding to the fluid located at the positions given in x.
				                       If CellId is given but not x, the list of indices corresponding to the fluid located at the Dofs (according to their FV representation)
				                       If no CellId neither x is given, the vector within which the values of the level set at dofs are stored (full vector in the same format as stored in LSValues)
		"""

		# ---------------- Updating the flags at Dofs ------------------------------

		#---- Case where we want to flag on a point within a cell, that is directely mappable to a degree of freedom (e.g. quadrature point) of the FS scheme
		if CellId>-1 and FaceId>-1 and len(x)>0 and np.size(x)==np.size(self.FaceWiseQPoints[CellId][FaceId]) and np.all(np.isclose(x, self.FaceWiseQPoints[CellId][FaceId])):
			# Get the variational information
			dele  = np.array(self.Mesh.ElementsDofs[CellId])
			LS    = LSValues[:, dele]

			# Evaluate the basis function values at the given points x
			EvaluatedBase = self.FaceWiseQBFValues[CellId][FaceId]

			# Evaluate the local values
			LSloc = np.dot(LS, EvaluatedBase.T)

			# Returnin the flags at the evaluated points
			return(np.argmax(LSloc, axis=0))

		elif CellId>-1 and len(x)>0 and np.size(x)==np.size(self.InnerQPoints[CellId]) and  np.all(np.isclose(x, self.InnerQPoints[CellId])):
			# Get the variational information
			dele  = np.array(self.Mesh.ElementsDofs[CellId])
			LS    = LSValues[:, dele]

			# Evaluate the basis function values at the given points x
			EvaluatedBase = self.InnerQBFValues[CellId]

			# Evaluate the local values
			LSloc = np.dot(LS, EvaluatedBase.T)

			# Returnin the flags at the evaluated points
			return(np.argmax(LSloc, axis=0))

		#---- Case where we want to flag on a point within a cell, that is not directely mappable to a degree of freedom (e.g. quadrature point)
		elif CellId>-1 and len(x)>0:
			# Get the variational information
			dele  = np.array(self.Mesh.ElementsDofs[CellId])
			LS    = LSValues[:, dele]

			# Evaluate the basis function values at the given points x
			Nele = len(self.Mesh.InnerElements[CellId])
			EvaluatedBase = BF.BarycentricBasis(Nele, "B", self.Order, x)

			# Evaluate the local values
			LSloc = np.dot(LS, EvaluatedBase.T)

			# Returnin the flags at the evaluated points
			return(np.argmax(LSloc, axis=0))

		#---- Case where we want to flag all the degrees of freedom of a given cell CllId
		elif CellId>-1:
				# Retrieve the FluidSpotter state at the Dofs of the cell
				dele  = np.array(self.Mesh.ElementsDofs[CellId])
				LS    = LSValues[:, dele]

				# Return the flag at all dofs (in the same order of the storage)
				return(np.argmax(LS, axis=0))

		# ---- Case where we want to flag upon a given fluid spotter state without requiring any spatial information
		else:
			# Return the flag at all given points (in the same order of the storage)
			return(np.argmax(LSValues, axis=0))

	#### Routine that maps the values from the Dofs to the Physical vertices of the mesh ####
	def ReconstructFSAtVertices(self, Solution):
		""" Routine that maps the values from the Dofs to the Physical vertices of the mesh
		of the FluidSpotter and flags.

		Args:
			Solution  (Solution):  the currently being computed solution

		Returns:
			None: fills direclty the RSol and RFluidFlag values in the data structure

		.. Note::

			This is only for later plotting purposes.
		"""

		# -------------- Initialising the reconstructed values ----------------------------
		Solution.RFluidFlag  = np.zeros(self.Mesh.NbMeshPoints)
		Solution.RLSValues   = np.zeros((np.shape(Solution.LSValues)[0],  self.Mesh.NbMeshPoints))

		# ------------- Reconstructing the values -----------------------------------------
		# Extract the cartesian coordinates of the Dofs
		xyDofs = self.Mesh.DofsXY[:,:]

		# Spanning all the mesh points where to fill the values
		for i in range(self.Mesh.NbMeshPoints):

			# Averaging all the Dofs values located at the corresponding physical vertex
			index = self.Mesh.Vertex2Dofs[i]
			RS  = np.mean(Solution.LSValues[:,index], axis=1)
			FF  = int(np.mean(Solution.FluidFlag[index]))

			# Fill the reconstructed solution upon the found index
			Solution.RLSValues[:,i] = RS
			Solution.RFluidFlag[i]  = FF

	#### Recomputing the subdomains according to the new values of the level set #####
	def UpdateSubdomains(self, Solution):
		""" Method modifying the subdomains associated to the Solution upon the values of
		LSValues.

		Args:
			Solution  (Solution):  the solution instance the scheme is working on

		Returns:
			None: updating directly the subdomains in the solution instance

		.. warning:: Not safe yet if the first path given out is internal
		"""

		# ------- Updating the subdomains from the countour values -----------------
		# Matplotlib initialisation to get back the countours
		fig = plt.figure()
		ax = fig.add_subplot(2, 1, 1)

		# Update each of the fluid's locations
		for i in range(self.Problem.NbFluids):
			# ---------- Initialisations --------------------------------------------
			# Buffering the LSvalues to modify them on the edges upon our wishes to infer closed contours
			Buffer = Solution.LSValues[:,:]

			# ----------- Editing the LS value with a pythonic hack -----------------
			# Force the boundary of the domain to be part of the contour if relevant
			ind = np.where(Solution.LSValues[i,self.Mesh.BoundaryDofs]>=0)[0]
			Buffer[i,np.array(self.Mesh.BoundaryDofs)[ind]]=0

			cs = ax.tricontour(self.Mesh.DofsXY[:,0], self.Mesh.DofsXY[:,1],\
					           self.Mesh.ElementsBoundaryDofs, Buffer[i,:], [0])

			# ----------- Extracting the contour of the level set -------------------
			# And transforming into a shapely mutlipolygon

			# Extracting the 0 level of the function and creating a polygon out of it,
			# making sure that it is not self-intersecting
			LV0 = cs.allsegs[0]
			polygon = ShapelyPolygon([tuple(a) for a in np.array(LV0[0])]).buffer(0)

			# Spanning over the possibly disconnected area encompassing the target fluid
			for contour in LV0[1:]:
				# Determining whether the patch is out or in the previously registered polygon
				# and adding/removing it upon
				patch = ShapelyPolygon([tuple(a) for a in np.array(contour)]).buffer(0)
				if patch.difference(polygon).is_empty == False:	polygon = polygon.union(patch)
				else: polygon = polygon.difference(patch)

			# Converting it to multipolygon for compatibility with other parts of the code (esp. plots)
			if not (polygon.geom_type == 'MultiPolygon'): polygon = ShapelyMultiPolygon([polygon])

			# Registering the new shape
			Solution.Subdomains[i] = polygon

		plt.close(fig)

	#### Redistancing the LSvalues according to the freshly computed level set values ####
	def Redistancing(self, Solution):
		""" Redistances the Level set values upon the computed subdomains.

		Args:
			Solution  (Solution):  the solution instance the scheme is working on

		Returns:
			None:  updating directly the LSValues in the solution instance
		"""

		# Redistancing the points with the nearest
		# Looping on the intial number of fluids present in the computational domain
		for i in range(self.Problem.NbFluids):
			# Computing the distance and location w.r.t. the subdomain through the shapely information
			FluidArea = Solution.Subdomains[i]
			Value = np.array([[FluidArea.boundary.distance(ShapelyPoint(tuple(pt))),                 \
			                   int(2*FluidArea.intersects(ShapelyPoint(tuple(pt)).buffer(1e-14))-1)] \
							   for pt in self.Mesh.DofsXY]).T

			# Computing the signed distance
			Solution.LSValues[i,:] = np.prod(Value, axis=0)


	# ****************************************************************************
	# *  Definition of the Fluid spotter's evolution's process (e.g. advection)  *
	# ****************************************************************************

	#### Emulates a flux computation as done in the iteration routine ####
	def ComputeFlux(self, U, LSValues):
		""" Emulates a flux computation as done in the iteration routine in order
			to be used in the DeC subtimestep evaluation.

		Args:
			U           (numpy float array):  buffer containing the current state values at each dof (NbVars   x NbDofs)
			LS          (numpy float array):  buffer containing the current FluidSpotters values at each dof(NbFluids x NbDofs)

		Returns:
			fluxes      (numpy float (multiD)array):   fluxes in the format requires by the UpdateFSValues routine
		"""

		# --------------------------  Initialisations -------------------------------------------------
		# Computing the quantities for allocating the flux vector big enough (dirty hack for vectorialisation)
		MaxInnerQp = np.max([len(self.InnerQWeigths[i])       for i in range(0, self.Mesh.NbInnerElements)])													# Computing the space needed for the inner quadrature points
		MaxBdQp    = np.max([len(self.FaceWiseQWeigths[i][f]) for i in range(0, self.Mesh.NbInnerElements) for f in range(len(self.Mesh.InnerElements[i]))])	# Computing the space needed for the boundary quadrature points

		# Defining the output fluxes (inner and boundary ones)
		# The very first coordinate corresponds to the inner flux. Only Flux[0, :, 0, :, :, :] is filled
		# The second coordinate corresponds to the inner flux.     Only Flux[1, :, :, :, :, 0:3] is filled
		fluxes = np.zeros((2, self.Mesh.NbInnerElements, 3, 2, self.Problem.NbFluids, np.max([MaxInnerQp, MaxBdQp])))

		# ------------- Flux computation at the control's volume interface ----------------------------
		# Spanning on each element
		for i in range(self.Mesh.NbInnerElements):
			# ---- Getting the element and state informations
			# Retrieving the physical information on the spanned element's vertices
			ele = self.Mesh.InnerElements[i]
			vol = self.Mesh.InnerElementsVolume[i]

			# Retrieving the information on the local degree of freedom
			dele = np.array(self.Mesh.ElementsDofs[i])
			LS   = LSValues[:, dele]
			UU   = U[:, dele]

			# Get the coordinates of the vertices in the physical order
			elevert = self.Mesh.CartesianPoints[ele,:]

			# ---- Computing the plain integral
			# Retrieving the quadrature points as barycentric coordinates and convert them to cartesian
			# in the Dof's vertex ordering referential
			bqp      = self.InnerQPoints[i]
			weights  = self.InnerQWeigths[i]
			qp = BT.GetCartesianCoordinates(bqp, elevert)

			# Evaluate the basis function in the order of the Dofs
			EvaluatedBase  = self.InnerQBFValues[i]

			# Reconstructing the solution and its gradient at the quadrature point
			Uloc  = np.dot(UU, EvaluatedBase.T)
			LSloc = np.dot(LS, EvaluatedBase.T)

			# Compute the flux according to the fluid's flag
			UV = self.Problem.GoverningEquations.GetUV(Uloc, qp)
			MidFluxes = self.Flux(LSloc, UV)
			fluxes[0, i, 0, :, :, 0:len(self.InnerQWeigths[i])] = MidFluxes

			for face in range(len(ele)):
				# Get the coordinates of the quadrature points on the edge, flipping the
				# points to match the Dof's order with the physical vertices
				qbbp    = self.FaceWiseQPoints[i][face]
				weights = self.FaceWiseQWeigths[i][face]
				bqp = BT.GetCartesianCoordinates(qbbp, elevert)

				# Compute each basis function value at those points
				# (get the order of the basis in the order of the physical vertices:
				# the dele have to be ordered in the same output as the evaluated base:
				# check also for higher order the order of the dele given in the mesh and
				# the order of the basis function given here)
				EvaluatedBase = self.FaceWiseQBFValues[i][face]

				# Reconstruct the solution's value at this point
				Uloc  = np.dot(UU,  EvaluatedBase.T)
				LSloc = np.dot(LS, EvaluatedBase.T)

				# Compute the flux
				UV = self.Problem.GoverningEquations.GetUV(Uloc, bqp)
				MidFluxes = self.Flux(LSloc, UV)
				fluxes[1,i,face,:,:,0:len(self.FaceWiseQWeigths[i][face])] = MidFluxes

		# Returning the full array
		return(fluxes)

	#### (Crucial routine) Spatial scheme to update the level set ####
	def Iteration(self, Solution, fluxes, i, du=0, dt=1):
		""" Main iteration of the scheme, implementing the continuous galerkin scheme.

		Args:
			Solution   (Solution):             structure containing the current solution's values to iterate
			fluxes     (numpy (multiD)array):  pre-computed fluxes at the points of interest. For this scheme, access with fluxes[element, face, coordinate(fx or fy), variable, pointindex]
			i          (integer):              the index of the considered element within which the partial residuals will be computed
			du         (float numpy array):    (optional) when using DeC, the difference in the time iteration
			dt         (float):                (optional) when using DeC, the full time step

		Returns:
			Resu       (float numpy array):    the computed residuals (NbVars x NbDofs)
		"""

		# -------- Initialisation ------------------------------------------------
		# Getting the dofs contaned in the considered element
		dele   = np.array(self.Mesh.ElementsDofs[i])
		NbDofs = len(dele)

		# Getting the info about the number of quadrature points
		NbInnerQp = len(self.InnerQWeigths[i])
		NbBdQp    = [len(self.FaceWiseQWeigths[i][f]) for f in range(len(self.Mesh.InnerElements[i]))]

		# Initialising the mollification vector (residuals)
		Resu  = np.zeros([np.shape(Solution.LSValues)[0], NbDofs])

		# ------- Getting the CG residuals for each element -------------------------

		# ---- Getting the element and state informations
		# Retrieving the physical information on the spanned element's vertices
		ele = self.Mesh.InnerElements[i]
		vol = self.Mesh.InnerElementsVolume[i]
		n   = self.Mesh.ElementsBoundaryWiseNormals[i]

		# Retrieving the information on the local degree of freedom
		coords = self.Mesh.DofsXY[dele,:]
		Vars   = Solution.Sol[:,dele]
		LS     = Solution.LSValues[:,dele]
		dU     = du[:, dele]

		# ---- Computing the plain integral
		# Retrieving the quadrature points as barycentric coordinates and convert them to cartesian
		# in the Dof's vertex ordering referential
		bqp      = self.InnerQPoints[i]
		weights  = self.InnerQWeigths[i]

		# Evaluate the basis function in the order of the Dofs
		EvaluatedBase      = self.InnerQBFValues[i]
		EvaluatedGradients = self.InnerQGFValues[i]

		# Reconstructing the solution and its gradient at the quadrature point
		dUloc = np.dot(dU, EvaluatedBase.T)

		# Retrieving the inner fluxes
		Flux = fluxes[0, i, 0, :, 0:NbInnerQp]

		# Complete the resodual at each Dof by the relevant quadrature value
		for dof in range(len(dele)):
			# Evaluating the flux at the quadrature point for the reconstructed flux variable
			qt  = np.sum([weights[q]*(EvaluatedGradients[dof,q,0]*Flux[0,:,q]+EvaluatedGradients[dof,q,1]*Flux[1,:,q]) for q in range(len(weights))], axis=0)
			DEC = np.sum( weights[:]*EvaluatedBase[:,dof]*dUloc, axis=1)

			# Updating the residual at the spanned Dof by the local contribution
			Resu[:,dof] += (DEC - dt*qt)*vol

		# ---- Computing the boundary integral
		# Spanning each edge in the physical element
		for face in range(len(ele)):
			# Get the coordinates of the quadrature points on the edge, flipping the
			# points to match the Dof's order with the physical vertices
			qbbp    = self.FaceWiseQPoints[i][face]
			weights = self.FaceWiseQWeigths[i][face]

			# Compute each basis function value at those points
			# (get the order of the basis in the order of the physical vertices:
			# the dele have to be ordered in the same output as the evaluated base:
			# check also for higher order the order of the dele given in the mesh and
			# the order of the basis function given here)
			EvaluatedBase = self.FaceWiseQBFValues[i][face]

			# Evaluate the flux there
			Flux = fluxes[1,i,face,:,:,0:NbBdQp[face]]

			# Compute the flux's normal contribution to each dof
			Qt = (Flux[0,:,:]*n[face,0]+Flux[1,:,:]*n[face,1])
			WeigthedBaseWiseQt = np.array([weights[pt]*np.array([Qt[:,pt]]).T*np.array([EvaluatedBase[pt,:]]) for pt in range(len(weights))])
			PonderedQt = np.sum(WeigthedBaseWiseQt, axis=0)

			# Adjust the residuals
			Resu[:,:] += dt*PonderedQt

		# Returning the partial residuals
		return(Resu)

	#### Wrapper routine to update the quantitites that depend on the UpdateFSValues output ####
	def UpdateFSVAttributes(self, Solution):
		""" Wrapper routine to update the quantitites that depend on the UpdateFSValues fresh output

		Args:
			Solution   (Solution):  the solution instance the scheme is working on

		Returns:
			None: updating directly the subvalues in the solution instance
		"""

		#self.UpdateSubdomains(Solution)
		#self.Redistancing(Solution)
		self.FlagDofs(Solution)


	# **************************************************************
	# *  Definition of the Fluid spotter's scheme basis function   *
	# **************************************************************

	# Precomputing and storing the values of the basis functions at the quadrature points of each element
	def PreEvaluateQuadratureQuantities(self):
		"""
		Precomputing and storing the values of the basis functions at the quadrature points of each element

		Args:
			None:  Considers the given parameters of the class directly

		Returns:
			None: Creates the fields InnerBFValues and FaceWiseBFValues in the scheme structure, containing
			      the values of each basis function at each quadrature point for each element and face.

		"""

		# Initialising the long-term storing vectors
		self.InnerQBFValues    = [[]]*self.Mesh.NbInnerElements
		self.InnerQWeigths     = [[]]*self.Mesh.NbInnerElements
		self.InnerQGFValues    = [[]]*self.Mesh.NbInnerElements
		self.InnerQPoints      = [[]]*self.Mesh.NbInnerElements
		self.FaceWiseQWeigths  = [[]]*self.Mesh.NbInnerElements
		self.FaceWiseQPoints   = [[]]*self.Mesh.NbInnerElements
		self.FaceWiseQBFValues = [[]]*self.Mesh.NbInnerElements

		# Spanning each element
		for i in range(self.Mesh.NbInnerElements):

			# Get the elements properties
			ele = self.Mesh.InnerElements[i]
			vol = self.Mesh.InnerElementsVolume[i]
			n   = self.Mesh.ElementsBoundaryWiseNormals[i]

			# Get the type of the element through its vertices numbers
			self.FaceWiseQBFValues[i] = [[]]*len(ele)
			self.FaceWiseQWeigths[i]  = [[]]*len(ele)
			self.FaceWiseQPoints[i]   = [[]]*len(ele)

			# Computing the basis functions values at the inner quadrature points
			bqp, weights = self.QuadratureRules.InnerQuadrature(len(ele))
			self.InnerQPoints[i]   = bqp
			self.InnerQWeigths[i]  = weights
			self.InnerQBFValues[i] = BF.BarycentricBasis    (len(ele), self.VariationalType, self.Order, bqp)
			self.InnerQGFValues[i] = BF.BarycentricGradients(len(ele), self.VariationalType, self.Order, bqp, n, vol)

			# Computing per face the basis functions values at the edge's quadrature point
			for face in range(len(ele)):
				qbbp, weights = self.QuadratureRules.BoundaryQuadrature(len(ele), face)
				self.FaceWiseQPoints[i][face]   = qbbp
				self.FaceWiseQWeigths[i][face]  = weights
				self.FaceWiseQBFValues[i][face] = BF.BarycentricBasis(len(ele), self.VariationalType, self.Order, qbbp)
