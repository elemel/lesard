#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

"""
.. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

|

Module that defines the DeC time iteration method, adapted to the residual distribution framework.
The main iteration is given by the routine :code:`DeC`; direclty referenced and mapped in the :code:`Mappers.py` file.

|


.. Note::
    - |bs| Adapted to RD and python from the Julia code of Davide Torlo
    - |bs| A standalone version for ODE with examples is available on request

|

"""

# ==============================================================================
#	Preliminary imports
# ==============================================================================

# Python and math imports
import numpy            as np
import numpy.polynomial as npp
import copy

# Custom libraries import
import LocalLib.LobattoQuadratureNodes as Lbt
import _Solver.Solver_Spatial          as SPL

# ==============================================================================
#	Tools
# ==============================================================================

#### Get the Lagrange polynomial's value ####
def LagrangeBasis(X,t,i):
	""" Interpolation of the ith Lagrange polynomial, evaluated at the point t given the nodes X

	Args:
		X  (float array-like):  the list of nodes that are constructing the Lagrange interpolation polynomial
		t  (float):             the point for which to evaluate the Lagrange polynomial
		i  (integer):           the considered Lagrange polynomial basis (w.r.t. the nodes order given in X)

	Returns:
		v  (float):  the interpolated value of the ith Lagrange polynomial at the location t
	"""

    # ------------------- Computations ------------------------------------------------------
	# Aborting if the asked basis number is unrealistic
	if (i<0 or i>=len(X)): raise(ValueError("Error in the Lagrange interpolation in DeC: \n\
	                                        Wrong number of basis function."))

	# Interpolating the value
	Val = np.prod([(t-X[j])/(X[i]-X[j]) for j in range(len(X)) if j != i], axis = 0)

	# Return the interpolated value
	return(Val)

#### Get the interpolation (quadrature) nodes ####
def Nodes(order, typpe):
	"""
	Retrieves the interpolation (quadrature) nodes between 0 and 1, depending on the wished interpolation order
	and the nodes type.

	Args:
		order (integer):     the wished interpolation order
		typpe (string):      the wished type of nodes. Implemented types are: "Equispaced", "GaussLobatto"

	.. rubric:: Returns

	|bs| -- **Nt** *(float numpy array)* --  the interpolation nodes

	|bs| -- **W**  *(float numpy array)* --  the associated quadrature weights
    """

    # ------------------- Computations ------------------------------------------------------
	# Computing and scaling the nodes to [0,1]
	if typpe == "Equispaced":
		Nt = np.linspace(0, 1, order)
		W  = (1/order)*np.ones(order)

	elif typpe == "GaussLobatto":
		Nt, W = Lbt.GaussLobatto(order-1, 1e-16)
		Nt = Nt*0.5 + 0.5
		W  = W*0.5


	else: raise(NotImplementedError("Error in the quadrature nodes definition for DeC. \n\
	                                 Nodes types not yet defined there. Abortion."))

	# Giving back the values in a tuple
	return (Nt, W)


# ==============================================================================
#	Actual DeC functions
# ==============================================================================

#### Compute the theta coefficients for DeC ####
def ThetaDec(order, typpe):
	""" Computes the theta

	Args:
		order (integer):  the wished interpolation order
		typpe (string):   the wished type of nodes. Implemented types are: "Equispaced", "GaussLobatto"

	Returns:
		theta (float numpy array):   matrix of values for DeC use (order x order)
    """

    # ------------------- Computations ------------------------------------------------------
	# Retrieve the quadrature nodes
	Nt,   W = Nodes(order, typpe)
	INt, IW = Nodes(order, "GaussLobatto")

	# Generate theta coefficients
	theta = np.zeros((order, order))
	for m in range(order):
		Nm = INt*Nt[m]
		Wm = IW*Nt[m]

		for r in range(order):
			theta[r,m] = np.sum(LagrangeBasis(Nt, Nm, r)*Wm[:])

	# Return the DeC weights values
	return(theta)

#### Actual DeC routine ####
def DeC(schemeSC, schemeFS, dt, Problem, Mesh, Sol, M, K, typpe = "Equispaced"):
	""" Provides a full time iteration according to the DeC method

	.. rubric:: Functional inputs

	|bs| -- **schemeSP** *(scheme module list)* --  handler of the spatial scheme module, should be vectorised for using on several (spatial) points simultaneously followed (if any) by the list of its amendments (limiter, jumps etc)

	|bs| -- **schemeFS** *(scheme module list)* --  handler of the FluidSpotter scheme module, should be vectorised for using on several (spatial) points simultaneously followed (if any) by the list of its amendments (limiter, jumps etc)

	.. rubric:: Problem inputs

	|bs| -- **Problem**  *(Problem)*       --    the considered problem

	|bs| -- **Mesh**     *(MeshStructure)* --    the considered mesh

	.. rubric:: Time-dependent inputs

	|bs| -- **dt**  *(float)* --                 time span on which to evolve the solution

	|bs| -- **Sol** *(Solution structure)* --    the initial condition, as a solution structure

	.. rubric:: Input parameters:

	|bs| -- **M**     *(integer)* --              the number of subtimesteps

	|bs| -- **K**     *(integer)* --              the number of corrections

	|bs| -- **typpe** *(string)* --               type of the interpolation (quadrature) nodes to be used: "Equispaced", "GaussLobatto".  Default is set as Equispaced

	.. rubric:: Returns

	|bs| -- *Tspan (float)*         --   the full time step interval

	|bs| -- *U (float numpy array)* --   the updated solution (NbVars x (NbGivenPoints))
    """

	# --------- Initilising the variables --------------------------------------
	# Initialising the variables of interest and buffering partially the quantities
	# of interest in the solution (either the Solution itself or the level set only)
	SolBuff = copy.deepcopy(Sol)
	U  = Sol.Sol
	LS = Sol.LSValues

	# Getting the dimension of the problem and furninshing
	NbVars,   NbPoints = np.shape(U)
	NbFluids, NbPoints = np.shape(LS)

	# Initialising the buffer variables (of the size of each subtimestep)
	up = np.zeros((NbVars,   NbPoints, M+1))
	ua = np.zeros((NbVars,   NbPoints, M+1))
	lp = np.zeros((NbFluids, NbPoints, M+1))
	la = np.zeros((NbFluids, NbPoints, M+1))

	# --------- Computing one DeC time step ------------------------------------
	# Computing the DeC weights
	Theta = ThetaDec(M+1, typpe)


	# Looping over the subtimesteps
	for m in range(M+1):
		# Initialising the running variable upon the intiial solution's value
		ua[:,:,m], up[:,:,m] = copy.deepcopy(U),  copy.deepcopy(U)
		la[:,:,m], lp[:,:,m] = copy.deepcopy(LS), copy.deepcopy(LS)

	# Looping over the corrections
	for k in range(1, K+1):
		# Getting back the previous value of the solution
		up = copy.deepcopy(ua)
		lp = copy.deepcopy(la)

		# Computing the correction to the solution
		for m in range(1, M+1):
			# Retrieving a RD-adapted increment on the residuals -- rq: If not RD,
			# np.sum(np.array([Theta[r,m]*scheme(up[:,:,r]) for r in range(M+1)]), axis = 0)
			# should be enough
			[upres, lpres] = SPL.RD(Theta, m, M, dt, schemeSC, schemeFS, Problem, Mesh, SolBuff, up, lp)

			# Retrieving a DeC-adapted increment on the residuals
			ua[:,:,m] = up[:,:,m] - upres[:,:]
			la[:,:,m] = lp[:,:,m] - lpres[:,:]

	# Updating the subtime step
	U  = ua[:,:,M]
	LS = la[:,:,M]

	# Returning the full timestep
	return([U, LS])
