#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

# File defining the Dirichlet boundary conditions, located in :code:`BoundaryConditions/Dirichlet`


# ==============================================================================
# Dirichlet boundary conditions
# ==============================================================================

def Dirichlet(Problem, Mesh, Solution, Residuals, FluidId, Bd, BdIndex, Value, *params):
	"""
	Routine that does nothing, just lets outflow (there for compatibility)

	Args:
		Problem    (Problem):            the considered problem
		Mesh       (MeshStructure):      the considered mesh
		Solution   (Solution):           the considered solution (as of the previous subtimestep (as used in the temporal scheme))
		Residuals  (2D numpy array):     the value of the currently computed residuals(NbVars x NbDofs)
		FluidId    (integer):            the index of the considered fluid (warning: NOT its value in mappers)
		Bd         (integer):            the tag of the boundary the considered points are lying on
		BdIndex    (integer list):       the list of indices of the degrees of freedom to treat
		Constant   (floar):              the constant value to force on the degrees of freedom

	Returns:
		res        (2D float numpy array):  the updated residuals

	.. note::

		*args is here only for compatibility on call	
	"""

	return(Value+0*Residuals[:,BdIndex])
