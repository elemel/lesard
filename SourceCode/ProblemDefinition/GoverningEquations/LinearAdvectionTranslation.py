#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

"""

All the necessary routines that implements a
simple 2D translation, linear advection are defined in
:code:`GoverningEquations/LinearAdvectionTranslation.py`
"""

# ==============================================================================
#	Preliminary imports
# ==============================================================================
import numpy as np


# ==============================================================================
#	Parameter that should be accessible from outside the class
# ==============================================================================
FluidModel   = "Dummy"
IntegratedLS = False

# ==============================================================================
#	Equation class giving the flux, jacobian and all tools required by schemes
# ==============================================================================
class Equations():
	"""
	Class furnishing all the methods that are required to define numerical
	schemes and evolve the solution according to the Linear Advection (translation)

	|

	.. rubric:: Fields

	.. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

	It contains  the following initialisation parameters (later available as well as attributes)

		- |bs| **FluidProp**   *(list of FluidModel)*  --  the list of fluid property instances (NbFluids)
		- |bs| **EOS**         *(list of EOS)*         --  the list of Equation of state instances (NbFluids)

	It further contains the following attributes, filled upon initialisation

		- |bs| **NbVariables**          *(integer)*          --   number of variables of the model
		- |bs| **VariablesNames**       *(list of strings)*  --   ordered name of the variables
		- |bs| **VariablesUnits**       *(list of strings)*  --   symbols of the units of the variables
		- |bs| **VariablesLatexNames**  *(list of strings)*  --   ordered name of the variables, latex encoding
		- |bs| **VariablesLatexUnits**  *(list of strings)*  --   symbols of the units of the variables, latex encoding
		- |bs| **XYLatexUnits**         *(list of strings)*  --   units of the x-y coordinates in latex encoding

	"""

	#### Automatic initialisation routine ####
	def __init__(self, FluidProp, EOs):
		"""
		|

		Args:
			FluidProp   (list of FluidModel):   the list of fluid property instances (NbFluids)
			EOS         (list of EOS):         the list of Equation of state instances (NbFluids)

		|

		.. rubric:: Methods

		"""

		# Defines the (conservative) variables number and names, in a latex typography
		self.NbVariables    = 1
		self.VariablesNames = ["h"]
		self.VariablesUnits = ["m"]

		self.VariablesLatexNames = [r'$h$']
		self.VariablesLatexUnits = [r'$m$']
		self.XYLatexUnits = [r'$m$', r'$m$']

		self.PrimaryVariablesLatexUnits = [r'$m$']
		self.PrimaryVariablesLatexNames = [r'$h$']
		self.PrimaryVariablesNames      = ["h"]

		# Register the shortcut of the fluid's properties
		self.FluidProp = FluidProp
		self.EOs       = EOs

	#### Retrieve the primary variables from the conservative ones ####
	def ConservativeToPrimary(self, Var, FluidIndex, *args):
		"""Converts the conservative variables to the primary ones

		Args:
			Var         (2D numpy array):         the variables of the problem (NbVars x NbGivenPoints)
			FluidIndex  (integer numpy array):    the fluid present at each given point (NbGivenPoints)

		Returns:
			Var (numpy array): (NbVars x NbGivenPoints) the corresponding primary variables

		.. note::

			- | *args is there only for compatibility reason at call time
		"""

		# ------- Constructing the primary variables ----------------------------------------------------
		# Computing the primary variables
		PrimVars = Var[:]

		# Returning the Result
		return(PrimVars)

	#### Retrieve the conservative variables from the primary ones ####
	def PrimaryToConservative(self, PrimVar, FluidIndex, *args):
		"""Converts the primary variables to the conservative ones

		Args:
			Var         (2D numpy array):         the primary variables of the problem (NbVars x NbGivenPoints)
			FluidIndex  (integer numpy array):    the fluid present at each given point (NbGivenPoints)

		Returns:
			Var (numpy array): (NbVars x NbGivenPoints) the corresponding conservative variables

		.. note::

			- | *args is there only for compatibility reason at call time
		"""

		# ------- Constructing the conservative variables ----------------------------------------------------
		# Computing the primary variables
		Vars = PrimVar[:]

		# Returning the Result
		return(Vars)

	#### Flux function of the equations set ####
	def Flux(self, Var, x, *args):
		"""Flux function of the LinearAdvection (translation case)

		.. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

		Args:
			Var  (float numpy array):   the value of the variables (NbVariables x NbGivenPoints)
			x    (float numpy array):   (generally optional, required for this problem), x-y coordinates of the given points (NbGivenPoints x 2)

		.. rubric:: Optional argument

		- |bs| **FluidIndex**  *(integer numpy array)*  --  the fluid present at each given point (NbGivenPoints)

		Returns:
			Flux (3D numpy array):   the obtained flux (spatialdim x NbVars x NbPoints)

		|

		.. note::

			- | This function is Vectorised
			- | Fluid index is the fluid index in the initially given list, not the fluid type
			- | FluidProp is the list of all the fluid properties for each fluid (given the list, not the type)
			- | *args is there only for compatibility reason at call time

		"""

		# --------- Computing the Advection flux directly in conservative variables  ------------------------
		# Giving the flux furnishing the roation
		Fx   =    Var[[0],:]         # Displacement in x direction
		Fy   =  0*Var[[0],:]         # Displacement in y direction
		Flux = np.array([Fx, Fy])

		# Returning the flux
		return(Flux)

	#### Function that extracts the propagation speed from the variables to interface for the LevelSet ####
	def GetUV(self, Var, x, *argv):
		"""Function giving back the x-y velocity at the considered point, given the variational
		values given for the advection.

		.. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

		Args:
			Var  (float numpy array):   the value of the variables (NbVariables x NbGivenPoints)
			x    (float numpy array):   (generally optional, required for this problem), x-y coordinates of the given points (NbGivenPoints x 2)

		.. rubric:: Optional argument

		- |bs| **FluidIndex**  *(integer numpy array)*  --  the fluid present at each given point (NbGivenPoints)

		Returns:
			UV   (float numpy array) -- the velocities values at the points (2 x NbGivenPoints)

		.. note::

			- | This function is Vectorised
			- | *args is there only for compatibility reason at call time

		"""

		# Computing the velocity values from the conservative Euleur equations
		UV = np.squeeze(self.Flux(Var, x))

		# Returning the value
		return(UV)

	#### Jacobian of the equations set ####
	def Jacobian(self, Var, x, *argv):
		"""Computes the Jacobian of the flux for the LinearAdvection (rotation case)

		.. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

		Args:
			Var  (float numpy array):   the value of the variables (NbVariables x NbGivenPoints)
			x    (float numpy array):   (generally optional, required for this problem), x-y coordinates of the given points (NbGivenPoints x 2)

		.. rubric:: Optional argument

		- |bs| **FluidIndex**  *(integer numpy array)*  --  the fluid present at each given point (NbGivenPoints)

		Returns:
			J    (3D numpy array):   J[:,:,i] gives the jacobian of the flux taking care of the dynamic of the ith spatial coordinate.

		|

		.. note::

			- | For each flux fi = (fi1,..., fin), the returned Jacobian reads:

			  | |bs| J[:,:] = [dfi1/dx1, ....., dfi1/dxn
			  | |bs| |bs| |bs|	|bs| |bs| |bs|	 ....
			  | |bs| |bs| |bs|	|bs| |bs| |bs|	df in/dx1, ....., dfin/dxn]
			- | *args is there only for compatibility reason at call time

		"""

		# ------ Initialising the Jacobian structure ---------------------------
		# Getting the number of furnished points
		NbPoints = np.shape(Var)[1]
		J = np.zeros((1, 1, 2, NbPoints))

		# ------ Computing the actual Jacobian ---------------------------------
		# Creating the Jacobian for the first coordinate
		J[0, 0, 0, :] = 1+0*x[:,0]

		# Creating the Jacobian for the second coordinate
		J[0, 0, 1, :] = 0*x[:,1]

		# Returning the Jacobian
		return(J)

	#### Spectral radius of the equations set ####
	def SpectralRadius(self, Var, FluidIndex, n, x, *argv):
		"""Computes the spectral radius associated to the flux.

		Args:
			Var         (2D numpy array):         the variables of the problem (NbVars x NbGivenPoints)
			FluidIndex  (integer numpy array):    the fluid present at each given point (NbGivenPoints)
			n           (2D numpy array):         the x-y values of the normal at each given point  (NbGivenPoints x 2)
			x           (2D numpy array):         (generally optional, required for this problem) the x-y locations at which the variables are given  (NbGivenPoints x 2)

		Returns:
			Lmb (numpy array): the spectral radius computed at each given point

		.. note::

			- | *args is there only for compatibility reason at call time
		"""

		# ------------------ Computations --------------------------------------
		# Retrieving the Jacobian
		J = self.Jacobian(Var, x)

		# Computing the spectral radius at each given point, shortcuted since here we are scalar
		Lmb = np.abs(np.sum(J[0,0,:,:]*n.T, axis = 0))

		# Returning the spectral radius at each given point
		return(Lmb)

	#### Eigen values ####
	def EigenValues(self, Var, FluidIndex, n, x, *args):
		"""Computes the eigenvalues associated to the flux.

		Args:
			Var         (2D numpy array):         the variables of the problem (NbVars x NbGivenPoints)
			FluidIndex  (integer numpy array):    the fluid present at each given point (NbGivenPoints)
			n           (2D numpy array):         the x-y values of the normal at each given point  (NbGivenPoints x 2)
			x           (2D numpy array):         (generally optional, required for this problem) the x-y locations at which the variables are given  (NbGivenPoints x 2)

		Returns:
			lbd (numpy array): (NbGivenPoints x NbEig) the four eigenvalues at each given point

		.. note::

			- | *args is there only for compatibility reason at call time
		"""

		# ---------- Ugly always working way ------------------------------

		# --- Locating and extracting the right fluid's properties for each location
		# List of fluids present in the list to be computed
		Fluids = list(set(FluidIndex))

		# Retrieving the Jacobian
		J = self.Jacobian(Var, x)

		# Computing the spectral radius at each given point, shortcuted since here we are scalar
		lbd = np.reshape(np.sum(J[0,0,:,:]*n.T, axis = 0), (1, 1, Var.shape[1]))

		# Returning the values
		return(lbd)

	#### Right eigen vectors ####
	def RightEigenVectors(self, Var, FluidIndex, n, x, *args):
		"""Computes the right eigenvectors associated to the eigenvalues.

		Args:
			Var         (2D numpy array):         the variables of the problem (NbVars x NbGivenPoints)
			FluidIndex  (integer numpy array):    the fluid present at each given point (NbGivenPoints)
			n           (2D numpy array):         the x-y values of the normal at each given point  (NbGivenPoints x 2)
			x           (2D numpy array):         (generally optional, required for this problem) the x-y locations at which the variables are given  (NbGivenPoints x 2)

		Returns:
			reg (numpy array): (NbVars x MbVars x NbGivenPoints) the matrix of eigenvectors for each given point

		.. note::

			- | *args is there only for compatibility reason at call time
		"""

		# ---------------- Right eigenvalues -------------------------------------
		# Creating right eigenvalues
		REV = np.zeros((Var.shape[0], Var.shape[0], Var.shape[1]))
		REV[:, :, :] = 1

		# Returning the values
		return(REV)

	#### Right eigen vectors ####
	def LeftEigenVectors(self, Var, FluidIndex, n, x, *args):
		"""Computes the left eigenvectors associated to the eigenvalues.

		Args:
			Var         (2D numpy array):         the variables of the problem (NbVars x NbGivenPoints)
			FluidIndex  (integer numpy array):    the fluid present at each given point (NbGivenPoints)
			n           (2D numpy array):         the x-y values of the normal at each given point  (NbGivenPoints x 2)
			x           (2D numpy array):         (generally optional, required for this problem) the x-y locations at which the variables are given  (NbGivenPoints x 2)

		Returns:
			reg (numpy array): (NbVars x MbVars x NbGivenPoints) the matrix of eigenvectors for each given point

		.. note::

			- | *args is there only for compatibility reason at call time
		"""

		# ---------------- Creating left eigenvalues ---------------------------
		# Creating left eigenvalues
		LEV = np.zeros((Var.shape[0], Var.shape[0], Var.shape[1]))
		LEV[:, :, :] = 1

		# Returning the values
		return(LEV)



# ==============================================================================
#   Class containing all the methods for defining various Equation of states
# ==============================================================================
#### Class containing the methods giving back the pressure ####
class EOS():
	"""Class furnishing the methods giving the Equation of State that can be
	used in combination with the :code:`SimpleFluid` definition.
	Upon creation, fills the field :code:`EOS` with the function callback
	corresponding to the wished initialisation function and the field :code:`EOSName` with
	the associated label. The implemented EoS are linked as follows.
	See their respective documentation for more information.

		1. | Dummy

	|

	.. Note::

		After having been initialised the instance, the EOS is accessible from the field EOS within this class.

	.. warning::

		For advection, this class is void, just here for compatibility properties in the code

	|

	"""

	#### Automatic initialisation routine (mapper) ####
	def __init__(self, Id):
		"""
		Args:
			Id (integer):   the index corresponding to the equation of fluid the user wants when considering the governing equations given in this module and the associated fluid.

		|

		.. rubric:: Methods
		"""

		# Mapping towards the right function
		if   Id==1:
			self.EoS     = self.Dummy
			self.EoSName = "Dummy"

		else: raise NotImplementedError("Error in the selection of the Equation of State\n\
		                                 Unknown index. Check your Problem definition. Abortion.")

	#### Dummy EOS #####
	def Dummy(self, Var, FluidProp, Type):
		"""Dummy EOS returning the identity for the sake of the code compatibility.

		Args:
			Type      (string):             (dummy here)
			Var       (float numpy array):  the value of the variables (NbVariables x NbGivenPoints)
			FluidProp (FluidModel list):    (dummy here) the list of the fluid properties associated to each given Point

		Returns:
			val: the variable retrieved from the EoS (here identity as dummy function)
		"""

		# Void return
		val =  Var

		# Returning the asked value
		return(val)


# ==============================================================================
#	Class containing all the predefined suitable initial conditions
# ==============================================================================
class InitialConditions():
	"""Class furnishing the solution initialisation routines that are suitable
	to study the the Linear advection translation Equations, defined for working on a subdomain.
	Access the routine computing the initial conditions through the field :code:`IC`, filled upon creation
	with the function callback corresponding to the index of the wished initialisation function.
	The implemented initialisation method are linked as follows.
	See their respective documentation for more information.

		0. | ConstantState
		1. | Bump
		2. | BumpEdge
		3. | ObliquePlane

	|

	"""

	#### Automatic initialisation routine (mapper) ####
	def __init__(self, Id, *params):
		"""
		Args:
			Id (integer):                the index corresponding to the initialisation method the user wants when considering the governing equations given in this module.
			params (list of arguments):  the (fixed) arguments to pass to the selected function

		|

		.. rubric:: Methods

        """

		# Registering the parameters
		self.params = params

		# Mapping the initialisation routines
		if   Id == 0: self.IC = self.ConstantState
		elif Id == 1: self.IC = self.Bump
		elif Id == 2: self.IC = self.BumpEdge
		elif Id == 3: self.IC = self.ObliquePlane

		else: raise NotImplementedError("Error in the selection of the Initial conditions\
		                                 Unknown index. Check your Problem definition. Abortion.")

	#### Initialise the solution to a constant state ####
	def ConstantState(self, PointsID, Mesh, EOS = [], FluidProp = [], subdomain = [], *args):
		"""

		Initialising the solution to a constant state on the given points, all belonging to a same subdomain.

		Args:
			PointsID   (integer array-like):     the index of the points to consider
			Mesh       (MeshStructure):          the considered mesh
			EOS        (function callback):      (optional), the equation of state given by the model of the fluid that is present at the given points
			FluidProp  (FluidSpecs): (optional,  not used here) the the properties of the fluid present where the given points are
			Subdomain  (shapely multipolyon):    (optional, not used here) the shapely polygon to which the given points belong

		Returns:
			Init       (float numpy array):      The initialised values at the considered points (NbVariables x NbGivenPoints)

		.. note::

			- *args is here only for compatibility on call
			- There is usually one initialisation routine per subdomain, and the PointsID are not necessarily contigous, be careful when assigning the values back in the regular solution.
		"""

		# Initialising the point-values
		Init = np.ones((1, len(PointsID)))

		# Defining the raw initilisation variable
		Init[0,:] = float(self.params[0])

		# Returning the computed values
		return(Init)

	#### Initialise the solution to an oblique plane ####
	def ObliquePlane(self, PointsID, Mesh, EOS, FluidProp, subdomain, *args):
		"""
		Initialising the solution to an oblique plane centered at the x position center of the subdomain
		Args:
			PointsID   (integer array-like):     the index of the points to consider
			Mesh       (MeshStructure):          the considered mesh
			EOS        (function callback):      (optional), the equation of state given by the model of the fluid that is present at the given points
			FluidProp  (FluidSpecs):             (optional,  not used here) the the properties of the fluid present where the given points are
			Subdomain  (shapely multipolyon):    (optional, not used here) the shapely polygon to which the given points belong

		Returns:
			Init       (float numpy array):      The initialised values at the considered points (NbVariables x NbGivenPoints)

		.. note::

			- | *args is here only for compatibility on call
			- | There is usually one initialisation routine per subdomain, and the PointsID are not necessarily contigous, be careful when assigning the values back in the regular solution.
			- | For running smothly the inner circle diameter of the given subdomain should be at least 2
		"""

		# Initialising the point-values
		Init = np.zeros((1, len(PointsID)))

		# Retrieving the xy values of the points and the geometrical informations from the subdomain
		xy = Mesh.DofsXY[PointsID,:]
		yc = np.max([np.max(np.array(subdo.exterior.coords.xy).T[:,1]) for subdo in subdomain])-1

		# Defining the space-dependent quantities
		p = (xy[:,1]-yc)

		# Affect the value to the initialisation variable where it should be
		Init[0,:] = p

		# Returning the computed values
		return(Init)

	#### Initialise the solution to a Bump in the centre y of the subdomain, next to the max of the x span ####
	def BumpEdge(self, PointsID, Mesh, EOS = [], FluidProp = [], subdomain = [], *args):
		"""
		Initialising the solution to a Bump centred next to the righter boundary of the subdmain. (The given points should all belonging to a same subdomain).

		Args:
			PointsID   (integer array-like):     the index of the points to consider
			Mesh       (MeshStructure):          the considered mesh
			EOS        (function callback):      (optional), the equation of state given by the model of the fluid that is present at the given points
			FluidProp  (FluidSpecs):             (optional,  not used here) the the properties of the fluid present where the given points are
			Subdomain  (shapely multipolyon):    (optional, not used here) the shapely polygon to which the given points belong

		Returns:
			Init       (float numpy array):      The initialised values at the considered points (NbVariables x NbGivenPoints)

		.. note::

			- | *args is here only for compatibility on call
			- | There is usually one initialisation routine per subdomain, and the PointsID are not necessarily contigous, be careful when assigning the values back in the regular solution.
			- | For running smothly the inner circle diameter of the given subdomain should be at least 2
		"""

		# Initialising the point-values
		Init = np.zeros((1, len(PointsID)))

		# Retrieving the xy values of the points and the geometrical informations from the subdomain
		xy = Mesh.DofsXY[PointsID,:]
		xc = np.max([np.max(np.array(subdo.exterior.coords.xy).T[:,0]) for subdo in subdomain])-1
		yc = subdomain.centroid.y

		# Defining the space-dependent quantities
		r2 = ((xy[:,0]-xc)**2+(xy[:,1]-yc)**2)

		# Check the hard way with a large buffer otherwise exp*(r2<1) renders Nan
		ind = np.where(r2<1-1e-2)[0]

		# Affect the value to the initialisation variable where it should be
		Init[0,ind] = 3*np.exp(-1/(1-r2[ind]))

		# Returning the computed values
		return(Init)

	#### Initialise the solution to a Bump centred in the middle of the subdmain ####
	def Bump(self, PointsID, Mesh, EOS = [], FluidProp = [], subdomain = [], *args):
		"""Initialising the solution to a bump centred at the center of mass
		of the subdmain. (The given points should all belonging to a same subdomain).

		Args:
			PointsID   (integer array-like):     the index of the points to consider
			Mesh       (MeshStructure):          the considered mesh
			EOS        (function callback):      (optional), the equation of state given by the model of the fluid that is present at the given points
			FluidProp  (FluidSpecs): (optional,  not used here) the the properties of the fluid present where the given points are
			Subdomain  (shapely multipolyon):    (optional, not used here) the shapely polygon to which the given points belong

		Returns:
			Init       (float numpy array):      The initialised values at the considered points (NbVariables x NbGivenPoints)

		.. note::

			- | *args is here only for compatibility on call
			- | There is usually one initialisation routine per subdomain, and the PointsID are not necessarily contigous, be careful when assigning the values back in the regular solution.
			- | For running smothly the inner circle diameter of the given subdomain should be at least 2
		"""

		# Initialising the point-values
		Init = np.zeros((1, len(PointsID)))

		# Retrieving the xy values of the points and the geometrical informations from the subdomain
		xy = Mesh.DofsXY[PointsID,:]
		xc = subdomain.centroid.x
		yc = subdomain.centroid.y

		# Defining the space-dependent quantities
		r2 = ((xy[:,0]-xc)**2+(xy[:,1]-yc)**2)

		# Check the hard way with a large buffer otherwise exp*(r2<1) renders Nan
		ind = np.where(r2<1-1e-2)[0]

		# Affect the value to the initialisation variable where it should be
		Init[0,ind] = 3*np.exp(-1/(1-r2[ind]))

		# Returning the computed values
		return(Init)
