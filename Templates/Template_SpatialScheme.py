#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

# Template to copy in _Solver/SpatialSchemes/
# Add the scheme from its module name under a new index in the file Mappers.py

# ==============================================================================
#	Preliminary imports
# ==============================================================================
import numpy as np
import copy

# Import custom modules
import BarycentricBasisFunctions         as BF
import BarycentricTools                  as BT

# ==============================================================================
#	Parameter (and reader) class of scheme properties and runtime informations
# ==============================================================================
# Name of the module to be externally accessible
SchemeName = "The scheme name"

# Mapping to the mesh that should be loaded
class AssociatedMeshType():
	""" .. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

	Class that only gives out the mesh type that should be loaded when tackling the problem
	with this scheme, given the wished variational order. Access the associated mesh properties
	through the inner variables

		- |bs| **MeshType**  *(string)*   -- the mesh type according to fenics's classification
		- |bs| **MeshOrder** *(integer)*  -- the mesh order

	|

	.. Note::

		This is only a product-type class: no method is available

	|
	"""

	#### Automatic initialisation routine ####
	def __init__(self, SchemeParams = [1]):
		""" .. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE
		Args:
			SchemeParams (string list, optional):   the parameters of the spatial scheme wished by the user

        """

		# Extracting the schemes properties that are required to determine the Mesh
		VariationalOrder = int(SchemeParams[0])

		# Selecting the corresponding mesh type
		self.MeshType  = "The mesh type in a Fenics format"
		self.MeshOrder = VariationalOrder
		self.ControlVolumes = "The control volume type, either 'Dual' or 'Primal'"


# ==============================================================================
#	Class furnishing all the scheme tools
# ==============================================================================
class Scheme():
	""".. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE


	Class furnishing all the schemes tools and registers the scheme's wished
	parameters. The main iteration is accessible with the method "Iteration".

	Repeats the instance's initialisation arguments as fields with identical names
	within the structure.

	|

	"""

	#### Automatic initialisation routine ####
	def __init__(self, Problem, Mesh, QuadratureRules, *SchemeParams):
		"""
		Args:
			Problem (Problem):       the considered problem
			Mesh    (MeshStructure): the considered mesh instance
			QuadratureRules   (Quadratures):   the quadratures to use within the scheme.
			SchemeParams      (string list):   the scheme's parameters as wished by the user

		|

		.. note::

			If the scheme do not require any quadrature rule, the parameters and
			related lines below may be removed safely.

		.. rubric:: Methods

		"""

		# Storing internally the associated instances
		self.Problem = Problem
		self.Mesh    = Mesh
		self.QuadratureRules = QuadratureRules

		# Extracting the parameters from the user input list
		if len(SchemeParams)==1: self.Order = int(SchemeParams[0])
		else: raise ValueError("Wrong number of user input argument when definining the fluid selector. Check you problem definition.")

		# Precomputing the basis function values at quadrature points for fastening the run time
		self.PreEvaluateQuadratureQuantities()

	#### Emulates a flux computation as done in the iteration routine ####
	def ComputeFlux(self, FlagPoints, U, LSValues):
		""" Emulates a flux computation as done in the iteration routine in order to be used in the DeC subtimestep evaluation.

		Args:
			FlagPoints   (function callback):  mapper to the routine of the FluidSpotter FlagPoints method to flag the Fluid on given points according the given LSValues at Dofs
			U            (numpy float array):  buffer containing the current state values at each dof (NbVars   x NbDofs)
			LSValues     (numpy float array):  buffer containing the current FluidSpotters values at each dof(NbFluids x NbDofs)

		Returns:
			fluxes     (numpy float (multiD)array): fluxes in the format required by the Iteration routine
		|

		.. note::
			If the scheme should be used with a filter, make sure that the last row of fluxes corresponds to the format
			wished by the filters (fluxes[lastrowoffluxes, elementnumber, 0, :, :,1:NbDofs] = the fluxes computed at the dofs (dim x NbVars x NbDofs))

		"""


		# DEFINE HERE THE FLUX TO BE A-PRIORI COMPUTED (ESP. FOR THE COMPATIBILITY WITH THE DEC ITERATION)
		# AND PASSED TO THE MAIN ITERATION


		# Returning the full array
		return(Flux)

	#### Main routine, definining the scheme ####
	def Iteration(self, FlagPoints, Solution, fluxes, i, du=0, dt=1):
		"""Your scheme description

		Args:
			FlagPoints  (function callback):    the handle of a function flagging the points to the relevant fluid
			Solution    (solution structure):   structure containing the current solution's values to iterate
			fluxes      (numpy (multiD)array):  pre-computed fluxes at the points of interest. For this scheme, access with fluxes[element, face, coordinate(fx or fy), variable, pointindex]
			i           (integer):              the index of the considered element within which the partial residuals will be computed
			du          (float numpy array):    (optional) when using DeC, the difference in the time iteration
			dt          (float):                (optional) when using DeC, the full time step

		Returns:
			Resu        (float numpy array):    the computed residuals (NbVars x NbDofs)
		"""

		# DEFINE HERE THE MAIN SPATIAL ITERATION SCHEME
		# USE THE GIVEN PARAMETER FLUX FOR COMPATIBILITY WITH DEC REASONS

		# Returning the partial residuals
		return(Resu)

	#### Routine that maps the values from the Dofs to the Physical vertices of the mesh ####
	def ReconstructSolutionAtVertices(self, Solution):
		""" Routine that maps the values from the Dofs to the Physical vertices of the mesh
		of the FluidSpotter, solution and flags.

		Args:
			Solution  (Solution):  the currently being computed solution

		Returns:
			None: fills direclty the RSol and RFluidFlag values in the data structure

		.. Note::

			This is only for later plotting purposes.

		----------------------------------------------------------------------------------- """

		# -------------- Initialising the reconstructed values ----------------------------
		Solution.RFluidFlag  = np.zeros(self.Mesh.NbMeshPoints)
		Solution.RLSValues  = np.zeros((np.shape(Solution.LSValues)[0],  self.Mesh.NbMeshPoints))
		Solution.RSol           = np.zeros((np.shape(Solution.Sol)[0],       self.Mesh.NbMeshPoints))

		# ------------- Reconstructing the values -----------------------------------------
		# Extract the cartesian coordinates of the Dofs
		xyDofs = self.Mesh.DofsXY[:,:]

		# Spanning all the mesh points where to fill the values
		for i in range(self.Mesh.NbMeshPoints):

			# RECONSTRUCT HERE YOUR SOLUTION AT THE SOLUTION VERTICES

			# Fill the reconstructed solution upon the found index
			Solution.RLSValues[:,i] = RS
			Solution.RFluidFlag[i]  = FF
			Solution.RSol[:,i]      = RSO


	# **************************************************************
	# *  Definition of the scheme basis function                   *
	# **************************************************************

	# Precomputing and storing the values of the basis functions at the quadrature points of each element
	def PreEvaluateQuadratureQuantities(self):
		"""
		Precomputing and storing the values of the basis functions at the quadrature points of each element

		Input:
			None:  Considers the given parameters of the class directly

		Returns:
			None: Creates the fields InnerBFValues and FaceWiseBFValues in the scheme structure, containing
			      the values of each basis function at each quadrature point for each element and face.
		.. note::

			Depending on your scheme, this may not be required. In this case, remove the line 70.
		"""

		# COMPUTE HERE THE VALUES OF THE QUADRATURE POINTS AND BASIS FUNCTIONS THERE THAT WILL NOT BE TIME-DEPENDENT
		# FILL THE FIELDS:
		self.InnerQBFValues    = [[]]*self.Mesh.NbInnerElements
		self.InnerQWeigths     = [[]]*self.Mesh.NbInnerElements
		self.InnerQGFValues    = [[]]*self.Mesh.NbInnerElements
		self.InnerQPoints      = [[]]*self.Mesh.NbInnerElements
		self.FaceWiseQWeigths  = [[]]*self.Mesh.NbInnerElements
		self.FaceWiseQPoints   = [[]]*self.Mesh.NbInnerElements
		self.FaceWiseQBFValues = [[]]*self.Mesh.NbInnerElements
