#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

# Template to copy in _Solver/Redistancers/
# Add the scheme from its module name under a new index in the file Mappers.py

# ==============================================================================
#	Preliminary imports
# ==============================================================================
# Pythonic import
import types
import copy

# Classical math librairies import
import numpy as np

# Import custom libraries if needed (See if any of LocalLib can help you)
import LocalLib.Solve_WENO         as WE

# ==============================================================================================
#	Defining the class containing the properties to apply and the core reinitialisation routine
# ==============================================================================================
class Redistancing():
	"""
	Class furnishing the routines to define a redistanciation routine.
	The main redistancing is accessible with the method "Redistancing".
	"""

	### Initialisation of the instance by the parameters to consider in the reinitialisation process #####
	def __init__(self,  Your=0, Arguments=0, Quadratures):
		"""
		Args:
			Your           (float):   Optional, default value = 0.  The very first argument you need
			Arguments      (float):   Optional, default value = 0.  The very second argument you need
			Quadrature     (Quadrature instance):  A quadrature instance may also be given
		It repeats the arguments as fields in the structure.
		"""

		try:
			self.Your          = np.float(Your)
			self.Argument      = np.bool(Argument)

			# ------- Compatibility check with the user inputs and Amendements interface parsing ------------------------
			# Test the type of input that are required to work and inform the user accordingly
			# (may be removed if no quadrature needed)
			if type(Quadratures) == str: raise (ValueError("The first argument of the streamline filtering should be a quadrature rule. Abortion."))
			else: self.QuadratureRules = Quadratures

		except:
			raise(ValueError("Wrong argument type or number given to the selected redistancing routine. Please check your input settings. Abortion."))


	# ********************************************************************************
	# *  Defining the functions giving the PDE to solve as a redistancing operator   *
	# ********************************************************************************
	# You can change it if you like another redistancing equation
	# --------------------------------------------------------------------------------

	# Defining the flux of the Eikonal equation
	def H (self, dx, dy): return(np.sqrt(dx**2+dy**2))

	# Defining its partial derivatives with respect to phix and phiy
	def Hx(self, dx, dy): return(dx*(1/np.sqrt(self.eps+dx**2+dy**2)))
	def Hy(self, dx, dy): return(dy*(1/np.sqrt(self.eps+dx**2+dy**2)))

	# Defining the primary cost functions entering in the optimisation problem and in the
	# Bregman splitting, depending on J
	def J(self, Phi, x):  return(np.abs(Phi(x)))


	# ********************************************************************************
	# *  Defining the internal routines                                              *
	# ********************************************************************************
	#### Routine creating the interpolator based on the given Phi #####
	def InterpolatePhi(self, Points, Phi, *args):
		"""
		Interpolator that considers either a function Phi or a set of Points and point-values Phi and returns an interpolator function.
		It also returns the Gradient interpolator of the function contructed from a gradient approximation at the given Points.

		Args:
			Points (float NbPoints x2):                   The list of points where to consider the function for its interpolation
			Phi    (Function callback or NbPoints float): Level set to reinitialise, either given as a function or a list of point values associated to points
			args   (Two function callback, optional):     The callback function defining the partial derivatives in x and y, in that order

		Returns:
			InterpPhi   (function callback):   The interpolator of the function Phi
			InterpPhiDx (function callback):   The interpolator of the derivative in x of Phi
			InterpPhiDy (function callback):   The interpolator of the derivative in y of Phi

		.. note::

			If function callbacks are given, they should be 2d->IR, taking the xy-values as a column list (NbPoints x 2) and return a 1d numpy vector
		"""

		# If NEEDED, DEFINE YOUR INTERPOLATION FUNCTION THERE

		# Returning the quantities of interest
		return(InterpPhi, InterpPhiDx, InterpPhiDy)


	# ********************************************************************************
	# *  Defining the main redistancing routine aimed to be called externally        *
	# ********************************************************************************
	def Redistance(self, Points, Phi, *args, **kwargs):
		"""Implementation of the main iteration strategy of the Bregman split for the redistancing
		problem.

		Args:
			Points (float 2D numpy array):      The points on which reinitialise the level-set on (NbPoints x 2)
			Phi    (function callback):         The (initial) level set function to redistance (should return a scalar value),
			                                    given either as a float list associated to the points or as a function callback
			args   (Two function callback):     (Optional) The callback function defining the partial derivatives in x and y, in that order
			kwargs (keyword argument):          The only keyword argument available is "ConstructionPoints", only active when Phi is given as
			                                    a float list. It corresponds to where the given point values Phi have been computed. If the
			                                    argument is not passed when Phi is a float list, the function assumes that Phi has been constructed
			                                    on Points. Note that the Points should be contained within the convex hull of ConstructionPoints.

		Returns:
			RPhi  (float numpy array):  The value of the redistanced level set function at the dofs (NbDofs)
		"""

		# -------------------- Initialise the problem -------------------------------------------------------
		# Getting the information of the points to redistance for
		NbPoints = np.shape(Points)[0]

		# Scanning the keyword argument to see if Phi has been constructed on different points than those asked
		# DO NOT REMOVE THAT PLEASE
		InterpPoints = Points
		for args,val in kwargs.items():
			if args == "ConstructionPoints": InterpPoints = val

		# Getting the linearly interpolated Level Set function and its gradient
		# (note: if Phi is a Lambda function, InterpPhi=Phi and the gradient is
		#        interpolated from the points where Phi will be redistanced)
		# CAN BE REMOVED IF NOT NEEDED
		InterpPhi, InterpPhiDx, InterpPhiDy = self.InterpolatePhi(InterpPoints, Phi, *args)

		# DEFINE YOUR REDISTANCING ALGORITHM HERE

		# Return the redistanced level set values at the Dofs
		return(RPhi)
