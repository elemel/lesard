#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

# File defining any custom boundary condition, located in :code:`BoundaryConditions/TheConditionName`
# Should you need a template with a switch to bub functions, please copy paste the :code:`Inflow.py` and adapt it to your case.

def TheConditionName(Problem, Mesh, Solution, Residuals, FluidId, Bd, BdIndex, *params):
	"""
	Routine that does nothing, just here for you to modify at wish

	Args:
		Problem    (Problem):            the considered problem
		Mesh       (MeshStructure):      the considered mesh
		Solution   (Solution):           the considered solution (as of the previous subtimestep (as used in the temporal scheme))
		Residuals  (2D numpy array):     the value of the currently computed residuals(NbVars x NbDofs)
		FluidId    (integer):            the index of the considered fluid (warning: NOT its value in mappers)
		Bd         (integer):            the tag of the boundary the considered points are lying on
		BdIndex    (integer list):       the list of indices of the degrees of freedom to treat
		Params     (parameters):         (optional) as many parameters as one requires


	Returns:
		res  (2D float numpy array):  the updated residuals in the same format as the given residuals
	"""

	# TREAT THE BOUNDARY THE WAY YOU LIKE THERE
	Value = np.zeros(Residuals[:,BdIndex].size)

	return(Value)
