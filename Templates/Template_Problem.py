#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

# Template to create a new problem, to copy-paste in the PredefinedTests/PredefinedProblems folder

import numpy as np

class ProblemData():
	def __init__(self):

		# ******************************************************************************
		#                          Meshing
		#*******************************************************************************
		# Define a name for the problem
		self.ProblemID = "TheProblemName"

		# Select the geometry associated to the problem
		# (the root name of the target mesh stored in the PredefinedTests/PredefinedMeshes, without the resolution)
		self.MeshName  = "AssociatedMesh"

		# ******************************************************************************
		#                          Equation
		#*******************************************************************************
		# Index of the governing equation according to the Mappers.py file
		self.GoverningEquationsIndex = 0

		# Equation of state to consider for each fluid
		# (the length of the list matches the number of fluids)
		self.EOSEquationsIndex = [0,0,0]

		# *******************************************************************************
		#                          Fluids properties
		#********************************************************************************
		# Select the indices speciying the fluids properties living on each subdomain
		# according to the fluid's list in the Mappers.py file. Several fluids types
		# can be safely identical.
		self.FluidIndex = [0, 0, 0]

		# Define the subdomains on which the second and third fluids will live
		# (see the documentation of the initial subdomains definition)
		self.FluidInitialSubdomain = [0,0]

		# Index of the initial conditions to apply on each subdomain
		# (the index is associated to the Governing equation. See the doc of the equation
		# targetted by the GoverningEquationsIndex)
		self.InitialConditions = [(0,),(0,),(0,)]

		# Determine the boundary conditions
		# Row:     BoundaryTag as defined in the mesh
		# Column:  Index of the defined fluid
		# Content: Index of the boundary condition to apply if the considered fluid touches the given boundary segment
		self.BoundaryConditions = np.array(\
								  [[(0,), (0,), (0,)],\
								   [(0,), (0,), (0,)],\
								   [(0,), (0,), (0,)],\
								   [(0,), (0,), (0,)],\
								   [(0,), (0,), (0,)],\
								   [(0,), (0,), (0,)]])



# ==============================================================================
#                            Problem Solution
# ==============================================================================
class ProblemSolution():
	"""
		Simple class containing the tools and exact solution associated to the problem "TheProblemName".

		.. note::

			- THIS SOLUTION HAS NOT BEEN IMPLEMENTED YET

	"""

	def __init(self):
		"""
			Args:
				None
		"""
		self.ProblemID = "TheProblemName"
		self.MeshName  = "AssociatedMesh"

	#### Solution routine that will be called externally #####
	def Solution(self, xy, t):
		"""
		Function defining the exact solution associated to the problem "Advection_Translation_Bump_2Fluids".

		Args:
			xy  (2D numpy array, float)   The points on which to evaluate the exact solution (NbPoints x 2)
			t   (float)                   The time point where to consider the solution
		"""

		# ------------------- Initialisation -----------------------------------
		raise(NotImplementedError("Error: The exact solution to this problem has not be defined yet or do not exist. Abortion."))
