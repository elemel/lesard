#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

# Template to copy in _Solver/TemporalSchemes
# Adapt the file mapper to add the mapping to the routine "TheRoutineName"

# ==============================================================================
#	Preliminary imports
# ==============================================================================

# Python imports
import copy

# Custom libraries import
import _Solver.Solver_Spatial as SPL


# ==============================================================================
#	Possibly companion functions
# ==============================================================================


# ==============================================================================
#	Actual TimeStepping routine
# ==============================================================================

def TheRoutineName(schemeSC, schemeFS, dt, Mesh, Sol, *args):
	""" Provides a full time iteration according to the DeC method

	.. rubric:: Functional inputs

	|bs| -- **schemeSP** *(scheme module list)* --  handler of the spatial scheme module, should be vectorised for using on several (spatial) points simultaneously followed (if any) by the list of its amendments (limiter, jumps etc)

	|bs| -- **schemeFS** *(scheme module list)* --  handler of the FluidSpotter scheme module, should be vectorised for using on several (spatial) points simultaneously followed (if any) by the list of its amendments (limiter, jumps etc)

	.. rubric:: Time-dependent inputs

	|bs| -- **dt**  *(float)* --                 time span on which to evolve the solution

	|bs| -- **Sol** *(Solution structure)* --    the initial condition, as a solution structure

	.. rubric:: Input parameters:

	|bs| -- **args**     *(list of required parameters)* --              the number of subtimesteps

	.. rubric:: Returns

	|bs| -- *Tspan (float)*         --   the full time step interval

	|bs| -- *U (float numpy array)* --   the updated solution (NbVars x (NbGivenPoints))

	"""

	# --------- Initilising the variables --------------------------------------
	# Initialising the variables of interest and buffering partially the quantities
	# of interest in the solution (either the Solution itself or the level set only)
	SolBuff = copy.deepcopy(Sol)
	U  = Sol.Sol
	LS = Sol.LSValues

	# --------- Process the time iteration --------------------------------------

	# DEFINE HERE YOUR TEMPORAL SCHEME

	# Call the spatial scheme with a call analogous to
	# SPL.RD(Theta, m, M, dt, schemeSC, schemeFS, Mesh, SolBuff, up, lp)

	# Returning the full timestep
	return([U_updated, LS_updated])
