#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

# Template to copy in _Solver/SpatialModifiers/ under the name Limiter_LimiterName.py if
# it is a limiter, Streamline_StreamlineName.py if it is a streamine dissipator.
# Add the modifier from its module name under a new index in the file Mappers.py

# ==============================================================================
#	Preliminary imports
# ==============================================================================
import numpy as np
import copy

# Import custom modules (may not be reuquired for the desired implementation)
import BarycentricBasisFunctions            as BF
import BarycentricTools                     as BT

# ==============================================================================
#	Parameter (and reader) class of scheme properties and runtime informations
# ==============================================================================
# Name of the module to be externally accessible
LimiterName = "The mollifier's name"

# ==============================================================================
#	Class furnishing all the limiter tools
# ==============================================================================
class Mollifier():
	""" .. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

	Class furnishing all the schemes tools and registers the scheme's wished
	parameters. The main iteration is accessible with the method "Mollify".

	.. rubric:: Fields

	All the fields given as arguments at the instance's creation are repeated within
	the structure with the same names (see the parameters below).

	|

	"""

	#### Automatic initialisation routine ####
	def __init__(self, Problem, Mesh, QuadratureRules, *Params):
		""" .. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE
		Args:
			Problem (Problem):       the considered problem
			Mesh    (MeshStructure): the considered mesh instance
			QuadratureRules  (Quadratures):   the quadrature instance with which the scheme will be performed (madatory paramter)
			Params           (string list):   the limiters's parameters as wished by the user (useless here)

		|

		.. note::

			The quadrature rules may not be required and can be removed from the template safely.

		.. rubric:: Methods
		"""

		# ------- Compatibility check with the user inputs and Amendements interface parsing ------------------------
		# Test the type of input that are required to work and inform the user accordingly
		# (may be removed if no quadrature needed)
		if type(QuadratureRules) == str: raise (ValueError("The first argument of the streamline filtering should be a quadrature rule. Abortion."))
		else: self.QuadratureRules = QuadratureRules

		# ------- Setting up the parameters ------------------------------------------------------------------------
		# Setting the name of the class to be accessible from outside
		self.ModifierName = "The name"

		# Setting the mesh and parameters for an internal access
		self.Problem = Problem
		self.Params  = Params
		self.Mesh    = Mesh

		# Precomputing the basis function values at quadrature points for fastening the run time
		self.PreEvaluateQuadratureQuantities()	# May be removed if no quadrature needed


	#### Main routine, definining the limiting mollifier ####
	def Mollify(self, SolBuff, resu, fluxes, i, du=0, dt=1):
		""" .. |bs|   unicode:: U+00A0 .. NO-BREAK SPACE

		Limting routine according to the Psi rule

		Args:
			Solution   (solution structure):   structure containing the current solution's values to iterate
			resu       (float numpy array):    previoulsy computed residuals that have to be modified (NbVars x NbElementDofs)
			fluxes     (numpy (multiD)array):  pre-computed fluxes at the points of interest. For this scheme, access with fluxes[element, face, coordinate(fx or fy), variable, pointindex]
			i          (integer):              the index of the considered element within which the partial residuals will be computed
			du         (float numpy array):    (optional) when using DeC, the difference in the time iteration
			dt         (float):                (optional) when using DeC, the full time step

		Returns:
			Lresu      (float numpy array):    the limited residuals (NbVars x NbElementDofs)

		---------------------------------------------------------------------"""

		# -------- Initialisation ------------------------------------------------
		# Getting the number dofs contaned in the considered element
		NbVars, NbDofs = np.shape(resu)

		# Initialising the mollification vector (residuals)
		Lresu = np.zeros(np.shape(resu))

		# MODIFY HERE YOUR RESIDUALS

		# Returning the partial residuals
		return(Lresu)

	# **************************************************************
	# *  Definition of the Mollifier spotter's scheme basis function   *
	# **************************************************************

	# Precomputing and storing the values of the basis functions at the quadrature points of each element
	def PreEvaluateQuadratureQuantities(self):
		"""
		Precomputing and storing the values of the basis functions at the quadrature points of each element

		Input:
			None:  Considers the given parameters of the class directly

		Returns:
			None: Creates the fields InnerBFValues and FaceWiseBFValues in the scheme structure, containing
			      the values of each basis function at each quadrature point for each element and face.
		.. note::

			Depending on your scheme, this may not be required. In this case, remove the line 70.
		"""

		# COMPUTE HERE THE VALUES OF THE QUADRATURE POINTS AND BASIS FUNCTIONS THERE THAT WILL NOT BE TIME-DEPENDENT
		# FILL THE FIELDS:
		self.InnerQBFValues    = [[]]*self.Mesh.NbInnerElements
		self.InnerQWeigths     = [[]]*self.Mesh.NbInnerElements
		self.InnerQGFValues    = [[]]*self.Mesh.NbInnerElements
		self.InnerQPoints      = [[]]*self.Mesh.NbInnerElements
		self.FaceWiseQWeigths  = [[]]*self.Mesh.NbInnerElements
		self.FaceWiseQPoints   = [[]]*self.Mesh.NbInnerElements
		self.FaceWiseQBFValues = [[]]*self.Mesh.NbInnerElements
