#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

"""

This file can be located either in :code:`_SourceCode` or in LSRD folder,
and specifies where to find the libraries containing the user-defined problems.

Edit it directly upon your local configuration. It has vocation to allow flexibility in where
your meshfiles, settings and problems are stored, and where you want to export the results to.
Just modify the following lines at your ease.

.. code::

    MeshPath     = os.path.join(Dir, "Your wished location")
    SettingPath  = os.path.join(Dir, "Your wished location")
    ProblemPath  = os.path.join(Dir, "Your wished location")
    SolutionPath = os.path.join(Dir, "Your wished location")

"""

# System import
import os
Dir = os.path.dirname(__file__)

# Shortuct for the Path prefix that maps to the user's storage location, relatively to
# this file's storage location
MeshPath     = os.path.join(Dir, "PredefinedTests", "PredefinedMeshes")
SettingPath  = os.path.join(Dir, "PredefinedTests", "PredefinedSettings")
ProblemPath  = os.path.join(Dir, "PredefinedTests", "PredefinedProblems")
SolutionPath = os.path.join(Dir, "Solutions")
