.. _ProblemDefinition:

LESARD: Problem Definition
========================

-------------------------------------------------------------


|
|

.. rubric:: Problem's specifications

A problem that can be tackled with this code is determined by:

    - *A problem ID*
    - *A computational mesh type*
    - *The number of fluids, their nature (i.e. model) and their properties*
    - *The spatial distribution of the initial subdomains on which the fluid live*
    - *A governing equations that will drive all the fluids present within the computational domain*
    - *An equation of state for each fluid, depending on its governing equation*
    - *The initial conditions*
    - *The boundary conditions*

|
|

.. rubric:: Problem definition

Defining a problem consist then to explicit those specification in a problem file :code:`TheProblemID.py` that
should be located under the :code:`PredefinedTests/PredefinedProblems` folder (or equivalent if the path has been
changed in :code:`Pathes.py`). The problem's file should contain a class :code:`Problem` containing the following fields:

    - **MeshName**                  *(string)*       -- The name of the geometry for which meshes have been generated (without the _$Resolution information)
    - **ProblemID**                 *(string)*       -- The name of the problem, usually matching the file's name
    - **GoverningEquationsIndex**   *(integer)*      -- The index of the considered governing equation, whose index is given in the module :code:`Mappers.py` (see :ref:`Mapping` for more details)
    - **FluidIndex**                *(integer list)* -- The list of the considered fluids' properties according to the index
    - **EOSEquationsIndex**         *(integer list)* -- The list of considered Equation of State for each fluid, whose index is given in accordance to the chosen governing equation set (the index correspondance can be found in the initialisation method of the class :code:`EOS` in the module detailing the considered governing equations)
    - **FluidInitialSubdomain**     *(subdomains specification list)*  -- The list of the subdomains where the fluids 2 to last lives (see :ref:`Subdomains`) for more details
    - **InitialConditions**         *(integer list)*                   -- The list of the initial conditions to apply on each subdomain, whose index is given in accordance to the chosen governing equation set (the index correspondance can be found in the initialisation method of the class :code:`InitialConditions` in the module detailing the considered governing equations)
    - **BoundaryConditions**        *(2D integer nested list)*         -- The list of boundary conditions to apply: BoundaryConditions[BoundaryTag][FluidIndex] -> Boundary condition's type

|
|

.. note::

    - The fluids' properties can by selected through the FluidIndex, but the model of the fluid directly depends from the governing equations.
    - A template for the creation of problems can be found in :code:`Templates` and a quick tutorial is available in the :ref:`UserManual`.
