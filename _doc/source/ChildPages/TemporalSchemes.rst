.. _TemporalSchemes:

Temporal Schemes
================

|

The modules defining temporal schemes are located in the folder :code:`_Solver/TemporalSchemes`.
As only one main iteration routine is required externally, the module's architecture is free.

|

.. rubric:: *Already implemented schemes*   

.. contents::
    :local:

|
|

------------------------------

Deffered correction
--------------------

.. automodule::  3_LESARD.SourceCode._Solver.TemporalSchemes.DeC
   :members:
   :imported-members:
   
