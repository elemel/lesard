.. _UserManual:

User Manual
====================

|
|


This tutorial guides you step by step through the definition of a personalized problem.
It start by a smooth hands-on introduction of the code structure by running example-problem
and teaches you to define your owns with the help of the user-friendly features. Ultimately,
it shows you possibilities to enrich the codes's modular features as schemes and fluid selectors.

.. note::

    - Templates for designing and defining new problems, settings sets and modular technical features are available in the companion folder  :code:`Templates`

    - For all the tutorial we assume a unix-based distribution, though theoritically the code should be cross-platforms

|
|
|

-----------------------------------------------

Basic tutorial
-----------------------------------------------

The basic tutorial shows how to run predefined problem, and how to design problems
from the equation catalogue already implemented in the code.

+----------------------------+---------------+------------------------------+---------------+------------------------------+
|  :ref:`RunFirstProblem`    |               |     :ref:`DesignYourProblem` |               | :ref:`DesignSettings`        |
+----------------------------+---------------+------------------------------+---------------+------------------------------+

|
|

-----------------------------------------------

Challenger tutorial
-----------------------------------------------

The challenger tutorial shows how to run define a complete problem, from the mesh generation to
the equation's definition by enriching the code's catalogue. It also points out the format to
use when willing to enrich the modular code's features (schemes, fluid selectors etc)




+------------------------------+-------------------------+
|   :ref:`DefineFullProblem`   | :ref:`EnrichTheCode`    |
+------------------------------+-------------------------+

|
|

--------------------

Folders organisation
--------------------

When having trouble to find a file that has been mentionned in the tutorial, use the search tool
of your browser on the listing below.

|

*Only showing the relevant folders and files for a common use*

.. code-block::


	├── CleanCache.sh
	├── Pathes.py
	├── PredefinedTests
	│   ├── PredefinedMeshes
	│   │   ├── BigSimpleSquare_o1_CG_res_20.pyMsh
	│   │   ├── BigSimpleSquare_o2_CG_res_20.pyMsh
	│   ├── PredefinedProblems
	│   │   ├── Advection_Rotation_1Fluid.py
	│   │   ├── Advection_Translation_1Fluid.py
	│   │   ├── Advection_Translation_1Fluid_SmallDomain.py
	│   │   ├── Advection_Translation_Bump_2Fluids.py
	│   │   ├── Advection_Translation_Cste_2Fluids.py
	│   │   ├── Advection_Translation_Obstacle_2Fluids.py
	│   │   ├── Euler_Karni.py
	│   │   ├── Euler_KarniR22.py
	│   │   ├── Euler_KarniSquare.py
	│   │   ├── Euler_SOD_1Fluid.py
	│   │   ├── Euler_SOD_2Fluid.py
	│   │   ├── Euler_SOD_2Fluid_SmallDomain.py
	│   │   ├── Euler_ShockTube.py
	│   │   ├── Euler_StationaryVortex_1Fluid.py
	│   │   ├── Euler_StillBubble.py
	│   │   └── __init__.py
	│   └── PredefinedSettings
	│       ├── FirstOrderGalerkin.txt
	│       ├── SecondOrderGalerkin.txt
	│       ├── SecondOrderGalerkinStream.txt
	│       ├── SettingsForEngineers.txt
	│       ├── Spatial1Time2_CGLFX.txt
	│       ├── TestSettings.txt
	│       └── ThirdOrderGalerkin.txt
	├── RunPostProcess.py
	├── RunProblem.py
	├── Solutions
	├── SourceCode
	│   ├── Solve.py
	│   ├── _Solver
	├── Templates
	│   ├── Template_Problem.py
	│   ├── Template_Settings.txt
	├── TestSheet.odt
