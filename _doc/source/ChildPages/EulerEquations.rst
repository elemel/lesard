.. _EulerEquations:

Euler equations
==========================================

|
|

.. automodule:: 3_LESARD.SourceCode.ProblemDefinition.GoverningEquations.EulerEquations
   :member-order: bysource

|

.. contents::
    :local:

|

----------------------------

Governing equations
----------------------------

|

.. autoclass:: 3_LESARD.SourceCode.ProblemDefinition.GoverningEquations.EulerEquations.Equations
    :members:
    :member-order: bysource

|
|
|

----------------------------

Equations of state
----------------------------

|

.. autoclass:: 3_LESARD.SourceCode.ProblemDefinition.GoverningEquations.EulerEquations.EOS
    :members:
    :member-order: bysource

|
|
|

----------------------------

Initial conditions
----------------------------

|

.. autoclass:: 3_LESARD.SourceCode.ProblemDefinition.GoverningEquations.EulerEquations.InitialConditions
    :members:
    :member-order: bysource

|
