.. _DesignSettings:

Step 3: Design your settings
============================


|
|

We saw in the Step 1 that a numerical problem is seen as a triplet (problem definition, settings to apply, mesh resolution),
and saw how to run such a tripplet. If you have no idea of which settings would yield to a decent solution, use directly
the provided settings file :code:`SettingsForEngineers`.

If you like to knowingly tune the parameters, we explain below how to create a "settings" file based on the code available solvers.
For the sake of the example, we will create a solver consisting of a limited first order Continuous Galerkin with Lax Friedrichs flux
in time and a second order Deferred Correction in time. We will name this setting :code:`Spatial1Time2_CGLFX`.

|

.. rubric:: Steps

.. contents::
    :local:
    :depth: 1

|


----------------------------

1. Create a new settings file
---------------------------

Copy and paste the template inside the folder where the predefined settings are stored.

.. code::

    cp Templates/Template_Settings.txt PredefinedTests/PredefinedSettings/Spatial1Time2_CGLFX.txt

Open it with your favorite text editor to modify its values.

|
|
|

---------------------------

2. Set the spatial solver
---------------------------

The spatial solver consists of a scheme, its properties and possibly a sequence of
modifications to apply to the achieved residuals.

|

.. rubric:: Select the spatial scheme

To select the scheme one wants to use, get the list of implemented schemes and their
associated index either in the file "Mappers.py" in the :code:`SourceCode`, or by using the
technical documentation of the class  :code:`Spatial_Scheme` on the page :ref:`Mapping`.

|

In our case, we can use one of the following schemes.

.. code::

    $ python -c "import SourceCode.Mappers as MP; print(MP.Spatial_Scheme.__doc__)"

    # Find the lines similar to the following
    *Implemented spatial schemes given the id*
    1. | Continuous Galerkin
    2. | Continuous Galerkin + Lax-Friedrichs flux

We select the scheme indexed by "2" and modify accordingly the first line of the given template.

.. code::

    1			    # Index of the scheme (see Mappers.py to know the indices meaning)

|

Specify then the properties of the scheme one line below, according to its given format. To find it, get the
scheme's argumenst list by querying the documentation of the scheme in the technical
handbook (in the relevant rubric of :ref:`SpatialSchemes`), or in the module itself.
In our case, the parameters that should be given are the wished order and the type of basis functions.
Indicate them on the second line, separated by spaces.

.. code::

    1 B	            # Scheme properties

|
|

.. rubric:: Define a residual's amendement sequence

It is possible to define a sequence of modifications to apply to the residuals computed by the selected
spatial scheme. The specification has to be given in the third line of the given template,
following the guidelines of the :code:`Usage` rubric of :ref:`SpatialModifiers`.
The indices of the mollifiers can be looked for in the file "Mappers.py" located in the :code:`SourceCode`, or by using the
technical documentation of the classes  :code:`Limiter` and :code:`Filtering` on the page :ref:`Mapping`.
In our case, we have for choices:

.. code::

    $ python -c "import SourceCode.Mappers as MP; print(MP.Limiter.__doc__)"

    # Find the lines similar to the following
    *Implemented amendements given the id* and their type
    -  Limiters
        1. | Psi limiter


    $ python -c "import SourceCode.Mappers as MP; print(MP.Filtering.__doc__)"

    # Find the lines similar to the following
    *Implemented amendements given the id* and their type
    -  Filtering
        1. | Streamline


Think to check each of the selected modifiers definition to check if they need arguments.
There, we choose to apply a Psi limiter, not requiring any argument, on the original residual.
If you do not want to apply any modification, just write "0" instead.

.. code::

    Limiter_1		# Additives list Index of limiter (0=None)

|
|

.. rubric:: Choose a fluid selector

To select the fluid selector one wants to use, get the list of implemented ones and their
associated index either in the file "Mappers.py" located in :code:`SourceCode`, or by using the
technical documentation of the class  :code:`Fluid_Selector` on the page :ref:`Mapping`.

|

In our case, we can use one of the following schemes.

.. code::

    $ python -c "import SourceCode.Mappers as MP; print(MP.Fluid_Selector.__doc__)"

    # Find the lines similar to the following
    *Implemented fluid's selectors*
    1. | NaiveLS_CG
    2. | NaiveLS_CG_LFX


We select the scheme indexed by "2" and modify accordingly the fourth line of the given template.

.. code::

    2	            # Type of fluid spotter (1=LS)

|

Specify then the properties of the fluid selector one line below, according to its given format. To find it, get the
argumenst list by querying the documentation of the fluid selector in the technical
handbook (in the relevant rubric of :ref:`FluidSelectors`), or in the module itself.
In our case, the parameters that should be given are the wished order and the type of basis functions.
Indicate them on the second line, separated by spaces.

.. code::

    1 B	            # Fluid selector properties

|
|

.. rubric:: Choose a redisancing technique for the Level Set, if wished

As it is knwon that the Level set evolution equation does not prevent the level set not to preserve the signed-distance function shape,
you may like to use a redistancing procedure. A couple of redistancing methods are available, and its selection and the corresponding
parameters prescription are done in a similar way as the spatial amendements. To select the type of redistancing available, use the
page :ref:`Mapping` or dive into the file :code:`Mapping.py` to find the relevant mapping indices.

.. code::

    $ python -c "import SourceCode.Mappers as MP; print(MP.Redistancer.__doc__)"

    # Find the lines similar to the following
    *Implemented redistancing routines*
        1. | HopfLax with Gradient Descent as an optimisation solver and Weno-like gradient approximation (recomendedd)
        2. | HopfLax with Bregman split method as optimizer
        3. | HopfLax with Backstracking Bregman split as optimizer, using Weno-like gradient approximation
        4. | HopfLax with Classical Bregman split as optimizer and Backstracking Bregman split as sub-optimizer, using Weno-like gradient approximation

In our example, we will choose the Gradient Descent algorithm, indexed with :code:`1`. The parameters that can be/have to be specified are given in the description
of the :code:`Redistancing` routine of the corresponding redistancing algorithm's class (find the list of Redistancers and their properties in :ref:`Redistancing`).
In our example for the Gradient descent, we will only choose to set the :code:`InitTime` and :code:`eps` values to respectively :code:`0.5` and :code:`1e-4`, leaving
all the other parameters as default.

|

Then, choose the periodicity with which to redistance the level set function, that is the number of "full time step" after which the level-set will be redistanced.
In this example, we select 50 full time-steps.

|

Lastly, one has to choose the spatial application range of the redistancing. Usually it is enough for most application to redistance within a vicinity of the interface.
To see the available routines and select the available spatial application range, please refer to the documentation of the :code:`SelectRedistancing` routine of the
:code:`WrapRedistancer.Redistancing` class definition (see also the technical documentation :ref:`Redistancing`). You will also find the description of the parameters
to give for using the selected routine there. In our example, we can choose between the following.

.. code::

	0. | NoRedistancing               --  Returns an identity function
	1. | NarrowBandRedistancing       --  Performs a redistancing within a narrow band around the interface
	2. | GlobalRedistancing           --  Performs a redistancing over the full computational domain
	3. | NarrowBandFixedRedistancing   --  Performs a redistancing within a narrow band around the interface, keeping the level set values at the immediate vicinity of the interface untouched

We select the :code:`NarrowBandRedistancing` spatial application range, and choose as :code:`BandWidth` the value :code:`0.5` for the mandatory parameter.

|

Given all those choices, the line to specify the redistancing algorithm reads

.. code::

	# RedistancingRoutineIndex_(ApplicationRangeIndex,ApplicationPeriod,ApplicationRangeParameter1,ApplicationRangeParameter2,...)<Parameters,of,selected,routine>
	1_(1,50,0.5)<0.5,1e-8>	# Never use spaces within the definition of the parameter string

|
|
|

---------------------------

3. Set the temporal scheme
---------------------------

The temporal solver consists of a time scheme and time iteration constraints.

|

.. rubric:: Select the time scheme

To select the scheme one wants to use in time, get the list of implemented schemes and their
associated index either in the file "Mappers.py" in the :code:`SourceCode`, or by using the
technical documentation of the class  :code:`Temporal_Scheme` on the page :ref:`Mapping`.

|

In our case, we only use (for now) the DeC time stepping.

.. code::

    $ python -c "import SourceCode.Mappers as MP; print(MP.Temporal_Scheme.__doc__)"

    # Find the lines similar to the following
    *Implemented temporal schemes given the id*
    1. | Deffered correction (DeC)


We select the temporal scheme indexed by "1" and modify accordingly the line 13 of the given template.

.. code::

    1			# Time scheme (see Mappers.py to know the indices meaning)

|

Specify then the properties of the temporal scheme one line below, according to its given format.
To find it, get the time scheme's argumenst list by querying the documentation in the technical
handbook (in the relevant rubric of :ref:`TemporalSchemes`), or in the module itself.
In our case, the parameters that should be given are the wished order and the number of corrections.
Indicate them on the second line, separated by spaces.

.. code::

    2 2 		# Properties

|
|

.. rubric:: Select the time iteration constraint

The time constraints are only the CFL number and the final time point.
We consider that any problem always starts at "t=0". Modify the lines 14 and 15
to your wishes.

.. code::

    0.4 	    # CFL
    3		    # Tmax, in seconds

|
|


------------------------------

5. Quadrature properties
------------------------------

|

For schemes that rely on quadrature metohd, the selection of the rule to use is done separately for the inner quadrature and
the boundary quadrature. By assumption, we assert that the quadrature rule is wished to be the same in all element.
This assumption also hols for mixed meshes, regardless the type of element. Therefore, make sure to select a quadrature rule
that is available for the considered element's type(s). An error message will be triggered otherwise.

|

The format to specify a quadrature rule for a scheme reads as follows.

.. code::

  $QuadratureProvider $QuadratureType $QuadratureProperty1 $QuadratureProperty2 .... | $QuadratureProvider $QuadratureType $QuadratureProperty1 $QuadratureProperty2 ....

where :code:`$QuadratureProvider` corresponds to the module providing the quadrature (HandBased, QuadPy, etc),
:code:`$QuadratureType` is the choice (when applicable) of the quadrature within the selected module and :code:`$QuadratureProperty`
are the parameters defining the quadrature of the chosen type (when applicable), usually its degree.

|

Get the list of implemented quadrature module and their associated index either in the file "Mappers.py" in the :code:`SourceCode`, or by using the
technical documentation of the class :code:`Quadrature_Rules` on the page :ref:`Mapping`.

.. code::

    $ python -c "import SourceCode.Mappers as MP; print(MP.Quadrature_Rules.__doc__)"

    # Find the lines similar to the following
    *Implemented boundary conditions*
    1. | HandBased, hardly coded
    2. | Using the library of QuadPy, with further choices to select (see the module itself for more details)

|

In our case, we select for the spatial scheme the hand based quadrature on the boundary of the element a quadpy scheme.
To see the available quadpy scheme, refer to :ref:`QuadPy`. We select there the 5th scheme of Williams Shunn Jameson, indexed by 15.
The setting line corresponding to the spatial scheme will then read

.. code::

  2 15 5 | 1    # Spatial scheme quadrature choice (if not required by the scheme, the value will be ignored but the line cannot be blank)

We do the same for the fluid selector and select the quadrature rules

.. code::

  1 | 2 6 3    # Level set scheme quadrature choice

|

.. note::

    - If the quadrature rule for the fluid selector differs from the spatial scheme's one, the code will run significantly slower (some speedup cannot be enabled).
    - For any scheme based on quadrature rules, specifying them is mandatory, otherwise the computation will fail with an obscure message.
    - Should you like to use a scheme that do not require any quadrature, leave no parameter but keep the comments at the end of the line, that is write:

    .. code::

        # Quadrature properties (0: Quadpy (write the index of the quadrature scheme and its properties after: 1 1 or 1 2 etc)    1: Custom)
            # Spatial scheme quadrature choice (if not required by the scheme, the value will be ignored but the line cannot be blank)
            # Level set scheme quadrature choice

    Any other comment after # would do but the line should not be blank.


|
|

------------------

6. Export properties
--------------------

|

.. rubric:: Select the result's export periodicity

Select the interval time of result's export in the results folder. Whenever the elapsed solution time between the currently computed solution
and the previously exported result is bigger than the specified interval, the current solution will be exported in the result folder.

.. code::

    0.05		# Time export intervals

.. note::

    Please do not use an interval smaller than 1e-7 seconds, the filenames' output format is not as precise and your exported solution may overwrite previous ones.

|

.. rubric:: Select the result's export type

To offer the possibility of exporting the results in a lighter binary structure, you can choose to drop the export of the mesh and only export the mesh information
that is required to load it externally along with the results later. This provides a huge gain in storage, but the results will not be standalone anymore, nor robust
to an unwished overwrite of the previously used mesh file.

.. code::

    1       # 1: MeshFree light export, 0: Full export with mesh structure

|

.. rubric:: Select the console verbose output mode

.. warning::

    TODO

|
|
