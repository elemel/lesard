.. _EnrichTheCode:

Adding codes' features or numerical methods
===========================================

|
|

This reference file just tells you the steps not to forget when adding a modular feature to the code.

.. note::

    In case of errors during the process of adding codes's features, we strongly recommend to pay attention to the :ref:`TechnicalManual`

|
|

.. contents::
    :local:
    :depth: 1

|
|


-------------------------

Adding a temporal scheme
-------------------------

Define a new temporal scheme following the given steps.

    - Rename and fill all the classes of the template :code:`Templates/Template_TemporalScheme.py`
    - Copy and paste it in the folder :code:`SourceCode/_Solver/TemporalScheme`.
    - Edit the class :code:`Temporal_Scheme` of  :code:`SourceCode/Mappers.py` by appending to the conditional list:
    - Edit the compatibility information in the main documentation page

    .. code::

        elif   id==TheChosenIndex:  return(fcts.partial(TI.TheNewModule.TheMainIterationRoutine, *params))


|
|

-------------------------

Adding a spatial scheme
-------------------------

Define a new spatial scheme following the given steps.

    - Rename and fill all the classes of the template :code:`Templates/Template_SpatialScheme.py`
    - Copy and paste it in the folder :code:`SourceCode/_Solver/SpatialScheme`.
    - Edit the class :code:`Spatial_Scheme` of  :code:`SourceCode/Mappers.py` by appending to the conditional list:
    - Edit the compatibility information in the main documentation page

    .. code::

        elif   id==TheChosenIndex:  return(SC.TheNewModule)

|
|
|

------------------------------------

Adding a spatial scheme's amendement
------------------------------------

Define a new spatial scheme following the given steps.

    - Rename and fill all the classes of the template :code:`Templates/Template_Limiter.py`. If the modifier is a limiter, rename it as "Limiter_TheName.py", if it is a filter, rename it as "Filtering_TheName.py", analogously if it is something else.
    - Copy and paste it in the folder :code:`SourceCode/_Solver/SpatialModifiers`.
    - Edit the class :code:`Limiter` (or :code:`Filtering` or your custom type's name) of  :code:`SourceCode/Mappers.py` by appending the new routine to the conditional list the line below. You can also create a new type of Mollifier; in that case, create a new mapping class in the file :code:`SourceCode/Mappers.py` following the models of the limiter and filtering ones.

    .. code::

        elif id==TheChosenIndex:	return(SM.TheTypeOfMollifier_ItsName)

    - Edit the compatibility information in the main documentation page


|
|
|

-------------------------

Adding a fluid selector
-------------------------

Define a new fluid selector following the given steps.

    - Rename and fill all the classes of the template :code:`Templates/Template_FluidSelector.py`.
    - Copy and paste it in the folder :code:`SourceCode/_Solver/FluidSelectors`.
    - Edit the class :code:`Fluid_Selector` of  :code:`SourceCode/Mappers.py` by appending to the conditional list:
    - Edit the compatibility information in the main documentation page

    .. code::

        elif   id == TheChosenIndex: return(FS.TheModuleName)

|

.. note::

    Even if you only want to amend a scheme for an existing Fluid selector, you need to create a new one incorporating automatically the mollification.



|
|
|


--------------------------------

Adding a redistancing technique
--------------------------------

Define a new redistancing technique following the given steps.

    - Rename the file and fill all the classes of the template :code:`Templates/Template_Redistancing.py`.
    - Copy and paste it in the folder :code:`SourceCode/_Solver/Redistancers`.
    - Edit the compatibility information in the main documentation page
    - Edit the class :code:`Redistancer` of  :code:`SourceCode/Mappers.py` by appending to the conditional list

    .. code::

        elif   id == TheChosenIndex: return(FR.TheModuleName.TheClassName(*params))

|

Define a new spatial application range for any redistancing technique by editing the class contained in the module
:code:`SourceCode/_Solver/WrapRedistancer.py` as follows.

    - Prepare your function having for arguments a :code:`Solution` structure, knowing that you will be able to use the internally stored instances of :code:`Meshes` and :code:`Problem`.
      Use the header format :code:`def TheNewFunction(self, Solution, Your, Parameters, *args)` and return an numpy array of the shape :code:`Solution.LSValues`.
    - Paste it at the end of the class :code:`Redistancer`
    - Add your function with a corresponding index in the list within :code:`SelectRedistancing` by adding either of the two lines

      .. code::

             elif   RedistanciationRange == NewIndex: return(self.TheNewFunction)
             elif   RedistanciationRange == NewIndex:  return(fcts.partial(self.TheNewFunction, *RangeParameters))

      depending wether you require more parameters than the Solution itself.
    - Update the documentation of :code:`SelectRedistancing` accordingly.

|
|

-------------------------

Adding a quadrature rule
-------------------------

|

.. warning::

    TODO
