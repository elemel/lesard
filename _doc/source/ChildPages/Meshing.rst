LESARD: Meshing modules
=====================

|
|

Three modules are taking care of already-defined meshes. They respectively define the product-type used for aggregating a mesh information,
the handler allowing to load a mesh, extract its properties and convert its format, and lastly an extension that allows the extraction
of its corresponding dual properties.

.. contents:: 
    :local:
    :depth: 1

.. note::
    
    To create meshes whose format is compatible with those modules (and therefore this code), please use 
    the program :code:`GeneralMeshing` available on request. Upon complains, an interface with the gmsh2
    format may be considered and further developped.
   
|
|
|

----------------------------------------

Mesh Structure
**************
    
.. automodule::   3_LESARD.SourceCode.Meshing.MeshStructure
    :members:
    :member-order: bysource
   
|
|

----------------------------------------
  
Primal Mesh
***********
    
.. automodule::   3_LESARD.SourceCode.Meshing.Meshing_PrimalMesh
    :members:
    :member-order: bysource
    
|
|
    
----------------------------------------

Dual Mesh
*********
    
.. automodule::   3_LESARD.SourceCode.Meshing.Meshing_DualMesh
     :members:
     :member-order: bysource
