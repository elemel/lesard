.. _DefineFullProblem:



Define a new type of governing equations
========================================


|
|

We saw in the Step 2 of the :code:`UserManual` how to define new problems based on existing governing equations.
Let us now see how to define a new set of governing equations and its associated fluid model.

.. note::

    As this level of feature developement, always refer to the :code:`TechnicalManual` to ensure the conformity 
    of the module when integrated to the global code.


After having defined your new set of equations, define your general problem following the basic tutorial.

    
|
|
|

------------------------------

Defining a fluid model
------------------------------

|

To define a fluid model that will be used by your governing equation, fill, rename, copy and paste the
class furnished in :code:`Templates/Template_FluidModel.py` template inside the file :code:`SourceCode/ProblemDefinition/FluidModels.py`.

|

.. warning::
    
    Pay an extra attention to the note of the rubric "Fluid models" of :code:`GoverningEquations`.

|
|
|

------------------------------

Defining a governing equation
------------------------------

|

To define a new set of governing equations, three steps are required:

    - Rename and fill all the classes of the template :code:`Templates/Template_Equations.py` 
    - Copy and paste it in the folder :code:`SourceCode/ProblemDefinition/GoverningEquations`.
    - Edit the class :code:`Governing_Equations` of  :code:`SourceCode/Mappers.py` by appending to the conditional list:
    
    .. code::
        
        if   id==TheChosenIndex: return(GE.TheNewModuleName)
    
|
