#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

# File that procures the necessary tools to handle barycentric coordinates


# ==============================================================================
#	Preliminary imports
# ==============================================================================
import numpy as np

# ==============================================================================
#	GetBarycentricCoordinates handling
# ==============================================================================

#### Get the barycentric coordinates in their classical definiton ####
def GetBarycentricCoordinates(points, vertices):
    """ Barycentric coordinates from the given vertices (forming a triangles)

    Args:
        Points    (2D numpy array):  Cartesian coordinates of the points to convert (NbPoints x 2)
        vertices  (2D numpy array):  Cartesian coordinates of the points taken as reference, ordered. (3 x 2)

    Returns:
        bcoors   (2D numpy array):  Barycentric coordinates of points with respect to the vertices (NbPoints x 3)
    """

    # Performing the computations of the barycentric coordinates
    hyp1 = vertices[1,:]-vertices[0,:]
    hyp2 = vertices[2,:]-vertices[0,:]
    dist = points-vertices[0,:]

    dhyp11 = np.dot(hyp1, hyp1)
    dhyp12 = np.dot(hyp1, hyp2)
    dhyp22 = np.dot(hyp2, hyp2)

    dhyp31 = np.dot(dist, hyp1)
    dhyp32 = np.dot(dist, hyp2)

    quotu = dhyp11*dhyp22-dhyp12**2

    b1 = (dhyp22*dhyp31-dhyp12*dhyp32)/quotu
    b2 = (dhyp11*dhyp32-dhyp12*dhyp31)/quotu
    bcoords = np.array([1-b1-b2, b1, b2]).T

    # Returning the barycentric coordinates
    return(bcoords)

#### Retrieving the cartesian coordinates ####
def GetCartesianCoordinates(bcoords, vertices):
    """ Barycentric coordinates from the given vertices (forming a triangles)

    Args:
        bcoors    (2D numpy array):  Barycentric coordinates of points with respect to the vertices (NbPoints x 3)
        vertices  (2D numpy array):  Cartesian coordinates of the points taken as reference, ordered. (3 x 2)

    Returns:
        Points    (2D numpy array):  Cartesian coordinates of the points to convert (NbPoints x 2)
    """

    # Computing back the cartesian points and returning them
    points = np.dot(bcoords, vertices)
    return(points)
