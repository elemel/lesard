#***************************************************************************
# Copyright (C) 2020 Élise Le Mélédo
# This file is part of LESARD.
#
# LESARD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LESARD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LESARD.  If not, see <https://www.gnu.org/licenses/>.
#****************************************************************************

# File that procures simple pythonic hacks

# ==============================================================================
#	Pythonic hack routines
# ==============================================================================

#### Partial function applying the last arguments ####
def partial(func, *args):
    """
    Partial function applying the last arguments

    Args:
        func (function callback): the function of interest
        *args  (arguments list):  the arguments to parse to the function from the end.

    Returns:
        Lmb (function callback): the lambda function defining the partial callback from the last arguments

    """
    return(lambda *a: func(*(a + args)))
