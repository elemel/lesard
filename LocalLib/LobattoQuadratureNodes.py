#Module furnishing the lobatto quadrature points and weights, copied
#from an external library belonging to Greg von Winckel - 04/17/2004


# ==============================================================================
#	Preliminary imports
# ==============================================================================
import numpy as np

# ==============================================================================
#	Actual routine
# ==============================================================================
def GaussLobatto(n,eps):
    """
    Computes the Legendre-Gauss-Lobatto nodes, weights and the LGL Vandermonde
    matrix. The LGL nodes are the zeros of (1-x^2)*P'_N(x). Useful for numerical
    integration and spectral methods.
    Initial content from Greg von Winckel - 04/17/2004, Python translation of lglnodes.m,
    translated and modified into Python by Jacob Schroder - 9/15/2018


    Args:
        n    (integer): requesting an nth-order Gauss-quadrature rule on [-1, 1]
        epse (float):   safety net for division

    Returns:
        (nodes, weights) (tuple): representing the quadrature nodes and weights.

    .. rubric:: Example

    .. code::

        >>> from lglnodes import *
        >>> (nodes, weights) = lglnodes(3)
        >>> print(str(nodes) + "   " + str(weights))
        [-1.        -0.4472136  0.4472136  1.       ]   [0.16666667 0.83333333 0.83333333 0.16666667]

    .. note::

        - (n+1) nodes and weights are returned

        - Reference on LGL nodes and weights: C. Canuto, M. Y. Hussaini, A. Quarteroni, T. A. Tang, "Spectral Methods in Fluid Dynamics," Section 2.3. Springer-Verlag 1987
    """

    # -------------------- Initialisation --------------------------------------
    w = np.zeros((n+1,))
    x = np.zeros((n+1,))
    xold = np.zeros((n+1,))

    # The Legendre Vandermonde Matrix
    P = np.zeros((n+1,n+1))

    # -------------------- Computations ----------------------------------------
    # Use the Chebyshev-Gauss-Lobatto nodes as the first guess
    for i in range(n+1): x[i] = -np.cos(np.pi*i / n)

    # Compute P using the recursion relation
    # Compute its first and second derivatives and
    # update x using the Newton-Raphson method.
    xold = 2.0
    for i in range(100):
        xold = x
        P[:,0] = 1.0
        P[:,1] = x

        for k in range(2,n+1): P[:,k] = ((2*k-1)*x*P[:,k-1] - (k-1)*P[:,k-2])/k
        x = xold - (x*P[:,n] - P[:,n-1])/((n+1)*P[:,n])

        if (np.max(np.abs(x - xold).flatten()) < eps): break

    # Compute the weights
    w = 2.0/((n*(n+1))*(P[:,n]**2))

    # -------------------- End-tasks -------------------------------------------
    # Return the values
    return(x, w)
